Imports System.Data.SqlClient
Public Class FrmNotasdeCreditoLogitel
    Dim monto As Double
    Dim factura As Integer
    Dim factura_inicila As Integer
    Dim clave_txt As String
    'Variables biracora

    Private suc_aplica As String = Nothing
    Private caja As String = Nothing
    Private cajero As String = Nothing
    Private fecha_caducidad As String = Nothing
    Private Locactura As String = Nothing
    Private Observa As String = Nothing
    Private Locmonto As String = Nothing
    ' Private Locsaldo As String = nothing
    Private conGlo As New SqlConnection(MiConexion)
    Private eSaldo As Decimal = 0
    Private eRes As Integer = 0
    Private eMsg As String = String.Empty
    Private Sub damedatosbitacora()
        Try
            suc_aplica = Me.ComboBox7.Text
            caja = Me.ComboBox8.Text
            cajero = Me.ComboBox5.Text
            fecha_caducidad = Me.Fecha_CaducidadDateTimePicker.Text
            Locactura = Me.ComboBox3.Text
            Observa = Me.ObservacionesTextBox.Text
            Locmonto = Me.MontoTextBox.Text
            'Locsaldo = Me.TextBox1.Text
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora()
        Try
            If OPCION = "M" Then
                'suc_aplica = Me.ComboBox7.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, "Sucural Aplica" + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), suc_aplica, Me.ComboBox7.Text, SubCiudad)
                'caja = Me.ComboBox8.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, "Caja Aplica" + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), caja, Me.ComboBox8.Text, SubCiudad)
                'cajero = Me.ComboBox5.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, "Cajero Aplica" + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), cajero, Me.ComboBox5.Text, SubCiudad)
                'fecha_caducidad = Me.Fecha_CaducidadDateTimePicker.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, Me.Fecha_CaducidadDateTimePicker.Name + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), fecha_caducidad, Me.Fecha_CaducidadDateTimePicker.Text, SubCiudad)
                'Locactura = Me.ComboBox3.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, "Factura Genera" + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), Locactura, Me.ComboBox3.Text, SubCiudad)
                'Observa = Me.ObservacionesTextBox.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, Me.ObservacionesTextBox.Name + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), Observa, Me.ObservacionesTextBox.Text, SubCiudad)
                'Locmonto = Me.MontoTextBox.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, Me.MontoTextBox.Name + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), Locmonto, Me.MontoTextBox.Text, SubCiudad)
                ''Locsaldo = Me.TextBox1.Text
                'bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, Me.MontoTextBox.Name + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), Locsaldo, Me.TextBox1.Text, SubCiudad)
                damedatosbitacora()
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub busca()
        Try
            Dim con2 As New SqlConnection(MiConexion)
            con2.Open()
            Me.Consulta_NotaCreditoTableAdapter.Connection = con2
            Me.Consulta_NotaCreditoTableAdapter.Fill(Me.DataSetLydia.Consulta_NotaCredito, gloClvNota)

            'Me.DetalleNOTASDECREDITOTableAdapter.Connection = con2
            'Me.DetalleNOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.DetalleNOTASDECREDITO, gloClvNota)
            'DameTiponota] (@Clv_nota bigint,@tipo int output)
            Dim comando As New SqlClient.SqlCommand
            With comando
                .CommandText = "DameTipoNota"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = con2

                Dim prm As New SqlParameter("@Clv_Nota", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = gloClvNota
                .Parameters.Add(prm)

                Dim prm2 As New SqlParameter("Tipo", SqlDbType.Int)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)
                .ExecuteNonQuery()
                glotipoNota = prm2.Value
            End With
            con2.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmNotasdeCredito_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Glocontratosel > 0 Then
            Me.ContratoTextBox.Text = Glocontratosel
            Glocontratosel = 0
        End If
        'If bnd = 3 Then
        '    If glotipoNota = 0 Then
        '        Me.REDLabel3.Text = "Nota de Cr�dito en Efectivo "
        '        Me.Panel4.Show()
        '        Me.Panel5.Hide()
        '    ElseIf glotipoNota = 1 Then
        '        Me.REDLabel3.Text = "Nota de Cr�dito por Factura "
        '        Me.Panel4.Show()
        '        Me.Panel5.Hide()
        '        'ElseIf glotipoNota = 2 Then
        '        '    Me.REDLabel3.Text = "Nota de Cr�dito por Concepto de Servicio"
        '        '    Me.Panel4.Hide()
        '        '    Me.Panel5.Show()
        '        '    conGlo.Open()
        '        '    Me.MuestraDetalleNota_por_ConceptoTableAdapter.Connection = conGlo
        '        '    Me.MuestraDetalleNota_por_ConceptoTableAdapter.Fill(Me.DataSetLydia.MuestraDetalleNota_por_Concepto, Me.Clv_NotadeCreditoTextBox.Text)
        '        '    conGlo.Close()
        '    End If
        '    bnd = 2
        'End If

    End Sub

    Private Sub FrmNotasdeCreditoLogitel_BackgroundImageLayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.BackgroundImageLayoutChanged

    End Sub

    Private Sub FrmNotasdeCredito_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

    End Sub

    Private Sub FrmNotasdeCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim con3 As New SqlConnection(MiConexion)
            con3.Open()
            'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLydia.MuestraTipSerPrincipal_SER' Puede moverla o quitarla seg�n sea necesario.
            Me.MuestraTipSerPrincipal_SERTableAdapter.Connection = con3
            Me.MuestraTipSerPrincipal_SERTableAdapter.Fill(Me.DataSetLydia.MuestraTipSerPrincipal_SER)
            'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLydia.Muestra_Tipo_Nota' Puede moverla o quitarla seg�n sea necesario.
            Me.Muestra_Tipo_NotaTableAdapter.Connection = con3
            Me.Muestra_Tipo_NotaTableAdapter.Fill(Me.DataSetLydia.Muestra_Tipo_Nota, 1)
            'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLydia.MUESTRAUSUARIOSIN' Puede moverla o quitarla seg�n sea necesario.
            Me.MUESTRAUSUARIOSin2TableAdapter.Connection = con3
            Me.MUESTRAUSUARIOSin2TableAdapter.Fill(Me.DataSetLydia.MUESTRAUSUARIOSin2, 0)
            'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLydia.MUESTRAUSUARIOSIN' Puede moverla o quitarla seg�n sea necesario.
            Me.MUESTRAUSUARIOSINTableAdapter.Connection = con3
            Me.MUESTRAUSUARIOSINTableAdapter.Fill(Me.DataSetLydia.MUESTRAUSUARIOSIN)
            'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLydia.DAMEFECHADELSERVIDOR_2' Puede moverla o quitarla seg�n sea necesario.
            Me.DAMEFECHADELSERVIDOR_2TableAdapter.Connection = con3
            Me.DAMEFECHADELSERVIDOR_2TableAdapter.Fill(Me.DataSetLydia.DAMEFECHADELSERVIDOR_2)
            'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLydia.StatusNotadeCredito' Puede moverla o quitarla seg�n sea necesario.
            Me.StatusNotadeCreditoTableAdapter.Connection = con3
            Me.StatusNotadeCreditoTableAdapter.Fill(Me.DataSetLydia.StatusNotadeCredito)
            Me.MUESTRASUCURSALES2TableAdapter.Connection = con3
            Me.MUESTRASUCURSALES2TableAdapter.Fill(Me.DataSetLydia.MUESTRASUCURSALES2, 0)

            con3.Close()
            colorea(Me)
            Glocontratosel = 0
            If OPCION = "N" Then
                gloClvNota = 0
                Me.Consulta_NotaCreditoBindingSource.AddNew()
                Me.Fecha_deGeneracionDateTimePicker.Value = Me.DateTimePicker1.Text
                Me.Clv_NotadeCreditoTextBox.Text = 0
                Me.ComboBox4.Text = GloUsuario
                Me.ComboBox1.SelectedValue = GloSucursal
                Me.Panel1.Enabled = False
                Me.Panel4.Enabled = False
                Me.Panel5.Enabled = False
                Me.ComboBox2.SelectedValue = "A"
                Me.ComboBox2.Enabled = False
                Me.ComboBox4.Enabled = False
                Me.Button4.Enabled = False

            ElseIf OPCION = "M" Or OPCION = "C" Then
                Me.ContratoTextBox.ReadOnly = True
                Me.ContratoTextBox.BackColor = Color.White
                Me.ComboBox1.Enabled = True
                Me.Fecha_CaducidadDateTimePicker.Enabled = True
                Me.ComboBox2.Enabled = False
                Me.ComboBox4.Enabled = False
                busca()
                damedatosbitacora()

                If CInt(Me.MontoTextBox.Text) = CInt(Me.TextBox1.Text) Then
                    Me.ComboBox3.Enabled = True
                    Me.DAME_FACTURASDECLIENTETableAdapter.Connection = con3
                    Me.DAME_FACTURASDECLIENTETableAdapter.Fill(Me.DataSetLydia.DAME_FACTURASDECLIENTE, Me.ContratoTextBox.Text, Me.Clv_NotadeCreditoTextBox.Text)
                ElseIf CInt(Me.TextBox1.Text) = 0 Then
                    Me.ComboBox3.Enabled = False
                    Me.Detalle_NotasdeCreditoDataGridView.Enabled = False
                Else
                    Me.ComboBox3.Enabled = False
                End If
                Me.ComboBox3.SelectedValue = Me.TextBox4.Text
                Factura_inicial = CInt(Me.TextBox4.Text)

                ConRelEstadoDeCuentaAplicada(CLng(Me.Clv_NotadeCreditoTextBox.Text), CLng(Me.ComboBox3.SelectedValue))

            End If
            con3.Close()
            'If bnd = 1 And IdSistema <> "LO" Then
            '    LiTipo = 1
            '    FrmTipoNota.Show()
            'End If
            'If IdSistema = "VA" Then
            '    glotipoNota = 2
            '    Me.Panel4.Hide()
            '    Me.Panel5.Show()
            '    conGlo.Open()
            '    Me.MuestraDetalleNota_por_ConceptoTableAdapter.Connection = conGlo
            '    Me.MuestraDetalleNota_por_ConceptoTableAdapter.Fill(Me.DataSetLydia.MuestraDetalleNota_por_Concepto, Me.Clv_NotadeCreditoTextBox.Text)
            '    conGlo.Close()
            'End If

            If OPCION = "C" Or status = "Cancelada" Or status = "Saldada" Then
                Me.Button5.Enabled = True
                Me.Panel3.Enabled = True
                Me.ComboBox1.Enabled = False
                Me.ComboBox2.Enabled = False
                Me.ComboBox3.Enabled = False
                Me.ComboBox4.Enabled = False
                Me.ComboBox5.Enabled = False
                Me.ComboBox6.Enabled = False
                Me.ComboBox7.Enabled = False
                Me.ComboBox8.Enabled = False
                Me.Panel4.Enabled = False
                Me.Panel5.Enabled = False
                Me.Fecha_CaducidadDateTimePicker.Enabled = False
                Me.Fecha_deGeneracionDateTimePicker.Enabled = False
                Me.ObservacionesTextBox.Enabled = False
                Me.Button5.Enabled = True
                Me.Detalle_NotasdeCreditoDataGridView.Enabled = False
            End If
            Me.SOLOINTERNETCheckBox.Enabled = False
            Me.ESHOTELCheckBox.Enabled = False

            'If IdSistema = "VA" Then
            '    Me.TextBox1.Visible = False
            '    Me.CMBLabel1.Visible = False
            '    Me.Text = "Cat�logo de Devoluciones en Efectivo"
            '    Me.Label2.Text = "Pagos Realizados con La Devoluci�n :"
            '    ' Me.REDLabel3.Text = "Tipo de Devolucion"
            '    Me.REDLabel3.Text = "Devoluci�n en Efectivo "
            '    Me.Label4.Text = "Sucursal donde se Aplica la Devoluci�n :"
            '    Me.Usuario_AutorizoLabel.Text = "Usuario que realiza la Devoluci�n :"
            '    Me.Clv_sucursalLabel.Text = "Sucursal que Realiza la Devoluci�n :"
            '    Me.CMBClv_NotadeCreditoLabel.Text = "Devoluci�n :"
            '    'Me.Button4.Text = "Pagos Realizados con esta Devoluci�n"
            '    Me.Button4.Visible = False

            '    ComboBox7.SelectedValue = GloSucursal
            '    ComboBox8.SelectedValue = 0
            '    ComboBox8.SelectedValue = GloCaja
            '    ComboBox5.SelectedValue = 0
            '    ComboBox5.SelectedValue = GloUsuario
            '    ComboBox7.Enabled = False
            '    ComboBox8.Enabled = False
            '    ComboBox5.Enabled = False

            'End If

            If IdSistema = "LO" Then
                glotipoNota = 1
                Me.REDLabel3.Text = "Nota de Cr�dito por Factura "
                Me.REDLabel3.ForeColor = Color.Black
                Me.Panel4.Show()
                Me.Panel5.Hide()
                ComboBox7.SelectedValue = GloSucursal
                ComboBox8.SelectedValue = 0
                ComboBox8.SelectedValue = GloCaja
                ComboBox5.SelectedValue = 0
                ComboBox5.SelectedValue = GloUsuario
                ComboBox7.Enabled = False
                ComboBox8.Enabled = False
                ComboBox5.Enabled = False

            End If

            If OPCION = "C" Then
                Me.GroupBox2.Enabled = False
                Me.MontoTextBox.Enabled = False
                Me.Button3.Enabled = False
            End If
            If OPCION = "N" Or OPCION = "M" Then
                UspDesactivaBotones(Me, Me.Name)
            End If
            UspGuardaFormularios(Me.Name, Me.Text)
            UspGuardaBotonesFormularioSiste(Me, Me.Name)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Me.ComboBox3.SelectedValue) = True Then
            borra_Predetalle(Me.ComboBox3.SelectedValue)
        End If
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Consulta_NotaCreditoBindingSource.CancelEdit()
        Me.Close()
    End Sub

    Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged
        Dim nota As Integer
        Dim CONE As New SqlClient.SqlConnection(MiConexion)
        Try
            If IsNumeric(Me.ContratoTextBox.Text) = False Then
                Me.Panel1.Enabled = False
                Me.Panel5.Enabled = False
                Me.Panel4.Enabled = False
                CONE.Open()
                Me.BUSCLIPORCONTRATOTableAdapter.Connection = CONE
                Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.DataSetLydia.BUSCLIPORCONTRATO, 0, "", "", "", "", 0, 0)
                CONE.Close()
            ElseIf CInt(Me.ContratoTextBox.Text) = 0 Then
                Me.Panel1.Enabled = False
                Me.Panel5.Enabled = False
                Me.Panel4.Enabled = False
                CONE.Open()
                Me.BUSCLIPORCONTRATOTableAdapter.Connection = CONE
                Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.DataSetLydia.BUSCLIPORCONTRATO, 0, "", "", "", "", 0, 0)
                CONE.Close()
            ElseIf CInt(Me.ContratoTextBox.Text) > 0 Then

                Me.Panel1.Enabled = True
                Me.Panel5.Enabled = True
                Me.Panel4.Enabled = True
                If OPCION = "N" Or OPCION = "M" Then
                    If IsNumeric(Me.ContratoTextBox.Text) = True Then
                        Me.ComboBox3.Enabled = True
                        If Me.Clv_NotadeCreditoTextBox.Text = "" Then
                            nota = 0
                        Else
                            nota = Me.Clv_NotadeCreditoTextBox.Text
                        End If
                        CONE.Open()
                        Me.BUSCLIPORCONTRATOTableAdapter.Connection = CONE
                        Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.DataSetLydia.BUSCLIPORCONTRATO, Me.ContratoTextBox.Text, "", "", "", "", 0, 0)
                        Me.DAME_FACTURASDECLIENTETableAdapter.Connection = CONE
                        Me.DAME_FACTURASDECLIENTETableAdapter.Fill(Me.DataSetLydia.DAME_FACTURASDECLIENTE, Me.ContratoTextBox.Text, gloClvNota)
                        CONE.Close()
                        CREAARBOL()
                        Me.ComboBox3.Text = ""
                        Me.ComboBox3.SelectedValue = 0
                        If IsNumeric(Me.ComboBox3.SelectedValue) = True Then
                            'borra_Predetalle(Me.ComboBox3.SelectedValue)
                            'Muestra_Predetalle(Me.ComboBox3.SelectedValue)
                            Calcula_monto(Me.ComboBox3.SelectedValue, 0)
                            'Me.MontoTextBox.Text = Me.ComboBox6.Text
                            Me.TextBox1.Text = Format(Me.MontoTextBox.Text, "###,##0.00")
                        End If
                        If Me.ComboBox3.SelectedValue Is Nothing And Me.TextBox5.Text = "" Then
                            'Muestra_Predetalle(0)
                        End If
                    Else
                        Me.ComboBox3.Text = ""
                    End If
                ElseIf OPCION = "C" Then
                    CONE.Open()
                    Me.BUSCLIPORCONTRATOTableAdapter.Connection = CONE
                    Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.DataSetLydia.BUSCLIPORCONTRATO, Me.ContratoTextBox.Text, "", "", "", "", 0, 0)
                    CONE.Close()
                    CREAARBOL()
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub CREAARBOL()

        Try
            Dim CON As New SqlConnection(MiConexion)

            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next
            CON.Open()
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.DataSetLydia.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.DataSetLydia.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            CON.Close()
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.DataSetLydia.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)

                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next

            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        FrmSelCliente.Show()
    End Sub
    Private Sub Muestra_Predetalle(ByVal factura As Integer)
        Dim conlidia2 As New SqlClient.SqlConnection(MiConexion)
        conlidia2.Open()
        Dim filarow As DataRow
        Me.Detalle_NotasdeCreditoTableAdapter.Connection = conlidia2
        Me.Detalle_NotasdeCreditoTableAdapter.Fill(Me.DataSetLydia.Detalle_NotasdeCredito, factura)
        For Each FilaRow In Me.DataSetLydia.Detalle_NotasdeCredito.Rows
            If filarow("Descripcion".ToString()) Is Nothing Then
                Exit For
            End If
        Next
        conlidia2.Close()
    End Sub
    Private Sub borra_Predetalle(ByVal factura1 As Integer)
        Dim conlidia As New SqlClient.SqlConnection(MiConexion)
        Dim comando As New SqlClient.SqlCommand
        conlidia.Open()
        With comando
            .Connection = conlidia
            .CommandText = "Borrar_Session_Notas "
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0
            ' Create a SqlParameter for each parameter in the stored procedure.
            Dim prm As New SqlParameter("@Clv_factura", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm.Value = factura1
            .Parameters.Add(prm)
            Dim i As Integer = comando.ExecuteNonQuery()
        End With
        conlidia.Close()
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim eAplicada As Integer = 0
        Dim comando2 As New SqlClient.SqlCommand(MiConexion)
        Dim comando3 As New SqlClient.SqlCommand(MiConexion)
        Dim cone As New SqlClient.SqlConnection(MiConexion)
        Dim comando As New SqlClient.SqlCommand
        Dim msg As String
        Dim bnd As Integer
        Try
            If OPCION = "N" Then
                If IsNumeric(Me.ContratoTextBox.Text) = False Then
                    MsgBox(" Es Necesario que se Capture un N�mero de Contrato ", MsgBoxStyle.Information)
                ElseIf IsNumeric(Me.ComboBox1.SelectedValue) = False Then
                    MsgBox(" Es Necesario que se Capture una Sucursal ", MsgBoxStyle.Information)
                ElseIf IsNumeric(Me.ComboBox4.SelectedValue) = False Then
                    MsgBox(" Es Necesario que se Capture un Usuario que Autorize la Nota de Cr�dito ", MsgBoxStyle.Information)
                ElseIf Me.ComboBox3.Text.Length = 0 Then
                    MsgBox("Selecciona la Factura a la cual se le aplicar� la Nota de Cr�dito.", MsgBoxStyle.Information)
                ElseIf IsNumeric(Me.MontoTextBox.Text) = False Then
                    MsgBox(" Es Necesario que se Capture el Monto de la Nota de Cr�dito ", MsgBoxStyle.Information)
                ElseIf IsNumeric(Me.ComboBox7.SelectedValue) = False Then
                    MsgBox("Es Necesario que se Elija una Sucursal ", MsgBoxStyle.Information)
                ElseIf IsNumeric(Me.ComboBox8.SelectedValue) = False Then
                    MsgBox("Es Necesario que se Elija una Caja", MsgBoxStyle.Information)
                ElseIf Me.ComboBox5.Text = "" Then
                    MsgBox("Es Necesario que se Elija un Cajero ", MsgBoxStyle.Information)
                Else

                    cone.Open()
                    With comando
                        .Connection = cone
                        .CommandText = "Nueva_NotadeCredito "
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@Clv_Notadecredito", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@Contrato", SqlDbType.BigInt)
                        Dim prm12 As New SqlParameter("@Factura", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@Fecha_deGeneracion", SqlDbType.DateTime)
                        Dim prm3 As New SqlParameter("@Usuario_Captura", SqlDbType.VarChar)
                        Dim prm4 As New SqlParameter("@Usuario_Autorizo", SqlDbType.VarChar)
                        Dim prm5 As New SqlParameter("@Fecha_Caducidad", SqlDbType.DateTime)
                        Dim prm6 As New SqlParameter("@Monto", SqlDbType.Decimal)
                        Dim prm7 As New SqlParameter("@Status", SqlDbType.VarChar)
                        Dim prm8 As New SqlParameter("@Observaciones", SqlDbType.VarChar)
                        Dim prm9 As New SqlParameter("@Sucursal", SqlDbType.Int)
                        Dim prm10 As New SqlParameter("@suc_aplica", SqlDbType.Int)
                        Dim prm11 As New SqlParameter("@tipo", SqlDbType.Int)
                        Dim prm13 As New SqlParameter("@caja", SqlDbType.Int)
                        ' bigint, bigint,@Factura bigint, datetime , varchar(50)  varchar(50), datetime, decimal(18,0), varchar(50), varchar(max), int)
                        prm.Direction = ParameterDirection.Output
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm12.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm5.Direction = ParameterDirection.Input
                        prm6.Direction = ParameterDirection.Input
                        prm7.Direction = ParameterDirection.Input
                        prm8.Direction = ParameterDirection.Input
                        prm9.Direction = ParameterDirection.Input
                        prm10.Direction = ParameterDirection.Input
                        prm11.Direction = ParameterDirection.Input
                        prm13.Direction = ParameterDirection.Input

                        prm.Value = Me.Clv_NotadeCreditoTextBox.Text
                        prm1.Value = Me.ContratoTextBox.Text
                        prm2.Value = Me.Fecha_deGeneracionDateTimePicker.Text
                        If IsNumeric(Me.ComboBox3.SelectedValue) = False Then
                            prm12.Value = 0
                        Else
                            prm12.Value = Me.ComboBox3.SelectedValue
                        End If
                        prm3.Value = Me.ComboBox5.SelectedValue
                        prm4.Value = Me.ComboBox4.SelectedValue
                        prm5.Value = Me.Fecha_CaducidadDateTimePicker.Text
                        prm6.Value = CDec(Me.MontoTextBox.Text)
                        prm7.Value = Me.ComboBox2.SelectedValue
                        prm8.Value = Me.ObservacionesTextBox.Text
                        prm9.Value = Me.ComboBox1.SelectedValue
                        prm10.Value = Me.ComboBox7.SelectedValue
                        prm11.Value = glotipoNota
                        prm13.Value = Me.ComboBox8.SelectedValue

                        .Parameters.Add(prm)
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm12)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm5)
                        .Parameters.Add(prm6)
                        .Parameters.Add(prm7)
                        .Parameters.Add(prm8)
                        .Parameters.Add(prm9)
                        .Parameters.Add(prm10)
                        .Parameters.Add(prm11)
                        .Parameters.Add(prm13)

                        Dim i As Integer = comando.ExecuteNonQuery()
                        Me.Clv_NotadeCreditoTextBox.Text = prm.Value
                    End With



                    'If glotipoNota = 0 Or glotipoNota = 1 Then
                    '    With comando2
                    '        .Connection = cone
                    '        .CommandTimeout = 0
                    '        .CommandText = "Guarda_DetalleNota"
                    '        .CommandType = CommandType.StoredProcedure
                    '        Dim prm As New SqlParameter("Clv_Factura", SqlDbType.BigInt)
                    '        prm.Direction = ParameterDirection.Input
                    '        prm.Value = Me.ComboBox3.SelectedValue
                    '        .Parameters.Add(prm)
                    '        Dim i As Integer = comando2.ExecuteNonQuery
                    '    End With
                    'ElseIf glotipoNota = 2 Then
                    '    Agregar_Conceptos(2)
                    'End If

                    'Cancelacion_FacturasPorNotas](@CLV_FACTURA BIGINT,@MSG VARCHAR(250) OUTPUT,@BNDERROR INT OUTPUT)
                    If glotipoNota = 1 Then
                        With comando3
                            .CommandText = "Cancelacion_FacturasPorNotas"
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure
                            .Connection = cone
                            Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                            prm.Direction = ParameterDirection.Input
                            prm.Value = Me.ComboBox3.SelectedValue
                            .Parameters.Add(prm)

                            Dim prm2 As New SqlParameter("@MSG", SqlDbType.VarChar, 250)
                            prm2.Direction = ParameterDirection.Output
                            prm2.Value = ""
                            .Parameters.Add(prm2)


                            Dim prm3 As New SqlParameter("@BNDERROR", SqlDbType.Int)
                            prm3.Direction = ParameterDirection.Output
                            prm3.Value = 1
                            .Parameters.Add(prm3)

                            Dim i As Integer = comando3.ExecuteNonQuery
                            msg = prm2.Value
                            bnd = prm3.Value
                        End With
                    End If
                    cone.Close()
                    bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, "Se Hizo una Nueva nota de Cr�dito", "Tipo:" + CStr(glotipoNota), "Monto:" + CStr(Me.MontoTextBox.Text), SubCiudad)

                    CancelaEdoDCuentaPorNotas(CLng(Me.ComboBox3.SelectedValue))
                    If Me.RadioButtonSi.Checked = True Then
                        eAplicada = 1
                    Else
                        eAplicada = 0
                    End If
                    NueRelEstadoDeCuentaAplicada(Me.Clv_NotadeCreditoTextBox.Text, CLng(Me.ComboBox3.SelectedValue), eAplicada)


                    MsgBox("Se ha Guardado con �xito")
                    refrescar = True
                    gloClvNota = Me.Clv_NotadeCreditoTextBox.Text
                    LocBndNotasReporteTick = True
                    locoprepnotas = 1
                    FrmImprimirRepGral.Show()
                    Me.Close()
                    borra_Predetalle(Me.ComboBox3.SelectedValue)
                End If

            ElseIf OPCION = "M" Then
                If IsNumeric(Me.ComboBox4.SelectedValue) = False Then
                    MsgBox(" Es Necesario que se Capture un Usuario que Autorize la Nota de Cr�dito ", MsgBoxStyle.Information)
                    Exit Sub
                ElseIf IsNumeric(Me.MontoTextBox.Text) = False Then
                    MsgBox(" Es Necesario que se Capture el Monto de la Nota de Cr�dito ", MsgBoxStyle.Information)
                    Exit Sub
                ElseIf Me.ComboBox5.Text = "" Then
                    MsgBox("Es Necesario que se Elija un Cajero ", MsgBoxStyle.Information)
                    Exit Sub
                ElseIf IsNumeric(Me.ComboBox7.SelectedValue) = False Then
                    MsgBox("Es Necesario que se Elija una Sucursal ", MsgBoxStyle.Information)
                    Exit Sub
                ElseIf IsNumeric(Me.ComboBox8.SelectedValue) = False Then
                    MsgBox("Es Necesario que se Elija una Caja", MsgBoxStyle.Information)
                    Exit Sub
                End If

                cone.Open()
                With comando
                    .Connection = cone
                    .CommandText = "Modifica_NotaCredito "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm As New SqlParameter("@Clv_Notadecredito", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@Contrato", SqlDbType.BigInt)
                    Dim prm12 As New SqlParameter("@Factura", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Fecha_deGeneracion", SqlDbType.DateTime)
                    Dim prm3 As New SqlParameter("@Usuario_Captura", SqlDbType.VarChar)
                    Dim prm4 As New SqlParameter("@Usuario_Autorizo", SqlDbType.VarChar)
                    Dim prm5 As New SqlParameter("@Fecha_Caducidad", SqlDbType.DateTime)
                    Dim prm6 As New SqlParameter("@Monto", SqlDbType.Decimal)
                    Dim prm7 As New SqlParameter("@Status", SqlDbType.VarChar)
                    Dim prm8 As New SqlParameter("@Observaciones", SqlDbType.VarChar)
                    Dim prm9 As New SqlParameter("@Sucursal", SqlDbType.Int)
                    Dim prm10 As New SqlParameter("@suc_aplica", SqlDbType.Int)
                    Dim prm11 As New SqlParameter("@tipo", SqlDbType.Int)
                    Dim prm13 As New SqlParameter("@caja", SqlDbType.Int)

                    ' bigint, bigint,@Factura bigint, datetime , varchar(50)  varchar(50), datetime, decimal(18,0), varchar(50), varchar(max), int)
                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm12.Direction = ParameterDirection.Input
                    prm3.Direction = ParameterDirection.Input
                    prm4.Direction = ParameterDirection.Input
                    prm5.Direction = ParameterDirection.Input
                    prm6.Direction = ParameterDirection.Input
                    prm7.Direction = ParameterDirection.Input
                    prm8.Direction = ParameterDirection.Input
                    prm9.Direction = ParameterDirection.Input
                    prm10.Direction = ParameterDirection.Input
                    prm11.Direction = ParameterDirection.Input
                    prm13.Direction = ParameterDirection.Input

                    prm.Value = Me.Clv_NotadeCreditoTextBox.Text
                    prm1.Value = Me.ContratoTextBox.Text
                    prm2.Value = Me.Fecha_deGeneracionDateTimePicker.Text
                    If IsNumeric(Me.ComboBox3.SelectedValue) = False Then
                        prm12.Value = 0
                    Else
                        prm12.Value = Me.ComboBox3.SelectedValue
                    End If
                    prm3.Value = Me.ComboBox5.SelectedValue
                    prm4.Value = Me.ComboBox4.SelectedValue
                    prm5.Value = Me.Fecha_CaducidadDateTimePicker.Text
                    prm6.Value = CDec(Me.MontoTextBox.Text)
                    prm7.Value = Me.ComboBox2.SelectedValue
                    prm8.Value = Me.ObservacionesTextBox.Text
                    prm9.Value = Me.ComboBox1.SelectedValue
                    prm10.Value = Me.ComboBox7.SelectedValue
                    prm11.Value = glotipoNota
                    prm13.Value = Me.ComboBox8.SelectedValue

                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm12)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm5)
                    .Parameters.Add(prm6)
                    .Parameters.Add(prm7)
                    .Parameters.Add(prm8)
                    .Parameters.Add(prm9)
                    .Parameters.Add(prm10)
                    .Parameters.Add(prm11)
                    .Parameters.Add(prm13)

                    Dim i As Integer = comando.ExecuteNonQuery()
                End With
                cone.Close()
                'If glotipoNota = 0 Or glotipoNota = 1 Then
                '    If Factura_inicial = Me.ComboBox3.SelectedValue Then
                '        cone.Open()
                '        With comando2
                '            .Connection = cone
                '            .CommandTimeout = 0
                '            .CommandText = "Modifica_DetalleNota"
                '            .CommandType = CommandType.StoredProcedure
                '            Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                '            prm.Direction = ParameterDirection.Input
                '            prm.Value = Me.ComboBox3.SelectedValue
                '            .Parameters.Add(prm)
                '            Dim i As Integer = comando2.ExecuteNonQuery
                '        End With
                '        cone.Close()
                '    Else
                '        borra_detalle()
                '        cone.Open()
                '        With comando2
                '            .Connection = cone
                '            .CommandTimeout = 0
                '            .CommandText = "Guarda_DetalleNota"
                '            .CommandType = CommandType.StoredProcedure
                '            Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                '            prm.Direction = ParameterDirection.Input
                '            prm.Value = Me.ComboBox3.SelectedValue
                '            .Parameters.Add(prm)
                '            Dim i As Integer = comando2.ExecuteNonQuery
                '        End With
                '        cone.Close()
                '    End If

                '    borra_Predetalle(Me.ComboBox3.SelectedValue)
                'End If
                CancelaEdoDCuentaPorNotas(CLng(Me.ComboBox3.SelectedValue))
                If Me.RadioButtonSi.Checked = True Then
                    eAplicada = 1
                Else
                    eAplicada = 0
                End If
                NueRelEstadoDeCuentaAplicada(Me.Clv_NotadeCreditoTextBox.Text, CLng(Me.ComboBox3.SelectedValue), eAplicada)
                
                MsgBox("Se ha Guardado con �xito")
                guardabitacora()
                refrescar = True
                Me.Close()
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub borra_detalle()
        Dim conLidia2 As New SqlClient.SqlConnection(MiConexion)
        Dim comando4 As New SqlClient.SqlCommand(MiConexion)
        conLidia2.Open()
        With comando4
            .Connection = conLidia2
            .CommandTimeout = 0
            .CommandText = "Borra_DetFactura_nota"
            .CommandType = CommandType.StoredProcedure
            Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm.Value = Factura_inicial
            .Parameters.Add(prm)
            Dim i As Integer = comando4.ExecuteNonQuery
        End With
        conLidia2.Close()
    End Sub
    Private Sub modifica_PreDetalle(ByVal factura2 As Integer, ByVal value As Integer)
        Dim conlidia As New SqlClient.SqlConnection(MiConexion)
        Dim comando As New SqlClient.SqlCommand
        conlidia.Open()
        With comando
            .Connection = conlidia
            .CommandText = "Modifica_DetalleNotas "
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0
            ' Create a SqlParameter for each parameter in the stored procedure.
            Dim prm As New SqlParameter("@Clv_factura", SqlDbType.BigInt)
            Dim prm1 As New SqlParameter("@Value", SqlDbType.Int)
            Dim prm2 As New SqlParameter("@Clv_detalle", SqlDbType.Int)
            prm.Direction = ParameterDirection.Input
            prm1.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Input

            prm.Value = factura2
            prm1.Value = value
            prm2.Value = Me.TextBox2.Text
            .Parameters.Add(prm2)
            .Parameters.Add(prm)
            .Parameters.Add(prm1)
            Dim i As Integer = comando.ExecuteNonQuery()
        End With
        conlidia.Close()
    End Sub
    Private Sub Calcula_monto(ByVal fac As Long, ByVal opc As Integer)
        Dim x As Double
        Dim conlidia As New SqlClient.SqlConnection(MiConexion)
        Dim comando As New SqlClient.SqlCommand
        conlidia.Open()
        With comando
            .Connection = conlidia
            .CommandText = "Calcula_monto "
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0
            ' Create a SqlParameter for each parameter in the stored procedure.
            Dim prm As New SqlParameter("@Clv_factura", SqlDbType.BigInt)
            Dim prm2 As New SqlParameter("@Monto", SqlDbType.Money)
            Dim prm3 As New SqlParameter("@Opc", SqlDbType.Int)
            prm.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Output
            prm3.Direction = ParameterDirection.Input

            prm.Value = fac
            prm2.Value = 0
            prm3.Value = opc
            .Parameters.Add(prm)
            .Parameters.Add(prm2)
            .Parameters.Add(prm3)
            Dim i As Integer = comando.ExecuteNonQuery()
            x = prm2.Value
            Me.TextBox7.Text = Format(x, "##,##0.00")
            Me.MontoTextBox.Text = 0
            CalculaSaldo(fac, Me.MontoTextBox.Text)
        End With
        conlidia.Close()
    End Sub

    Private Sub CalculaSaldo(ByVal Clv_Factura As Long, ByVal Monto As Decimal)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("CalculaSaldo", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Factura
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Monto", SqlDbType.Money)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Monto
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Saldo", SqlDbType.Money)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Res", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Output
        parametro4.Value = 0
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro5.Direction = ParameterDirection.Output
        parametro5.Value = ""
        comando.Parameters.Add(parametro5)

        Try
            eSaldo = 0
            eRes = 0
            eMsg = String.Empty
            conexion.Open()
            comando.ExecuteNonQuery()
            eSaldo = parametro3.Value
            eRes = parametro4.Value
            eMsg = parametro5.Value
            conexion.Close()
            If eRes = 1 Then
                Me.MontoTextBox.Clear()
                Me.TextBox1.Clear()
                MsgBox(eMsg, MsgBoxStyle.Information)
            Else
                Me.TextBox1.Text = Format(eSaldo, "##,##0.00")
            End If
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

    End Sub


    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged
        If IsNumeric(factura) = True Then
            'borra_Predetalle(factura)
            'borra_Predetalle(Me.ComboBox3.SelectedValue)
        End If
        factura = Me.ComboBox3.SelectedValue
        If IsNumeric(Me.ComboBox3.SelectedValue) = True Then
            'Muestra_Predetalle(Me.ComboBox3.SelectedValue)
            Calcula_monto(Me.ComboBox3.SelectedValue, 0)
            'Me.TextBox1.Text = CDec(Me.MontoTextBox.Text)
        End If
    End Sub

    Private Sub Detalle_NotasdeCreditoDataGridView_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Detalle_NotasdeCreditoDataGridView.CellClick
        'Try

        '    Dim bol As String
        '    If e.ColumnIndex = 0 Then
        '        bol = Me.Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells(0).Value.ToString
        '        If Me.Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells(0).Value.ToString = "False" Then
        '            Me.Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells(0).Value = True
        '            modifica_PreDetalle(Me.ComboBox3.SelectedValue, 1)
        '            Calcula_monto(Me.ComboBox3.SelectedValue, 0)
        '            'If Me.ComboBox3.Enabled = True Then
        '            Me.TextBox1.Text = CDec(Me.MontoTextBox.Text)
        '            'End If
        '        ElseIf Me.Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells(0).Value.ToString = "True" Then
        '            Me.Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells(0).Value = False
        '            modifica_PreDetalle(Me.ComboBox3.SelectedValue, 0)
        '            Calcula_monto(Me.ComboBox3.SelectedValue, 0)
        '            'If Me.ComboBox3.Enabled = True Then
        '            Me.TextBox1.Text = CDec(Me.MontoTextBox.Text)
        '            'End If
        '        End If
        '    End If

        'Catch ex As Exception
        '    Exit Sub
        'End Try
    End Sub

    Private Sub TextBox4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox4.TextChanged
        factura = Me.TextBox4.Text
    End Sub
    Private Sub ComboBox7_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox7.SelectedIndexChanged
        Dim conLidia As New SqlClient.SqlConnection(MiConexion)
        conLidia.Open()
        Me.MUESTRACAJAS2TableAdapter.Connection = conLidia
        Me.MUESTRACAJAS2TableAdapter.Fill(Me.DataSetLydia.MUESTRACAJAS2, Me.ComboBox7.SelectedValue)
        conLidia.Close()
        If OPCION = "N" And (IdSistema <> "VA" Or IdSistema = "LO") Then
            Me.ComboBox8.Text = ""
        End If

    End Sub

    Private Sub ComboBox8_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox8.SelectedIndexChanged
        Dim conLidia2 As New SqlClient.SqlConnection(MiConexion)
        conLidia2.Open()
        Me.MUESTRACAJERASTableAdapter.Connection = conLidia2
        Me.MUESTRACAJERASTableAdapter.Fill(Me.DataSetLydia.MUESTRACAJERAS, 0)
        conLidia2.Close()
        If OPCION = "N" And (IdSistema <> "VA" Or IdSistema = "LO") Then
            Me.ComboBox5.Text = ""
        End If
    End Sub


    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox5.TextChanged
        If IsNumeric(factura) = True Then
            borra_Predetalle(factura)
            borra_Predetalle(Me.TextBox5.Text)
        End If
        factura = Me.TextBox5.Text
        If IsNumeric(Me.TextBox5.Text) = True Then
            Muestra_Predetalle(Me.TextBox5.Text)
            Calcula_monto(Me.TextBox5.Text, 0)
            Me.TextBox1.Text = CDec(Me.MontoTextBox.Text)
        End If
    End Sub


    Private Sub TextBox6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox6.TextChanged
        Dim conlidia As New SqlClient.SqlConnection(MiConexion)
        conlidia.Open()
        Me.MUESTRACAJERASTableAdapter.Connection = conlidia
        Me.MUESTRACAJERASTableAdapter.Fill(Me.DataSetLydia.MUESTRACAJERAS, 0)
        Me.ComboBox5.SelectedValue = Me.TextBox6.Text
        conlidia.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Panel1.Enabled = True
        Me.Panel3.Enabled = True
        Me.Button5.Enabled = True
        Me.Panel3.Visible = True

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Panel1.Enabled = False
        Me.Panel3.Visible = False
    End Sub

    Private Sub ComboBox9_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox9.SelectedIndexChanged
        Dim conFact As New SqlConnection(MiConexion)

        If IsNumeric(Me.ComboBox9.SelectedValue) = True Then
            conFact.Open()
            Me.MuestraServicios_por_TipoTableAdapter.Connection = conFact
            Me.MuestraServicios_por_TipoTableAdapter.Fill(Me.DataSetLydia.MuestraServicios_por_Tipo, Me.ComboBox9.SelectedValue)
            conFact.Close()
        End If
    End Sub

    Private Sub ComboBox10_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox10.SelectedIndexChanged
        If IsNumeric(Me.ComboBox10.SelectedValue) = True Then
            Dame_Datos_Servicio(Me.ComboBox10.SelectedValue)
        End If
    End Sub
    Private Sub Dame_Datos_Servicio(ByVal clv_serv As Integer)
        Dim confact2 As New SqlConnection(MiConexion)

        Dim Cmd As New SqlCommand
        Dim reader As SqlDataReader
        confact2.Open()
        With Cmd
            .CommandText = "Dame_DatosServicio"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = confact2
            Dim prm As New SqlParameter("@clv_servicio", SqlDbType.Int)
            Dim prm2 As New SqlParameter("@clv_tipser", SqlDbType.Int)
            prm.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Input
            prm.Value = clv_serv
            prm2.Value = Me.ComboBox9.SelectedValue
            .Parameters.Add(prm)
            .Parameters.Add(prm2)
            reader = .ExecuteReader()
            Using reader
                While reader.Read
                    clave_txt = reader.GetValue(0)
                    Me.TextBox9.Text = reader.GetValue(1)
                End While
            End Using

        End With
        confact2.Close()

    End Sub
    Private Sub busca_detalleNota()
        Dim ConFact4 As New SqlConnection(MiConexion)
        Dim Cmd4 As New SqlCommand
        ConFact4.Open()
        Me.MuestraDetalleNota_por_ConceptoTableAdapter.Connection = ConFact4
        Me.MuestraDetalleNota_por_ConceptoTableAdapter.Fill(Me.DataSetLydia.MuestraDetalleNota_por_Concepto, Me.Clv_NotadeCreditoTextBox.Text)
        ConFact4.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Agregar_Conceptos(0)
        busca_detalleNota()
        Calcula_monto(Me.Clv_NotadeCreditoTextBox.Text, 1)
    End Sub
    Private Sub Agregar_Conceptos(ByVal opc As Integer)
        Dim conFact3 As New SqlConnection(MiConexion)
        Dim Cmd3 As New SqlCommand
        conFact3.Open()
        With Cmd3
            .CommandText = "Agregar_Conceptos_X_Nota"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = conFact3
            Dim prm1 As New SqlParameter("@clv_NotaCredito", SqlDbType.BigInt)
            Dim prm2 As New SqlParameter("@clv_Servicio", SqlDbType.Int)
            Dim prm3 As New SqlParameter("@clv_Tipser", SqlDbType.Int)
            Dim prm4 As New SqlParameter("@Precio", SqlDbType.Money)
            Dim prm5 As New SqlParameter("@opc", SqlDbType.Int)
            prm1.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Input
            prm3.Direction = ParameterDirection.Input
            prm4.Direction = ParameterDirection.Input
            prm5.Direction = ParameterDirection.Input
            prm1.Value = Me.Clv_NotadeCreditoTextBox.Text
            prm2.Value = Me.ComboBox10.SelectedValue
            prm3.Value = Me.ComboBox9.SelectedValue
            prm4.Value = Me.TextBox9.Text
            prm5.Value = opc
            .Parameters.Add(prm1)
            .Parameters.Add(prm2)
            .Parameters.Add(prm3)
            .Parameters.Add(prm4)
            .Parameters.Add(prm5)
            Dim i As Integer = .ExecuteNonQuery
        End With
        conFact3.Close()

    End Sub
    Private Sub Borra_conceptos()
        Dim conFact5 As New SqlConnection(MiConexion)
        Dim Cmd5 As New SqlCommand
        conFact5.Open()
        With Cmd5
            .CommandText = "Borrar_Conceptos_X_Nota"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = conFact5
            Dim prm1 As New SqlParameter("@clv_Detalle", SqlDbType.BigInt)
            prm1.Direction = ParameterDirection.Input
            prm1.Value = Me.TextBox10.Text
            .Parameters.Add(prm1)
            Dim i As Integer = .ExecuteNonQuery
        End With
        conFact5.Close()
    End Sub
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Borra_conceptos()
        busca_detalleNota()
        Calcula_monto(Me.Clv_NotadeCreditoTextBox.Text, 1)
    End Sub

    Private Sub TextBox9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox9.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Agregar_Conceptos(0)
            busca_detalleNota()
            Calcula_monto(Me.Clv_NotadeCreditoTextBox.Text, 1)
        End If
    End Sub

    Private Sub MontoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MontoTextBox.TextChanged
        If OPCION <> "N" Then
            Exit Sub
        End If
        If IsNumeric(Me.MontoTextBox.Text) = False Or IsNumeric(Me.ComboBox3.SelectedValue) = False Then
            Me.MontoTextBox.Text = String.Empty
            Me.TextBox1.Text = String.Empty
            Exit Sub
        End If
        CalculaSaldo(Me.ComboBox3.SelectedValue, Me.MontoTextBox.Text)
    End Sub

    Private Sub CancelaEdoDCuentaPorNotas(ByVal Clv_Proceso As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("CancelaEdoDCuentaPorNotas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Proceso", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Proceso
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ConRelEstadoDeCuentaAplicada(ByVal Clv_NotaDeCredito As Long, ByVal Clv_Proceso As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConRelNotaDeCreditoAplicada", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_NotaDeCredito", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_NotaDeCredito
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Proceso", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Proceso
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Aplicada", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            If CInt(parametro3.Value) = 1 Then
                Me.RadioButtonSi.Checked = True
            Else
                Me.RadioButtonNo.Checked = True
            End If
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub NueRelEstadoDeCuentaAplicada(ByVal Clv_NotaDeCredito As Long, ByVal Clv_Proceso As Long, ByVal Aplicada As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRelNotaDeCreditoAplicada", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_NotaDeCredito", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_NotaDeCredito
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Proceso", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Proceso
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Aplicada", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Aplicada
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

    End Sub


End Class
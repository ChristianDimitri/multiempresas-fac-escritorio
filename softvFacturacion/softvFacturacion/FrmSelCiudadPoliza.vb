Imports System.Data.SqlClient

Public Class FrmSelCiudadPoliza
    Private Sub DameClv_Session_Servicios()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameClv_Session_Servicios")
        gloClv_Session = BaseII.dicoPar("@Clv_Session").ToString()
    End Sub
    Private Sub CONTABLA_POLIZACiudadTmp(ByVal Clv_Session As Integer, ByVal Clv_Compania As Integer, ByVal Op As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Clv_Compania", SqlDbType.Int, Clv_Compania)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, Op)
        If Op = 0 Then
            dgvIzq.DataSource = BaseII.ConsultaDT("CONTABLA_POLIZACiudadTmp")
        ElseIf Op = 1 Then
            dgvDer.DataSource = BaseII.ConsultaDT("CONTABLA_POLIZACiudadTmp")
        End If
    End Sub
    Private Sub NUETABLA_POLIZACiudadTmp(ByVal Clv_Session As Integer, ByVal Op As Integer, ByVal Clv_Ciudad As Integer, ByVal Clv_Compania As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_ciudad", SqlDbType.Int, Clv_Ciudad)
        BaseII.CreateMyParameter("@Clv_Compania", SqlDbType.Int, Clv_Compania)
        BaseII.Inserta("NUETABLA_POLIZACiudadTmp")
    End Sub
    Private Sub BORTABLA_POLIZACiudadTmp(ByVal Clv_Session As Integer, ByVal Op As Integer, ByVal Clv_Ciudad As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, Clv_Ciudad)
        BaseII.Inserta("BORTABLA_POLIZACiudadTmp")
    End Sub
    Private Sub FrmSelCiudad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            colorea(Me)
            gloClv_Session = 0
            DameClv_Session_Servicios()
            CONTABLA_POLIZACiudadTmp(gloClv_Session, GloClvCompania, 0)
            CONTABLA_POLIZACiudadTmp(gloClv_Session, GloClvCompania, 1)

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            If dgvDer.RowCount = 0 Then Exit Sub
            BORTABLA_POLIZACiudadTmp(gloClv_Session, 1, 0)
            CONTABLA_POLIZACiudadTmp(gloClv_Session, GloClvCompania, 0)
            CONTABLA_POLIZACiudadTmp(gloClv_Session, GloClvCompania, 1)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Try
            If dgvDer.RowCount = 0 Then Exit Sub
            BORTABLA_POLIZACiudadTmp(gloClv_Session, 0, dgvDer.SelectedCells(0).Value)
            CONTABLA_POLIZACiudadTmp(gloClv_Session, GloClvCompania, 0)
            CONTABLA_POLIZACiudadTmp(gloClv_Session, GloClvCompania, 1)

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            If dgvIzq.RowCount = 0 Then Exit Sub
            NUETABLA_POLIZACiudadTmp(gloClv_Session, 0, dgvIzq.SelectedCells(0).Value, GloClvCompania)
            CONTABLA_POLIZACiudadTmp(gloClv_Session, GloClvCompania, 0)
            CONTABLA_POLIZACiudadTmp(gloClv_Session, GloClvCompania, 1)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            If dgvIzq.RowCount = 0 Then Exit Sub
            NUETABLA_POLIZACiudadTmp(gloClv_Session, 1, 0, GloClvCompania)
            CONTABLA_POLIZACiudadTmp(gloClv_Session, GloClvCompania, 0)
            CONTABLA_POLIZACiudadTmp(gloClv_Session, GloClvCompania, 1)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'MsgBox(Me.DataGridView2.RowCount)

        If Me.dgvDer.RowCount > 0 Then
            If GloBnd_Des_Men = True Then
                FrmFechaIngresosConcepto.Show()
                Me.Close()
            ElseIf LocbndPolizaCiudad = True Then
                FrmFechaIngresosConcepto.Show()
                Me.Close()
            Else
                GloBnd_Des_Men = False
                GloBnd_Des_Cont = False
                LocbndPolizaCiudad = False
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.Dame_ciudad_carteraTableAdapter.Connection = CON
                Me.Dame_ciudad_carteraTableAdapter.Fill(Me.Procedimientos_arnoldo.Dame_ciudad_cartera, gloClv_Session, LocCiudades)
                CON.Close()
                FrmFechaIngresosConcepto.Show()
                Me.Close()
            End If
        Else
            MsgBox("Seleccione alguna Ciudad ", MsgBoxStyle.Information)
        End If
    End Sub

End Class
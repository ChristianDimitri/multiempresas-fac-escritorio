﻿Public Class BrwCancelacionArqueos

    Private Sub BrwCancelacionArqueos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)

        '''''LLENAMOS EL COMBO DEL USUARIO QUE AUTORIZA (INICIO)
        Me.cmbBuscaAutoriza.DisplayMember = "nombre"
        Me.cmbBuscaAutoriza.ValueMember = "clvUsuario"
        Me.cmbBuscaAutoriza.DataSource = LlenaCombosUsuarios(1)
        '''''LLENAMOS EL COMBO DEL USUARIO QUE AUTORIZA (FIN)

        '''''LLENAMOS EL COMBO DEL CAJERO(A) (INICIO)
        Me.cmbBuscaCajera.DisplayMember = "nombre"
        Me.cmbBuscaCajera.ValueMember = "clvUsuario"
        Me.cmbBuscaCajera.DataSource = LlenaCombosUsuarios(0)
        '''''LLENAMOS EL COMBO DEL CAJERO(A) (FIN)

        '''''LLENAMOS EL COMBO DEL STATUS (INICIO)
        uspLlenaComboStatus()
        '''''LLENAMOS EL COMBO DEL STATUS (INICIO)

        '''''LLENAMOS EL GRID (INICIO)
        llenaGrid(0, "T", "TODOS", "TODOS")
        '''''LLENAMOS EL GRID (FIN)

        If Me.dgvCortesGlobales.RowCount > 0 Then
            Try
                Me.CMBlblIdCorte.Text = Me.dgvCortesGlobales.SelectedCells(0).Value
                Me.CMBlblTotalEfectivo.Text = CDec(Me.dgvCortesGlobales.SelectedCells(4).Value).ToString("#.00")
                Me.CMBlblTotalTarjeta.Text = CDec(Me.dgvCortesGlobales.SelectedCells(5).Value).ToString("#.00")
                Me.CMBlblTotalCheques.Text = CDec(Me.dgvCortesGlobales.SelectedCells(6).Value).ToString("#.00")
                Me.CMBlblEntregasPaciales.Text = CDec(Me.dgvCortesGlobales.SelectedCells(7).Value).ToString("#.00")
                Me.CMBlblGastos.Text = CDec(Me.dgvCortesGlobales.SelectedCells(9).Value).ToString("#.00")
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Function LlenaCombosUsuarios(ByVal prmOp As Integer) As DataTable
        '''''MANDAMOS LLAMAR EL MÉTODO QUE NOS DEVUELVE  UN DATA TABLE PARA LLENAR LOS COMBOS (INICIO)
        ControlEfectivoClass.limpiaParametros()
        ControlEfectivoClass.CreateMyParameter("@op", SqlDbType.Int, prmOp)
        LlenaCombosUsuarios = ControlEfectivoClass.ConsultaDT("uspLlenaComboUsuarios")
        '''''MANDAMOS LLAMAR EL MÉTODO QUE NOS DEVUELVE  UN DATA TABLE PARA LLENAR LOS COMBOS (FIN)
    End Function

    Private Sub llenaGrid(ByVal prmIdCorte As Long, ByVal prmStatus As String, ByVal prmCajera As String, ByVal prmAutoriza As String)
        '''''REALIZAMOS LAS MÚLTIPLES BÚSQUEDAS QUE LLENAN EL GRID (INICIO)
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@idCorte", SqlDbType.BigInt, prmIdCorte)
        ControlEfectivoClass.CreateMyParameter("@status", SqlDbType.VarChar, prmStatus, 1)
        ControlEfectivoClass.CreateMyParameter("@cajera", SqlDbType.VarChar, prmCajera, 5)
        ControlEfectivoClass.CreateMyParameter("@autoriza", SqlDbType.VarChar, prmAutoriza, 5)

        Me.dgvCortesGlobales.DataSource = ControlEfectivoClass.ConsultaDT("uspConsultaCortesGlobales")
        '''''REALIZAMOS LAS MÚLTIPLES BÚSQUEDAS QUE LLENAN EL GRID (FIN)
    End Sub

    Private Sub uspLlenaComboStatus()
        '''''LLENAMOS EL COMBO DE LOS ESTATUS (INICIO)
        ControlEfectivoClass.limpiaParametros()
        Me.cmbBuscaStatus.DisplayMember = "descripcion"
        Me.cmbBuscaStatus.ValueMember = "status"
        Me.cmbBuscaStatus.DataSource = ControlEfectivoClass.ConsultaDT("uspLlenaComboStatus")
        '''''LLENAMOS EL COMBO DE LOS ESTATUS (FIN)
    End Sub

    Private Sub uspCancelaCortesGlobales(ByVal prmIdCorte As Long)
        '''''EJECUTAMOS EL PROCEDIMIENTO QUE CANCELA LOS CORTES (INICIO)
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@idCorte", SqlDbType.BigInt, prmIdCorte)
        ControlEfectivoClass.Inserta("uspCancelaCortesGlobales")

        MsgBox("Registro Cancelado Satisfactoriamente", MsgBoxStyle.Information)
        '''''EJECUTAMOS EL PROCEDIMIENTO QUE CANCELA LOS CORTES (FIN)
    End Sub

    Private Sub dgvCortesGlobales_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvCortesGlobales.CurrentCellChanged
        If Me.dgvCortesGlobales.RowCount > 0 Then
            Try
                Me.CMBlblIdCorte.Text = Me.dgvCortesGlobales.SelectedCells(0).Value
                Me.CMBlblTotalEfectivo.Text = CDec(Me.dgvCortesGlobales.SelectedCells(4).Value).ToString("#.00")
                Me.CMBlblTotalTarjeta.Text = CDec(Me.dgvCortesGlobales.SelectedCells(5).Value).ToString("#.00")
                Me.CMBlblTotalCheques.Text = CDec(Me.dgvCortesGlobales.SelectedCells(6).Value).ToString("#.00")
                Me.CMBlblEntregasPaciales.Text = CDec(Me.dgvCortesGlobales.SelectedCells(7).Value).ToString("#.00")
                Me.CMBlblGastos.Text = CDec(Me.dgvCortesGlobales.SelectedCells(9).Value).ToString("#.00")
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub cmbBuscaAutoriza_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBuscaAutoriza.SelectedIndexChanged
        If Me.cmbBuscaCajera.Items.Count > 0 Then
            llenaGrid(0, Me.cmbBuscaStatus.SelectedValue, Me.cmbBuscaCajera.SelectedValue, Me.cmbBuscaAutoriza.SelectedValue)
        End If
    End Sub

    Private Sub cmbBuscaCajera_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBuscaCajera.SelectedIndexChanged
        If Me.cmbBuscaStatus.Items.Count > 0 Then
            llenaGrid(0, Me.cmbBuscaStatus.SelectedValue, Me.cmbBuscaCajera.SelectedValue, Me.cmbBuscaAutoriza.SelectedValue)
        End If
    End Sub

    Private Sub cmbBuscaStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBuscaStatus.SelectedIndexChanged
        llenaGrid(0, Me.cmbBuscaStatus.SelectedValue, Me.cmbBuscaCajera.SelectedValue, Me.cmbBuscaAutoriza.SelectedValue)
    End Sub

    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        If Me.dgvCortesGlobales.RowCount > 0 Then
            Dim frm As New FrmGuardaArqueos
            frm.IdCorte = Me.dgvCortesGlobales.SelectedCells(0).Value
            frm.Show()
        Else
            MsgBox("Seleccione al menos un Registro", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        If Me.dgvCortesGlobales.RowCount > 0 Then

            'If Me.dgvCortesGlobales.SelectedCells(10).Value = "C" Then ''''''SE VALIDA QUE EL REGISTRO A CANCELAR NO ESTÉ CANCELADO ACTUALMENTE
            '    MsgBox("El Registro Seleccionado ya ha sido Cancelado", MsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'If CDate(Me.dgvCortesGlobales.SelectedCells(3).Value) <> Today Then '''''SE VALIDA EL REGISTRO A CANCELAR SE DE LA FECHA DEL DÍA ACTUAL
            '    MsgBox("No puede Cancelar Cortes de Fechas Anteriores", MsgBoxStyle.Information)
            '    Exit Sub
            'End If

            Dim res = MsgBox("¿Realmente Deseas Cancelar el Registro Selecionado?", MsgBoxStyle.YesNo)

            If res = MsgBoxResult.Yes Then
                uspCancelaCortesGlobales(Me.dgvCortesGlobales.SelectedCells(0).Value)
                bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Se Cancelo Arqueo Final #" + CStr(Me.dgvCortesGlobales.SelectedCells(0).Value) + " del Usuario: " + CStr(Me.dgvCortesGlobales.SelectedCells(1).Value) + " de la Fecha: " + CStr(Me.dgvCortesGlobales.SelectedCells(3).Value), "", SubCiudad)
            End If
        Else
            MsgBox("Seleccione al menos un Registro", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
End Class
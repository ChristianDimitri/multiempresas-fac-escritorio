﻿Public Class FrmPerfilesSiste
    Dim IDMENU As Integer
    Dim IDFORMULARIO As Integer
    Dim IDBOTON As Integer
    Private Sub FrmPerfilesSiste_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        UspLlenaOpcionesAConfigurar()
        UspLlenaArbolMenuTMP()
        UspMuestraMenusNoVisibles()
        UspMuestraMenusVisibles()
    End Sub

    Private Sub UspLlenaOpcionesAConfigurar()
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            DT = BaseII.ConsultaDT("UspLlenaOpcionesAConfigurarFac")
            If DT.Rows.Count > 0 Then
                ComboBox1.DataSource = DT
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If ComboBox1.SelectedValue = 1 Then
            Me.Panel1.Visible = False
            UspLlenaArbolMenuTMP()
            UspMuestraMenusNoVisibles()
            UspMuestraMenusVisibles()
        Else
            Me.Panel1.Visible = True
            UspLlenaFormulariosSISTE()

        End If
    End Sub

    Private Sub UspMuestraMenusNoVisibles()
        Try
            BaseII.limpiaParametros()
            Me.ListBox1.DataSource = BaseII.ConsultaDT("UspMuestraMenusNoVisiblesFac")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraMenusVisibles()
        Try
            BaseII.limpiaParametros()
            Me.ListBox2.DataSource = BaseII.ConsultaDT("UspMuestraMenusVisiblesFac")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaArbolMenuTMP()
        Try
            BaseII.limpiaParametros()
            BaseII.Inserta("UspLlenaArbolMenuTMPFac")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspSeleccionaUnMenu()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IDMENU", SqlDbType.Int, IDMENU)
            BaseII.Inserta("UspSeleccionaUnMenuFac")
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub UspDeseleccionaUnMenu()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IDMENU", SqlDbType.Int, IDMENU)
            BaseII.Inserta("UspDeseleccionaUnMenuFac")
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub UspDeseleccionaTodoMenu()
        Try
            BaseII.limpiaParametros()
            BaseII.Inserta("UspDeseleccionaTodoMenuFac")
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub UspSeleccionaTodoMenu()
        Try
            BaseII.limpiaParametros()
            BaseII.Inserta("UspSeleccionaTodoMenuFac")
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        IDMENU = CInt(Me.ListBox1.SelectedValue)
        UspSeleccionaUnMenu()
        UspMuestraMenusNoVisibles()
        UspMuestraMenusVisibles()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        IDMENU = CInt(Me.ListBox2.SelectedValue)
        UspDeseleccionaUnMenu()
        UspMuestraMenusNoVisibles()
        UspMuestraMenusVisibles()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        UspSeleccionaTodoMenu()
        UspMuestraMenusNoVisibles()
        UspMuestraMenusVisibles()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        UspDeseleccionaTodoMenu()

        UspMuestraMenusVisibles()
        UspMuestraMenusNoVisibles()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub ListBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox3.SelectedIndexChanged
        IDFORMULARIO = CInt(Me.ListBox3.SelectedValue)
        Me.TextBox1.Text = ""
        UspLlenaGridBotonesSISTE()
        UspEsVisibleFormulario()

    End Sub

    Private Sub UspLlenaFormulariosSISTE()
        Try
            BaseII.limpiaParametros()
            Me.ListBox3.DataSource = BaseII.ConsultaDT("UspLlenaFormulariosSISTEFac")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaGridBotonesSISTE()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IDFORMULARIO", SqlDbType.Int, IDFORMULARIO)
            DataGridView1.DataSource = BaseII.ConsultaDT("UspLlenaGridBotonesSISTEFac")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspGuardaInformaciónBotones()
        Try
            Dim I As Integer
            Dim VISIBLEFORM As Integer
            Dim VISIBLEBOTON As Integer
            Dim TEXTOBOTON As String
            For I = 0 To DataGridView1.Rows.Count - 1
                If Me.CheckBox1.Checked = True Then
                    VISIBLEFORM = 1
                Else
                    VISIBLEFORM = 0
                End If

                If Me.DataGridView1(2, I).Value.ToString = True Then
                    VISIBLEBOTON = 1
                Else
                    VISIBLEBOTON = 0
                End If

                IDBOTON = CInt(Me.DataGridView1(0, I).Value.ToString)
                TEXTOBOTON = Me.DataGridView1(1, I).Value.ToString

                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@VISIBLEFORM", SqlDbType.Int, VISIBLEFORM)
                BaseII.CreateMyParameter("@VISIBLEBOTONES", SqlDbType.Int, VISIBLEBOTON)
                BaseII.CreateMyParameter("@IDBOTON", SqlDbType.Int, IDBOTON)
                BaseII.CreateMyParameter("@IDFORMULARIO", SqlDbType.Int, IDFORMULARIO)
                BaseII.CreateMyParameter("@TEXTOBOTON", SqlDbType.VarChar, TEXTOBOTON, 50)
                BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, TextBox1.Text, 250)
                BaseII.Inserta("UspGuardaInformaciónBotonesFac")

            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        IDFORMULARIO = CInt(Me.ListBox3.SelectedValue)
        UspGuardaInformaciónBotones()
        UspLlenaGridBotonesSISTE()
        MsgBox("Fue guardado con exito", MsgBoxStyle.Information, "Perfiles")
    End Sub
    Private Sub UspEsVisibleFormulario()
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IDFORMULARIO", SqlDbType.Int, IDFORMULARIO)
            DT = BaseII.ConsultaDT("UspEsVisibleFormularioFac")
            If DT.Rows.Count > 0 Then
                If DT.Rows(0)(0).ToString = True Then
                    Me.CheckBox1.Checked = True
                Else
                    Me.CheckBox1.Checked = False
                End If
            End If
            If Me.CheckBox1.Checked = True Then

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
﻿Public Class FrmEntregasGlobales

    Public prmOpcion As String
    Public prmClaveEntrega As Integer
    Public prmImporteAnterior As Decimal
    Public prmIdEntrega As Long

    Private Sub FrmEntregasGlobales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)

        If prmOpcion = "N" Then
            '''''SE ESTABLECE COMO VALOR PRINCIPAL DEL DTP EL DÍA QUE SE ESTÉ INGRESANDO CUANDO SEA UN NUEVO REGISTRO (INICIO)
            Me.dtpFecha.Value = Today
            Me.dtpFecha.Enabled = False
            Me.txtUsuario.Text = GloUsuario
            '''''SE ESTABLECE COMO VALOR PRINCIPAL DEL DTP EL DÍA QUE SE ESTÉ INGRESANDO CUANDO SEA UN NUEVO REGISTRO (FIN)
        End If

        '''''SE HABILITA EL CAMPO DE LA FECHA PARA CONTROL TOTA Y ENCARGADO DE SUCURSAL (INICIO)
        If GloTipoUsuario = 40 Or GloTipoUsuario = 1 Then
            Me.dtpFecha.Enabled = True
        Else
            Me.dtpFecha.Enabled = False
        End If
        '''''SE HABILITA EL CAMPO DE LA FECHA PARA CONTROL TOTA Y ENCARGADO DE SUCURSAL (FIN)

        If prmOpcion = "C" Then '''''SE BLOQUEAN LOS CAMPOS PARA CUANDO SÓLO SE VA A CONSULTAR
            Me.txtDescripcion.ReadOnly = True
            Me.txtImporte.ReadOnly = True
            Me.bnEntregasGlobalesGuargar.Enabled = False
            Me.dtpFecha.Enabled = False
        Else '''''EN CASO CONTRARIO QUE NO SEA CONSULTA SE HABILITAN LOS CAMPOS
            Me.txtDescripcion.ReadOnly = False
            Me.txtImporte.ReadOnly = False
            Me.bnEntregasGlobalesGuargar.Enabled = True
        End If

        If prmOpcion = "M" Or prmOpcion = "C" Then '''''SE OBTIENEN LOS DATOS DEL CLIENTE CUANDO ES UNA CONSULTA O MODIFICACIÓN
            LlenaCampos(prmClaveEntrega, "TODOS", "", "T", GloUsuario)
            uspConsultaRelEntregaGlobalDesglose(prmClaveEntrega)
        End If

        '''''SE REALIZA EL CÁLCULO DEL EFECTIVO COBRADO DEL DÍA (INICIO)
        uspCalculaEfectivo(GloUsuario, Me.dtpFecha.Value)
        '''''SE REALIZA EL CÁLCULO DEL EFECTIVO COBRADO DEL DÍA (FIN)

        '''''CUANDO SEA UNA MODIFICACIÓN SE SUMA EL IMPORTE DE LA ENTREGA REALIZADA MÁS EL EFECTIVO DISPONIBLE PARA PODER REALIZAR DICHO PROCESO
        If prmOpcion = "M" Then
            Me.txtEfectivoCobrado.Text = CDec(Me.txtEfectivoCobrado.Text) + CDec(Me.txtImporte.Text)
        End If
        '''''CUANDO SEA UNA MODIFICACIÓN SE SUMA EL IMPORTE DE LA ENTREGA REALIZADA MÁS EL EFECTIVO DISPONIBLE PARA PODER REALIZAR DICHO PROCESO
    End Sub

    Private Sub LlenaCampos(ByVal prmIdEntrega As Long, ByVal prmCajera As String, ByVal prmDescripcion As String, ByVal prmStatus As String, ByVal prmLoginUsuario As String)
        '''''LLENAMOS LOS CAMPOS CON LA INFORMACIÓN CORRESPONDIENTE (INICIO)
        Dim DT As New DataTable
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@idEntrega", SqlDbType.BigInt, prmIdEntrega)
        ControlEfectivoClass.CreateMyParameter("@cajera", SqlDbType.VarChar, prmCajera, 5)
        ControlEfectivoClass.CreateMyParameter("@descripcion", SqlDbType.VarChar, prmDescripcion, 250)
        ControlEfectivoClass.CreateMyParameter("@status", SqlDbType.VarChar, prmStatus, 1)
        ControlEfectivoClass.CreateMyParameter("@loginUsuario", SqlDbType.VarChar, prmLoginUsuario, 5)

        DT = ControlEfectivoClass.ConsultaDT("uspConsultaEntregasGlobales")
        Me.txtClaveEntrega.Text = DT.Rows(0)("idEntrega").ToString
        Me.dtpFecha.Value = DT.Rows(0)("fecha").ToString
        Me.txtUsuario.Text = DT.Rows(0)("clvUsuario").ToString
        Me.txtDescripcion.Text = DT.Rows(0)("descripcion").ToString
        Me.txtImporte.Text = CDec(DT.Rows(0)("importe")).ToString("#.00")
        '''''LLENAMOS LOS CAMPOS CON LA INFORMACIÓN CORRESPONDIENTE (FIN)
    End Sub

    '''''MÉTODO QUE EJECUTA EL PROCEDIMIENTO DE INSERCIÓN DE LOS DATOS A LA TABLA DE LOS GASTOS
    Private Sub INSERTAR(ByVal prmCajera As String, ByVal prmDescripcion As String, ByVal prmImporte As Decimal)
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@clvUsuario", SqlDbType.VarChar, prmCajera, 5)
        ControlEfectivoClass.CreateMyParameter("@descripcion", SqlDbType.VarChar, prmDescripcion, 250)
        ControlEfectivoClass.CreateMyParameter("@importe", SqlDbType.Money, prmImporte)
        ControlEfectivoClass.CreateMyParameter("@idEntrega", ParameterDirection.Output, SqlDbType.BigInt)

        ControlEfectivoClass.ProcedimientoOutPut("uspInsertaEntregasGlobales")
        prmIdEntrega = CLng(ControlEfectivoClass.dicoPar("@idEntrega").ToString())
        MsgBox("Registro Almacenado Exitosamente", MsgBoxStyle.Information)
    End Sub

    '''''MÉTODO QUE EJECUTA EL PROCEDIMIENTO DE MODIFICACIÓN DE LOS DATOS A LA TABLA DE LOS GASTOS
    Private Sub MODIFICAR(ByVal prmIdEntrega As Long, ByVal prmDescripcion As String, ByVal prmImporte As Decimal, ByVal prmOp As Integer)
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@idEntrega", SqlDbType.BigInt, prmIdEntrega)
        ControlEfectivoClass.CreateMyParameter("@descripcion", SqlDbType.VarChar, prmDescripcion, 250)
        ControlEfectivoClass.CreateMyParameter("@importe", SqlDbType.Money, prmImporte)
        ControlEfectivoClass.CreateMyParameter("@op", SqlDbType.Int, prmOp)

        ControlEfectivoClass.Inserta("uspModificaEntregasGlobales")
        MsgBox("Registro Almacenado Exitosamente", MsgBoxStyle.Information)
    End Sub

    Private Sub uspCalculaEfectivo(ByVal prmCajera As String, ByVal prmFecha As Date)
        '''''EJECUTAMOS EL PROCEDIMIENTO QUE NOS VA A DAR LOS TOTALES DEL EFECTIVO EN EL DÍA POR LA CAJERA (INICIO)
        Dim DT As New DataTable

        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@cajera", SqlDbType.VarChar, prmCajera, 5)
        ControlEfectivoClass.CreateMyParameter("@fecha", SqlDbType.DateTime, prmFecha)

        DT = ControlEfectivoClass.ConsultaDT("uspCalculaEfectivo")
        Me.txtEfectivoCobrado.Text = CDec(DT.Rows(0)("totalEfectivo")).ToString("#.00")
        '''''EJECUTAMOS EL PROCEDIMIENTO QUE NOS VA A DAR LOS TOTALES DEL EFECTIVO EN EL DÍA POR LA CAJERA (FIN)
    End Sub

    Private Sub uspInsertaRelEntregaGlobalDesglose(ByVal prmIdDescarga As Long, ByVal prmB1000 As Integer, ByVal prmB500 As Integer, ByVal prmB200 As Integer, _
                                                   ByVal prmB100 As Integer, ByVal prmB50 As Integer, ByVal prmB20 As Integer, ByVal prmM100 As Integer, ByVal prmM50 As Integer, _
                                                   ByVal prmM20 As Integer, ByVal prmM10 As Integer, ByVal prmM5 As Integer, ByVal prmM2 As Integer, ByVal prmM1 As Integer, _
                                                   ByVal prmM050 As Integer, ByVal prmM020 As Integer, ByVal prmM010 As Integer, ByVal prmOpcion As String)
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@idDescarga", SqlDbType.BigInt, prmIdDescarga)
        ControlEfectivoClass.CreateMyParameter("@b1000", SqlDbType.Int, prmB1000)
        ControlEfectivoClass.CreateMyParameter("@b500", SqlDbType.Int, prmB500)
        ControlEfectivoClass.CreateMyParameter("@b200", SqlDbType.Int, prmB200)
        ControlEfectivoClass.CreateMyParameter("@b100", SqlDbType.Int, prmB100)
        ControlEfectivoClass.CreateMyParameter("@b50", SqlDbType.Int, prmB50)
        ControlEfectivoClass.CreateMyParameter("@b20", SqlDbType.Int, prmB20)
        ControlEfectivoClass.CreateMyParameter("@m100", SqlDbType.Int, prmM100)
        ControlEfectivoClass.CreateMyParameter("@m50", SqlDbType.Int, prmM50)
        ControlEfectivoClass.CreateMyParameter("@m20", SqlDbType.Int, prmM20)
        ControlEfectivoClass.CreateMyParameter("@m10", SqlDbType.Int, prmM10)
        ControlEfectivoClass.CreateMyParameter("@m5", SqlDbType.Int, prmM5)
        ControlEfectivoClass.CreateMyParameter("@m2", SqlDbType.Int, prmM2)
        ControlEfectivoClass.CreateMyParameter("@m1", SqlDbType.Int, prmM1)
        ControlEfectivoClass.CreateMyParameter("@m050", SqlDbType.Int, prmM050)
        ControlEfectivoClass.CreateMyParameter("@m020", SqlDbType.Int, prmM020)
        ControlEfectivoClass.CreateMyParameter("@m010", SqlDbType.Int, prmM010)

        If prmOpcion = "N" Then
            ControlEfectivoClass.Inserta("uspInsertaRelEntregaGlobalDesglose")
        ElseIf prmOpcion = "M" Then
            ControlEfectivoClass.Inserta("uspModificaRelEntregaGlobalDesglose")
        End If
    End Sub

    Private Sub uspConsultaRelEntregaGlobalDesglose(ByVal pmrIdEntregaGlobal As Long)
        ControlEfectivoClass.limpiaParametros()

        Dim DT As New DataTable
        ControlEfectivoClass.CreateMyParameter("@idDescarga", SqlDbType.BigInt, pmrIdEntregaGlobal)

        DT = ControlEfectivoClass.ConsultaDT("uspConsultaRelEntregaGlobalDesglose")
        If DT.Rows.Count > 0 Then
            Me.txtB1000.Text = DT.Rows(0)("b1000").ToString
            Me.txtB500.Text = DT.Rows(0)("b500").ToString
            Me.txtB200.Text = DT.Rows(0)("b200").ToString
            Me.txtB100.Text = DT.Rows(0)("b100").ToString
            Me.txtB50.Text = DT.Rows(0)("b50").ToString
            Me.txtB20.Text = DT.Rows(0)("b20").ToString
            Me.txtM100.Text = DT.Rows(0)("m100").ToString
            Me.txtM50.Text = DT.Rows(0)("m50").ToString
            Me.txtM20.Text = DT.Rows(0)("m20").ToString
            Me.txtM10.Text = DT.Rows(0)("m10").ToString
            Me.txtM5.Text = DT.Rows(0)("m5").ToString
            Me.txtM2.Text = DT.Rows(0)("m2").ToString
            Me.txtM1.Text = DT.Rows(0)("m1").ToString
            Me.txtM050.Text = DT.Rows(0)("m050").ToString
            Me.txtM020.Text = DT.Rows(0)("m020").ToString
            Me.txtM010.Text = DT.Rows(0)("m010").ToString
        End If
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.txtDescripcion.Text.Length = 0 Then '''''SE VALIDA QUE HAYAN INGRESADO UNA DESCRIPCIÓN AL GASTO
            MsgBox("Capture una Descripción para continuar", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.txtImporte.Text.Length = 0 Then '''''SE VALIDA QUE SE HAYA INGRESADO EL IMPORTE DEL GASTO
            MsgBox("Capture el Importe del Gasto para continuar", MsgBoxStyle.Information)
            Exit Sub
        End If

        If IsNumeric(Me.txtImporte.Text) = False Then
            MsgBox("Capture un Importe Válido para continuar", MsgBoxStyle.Information)
            Exit Sub
        End If

        If CDec(Me.txtImporte.Text) > CDec(Me.txtEfectivoCobrado.Text) Then
            MsgBox("El Importe no puede ser mayor al Efectivo Disponible", MsgBoxStyle.Information)
            Exit Sub
        End If

        If prmOpcion = "N" Then ''''SE VALIDA QUE CUANDO SEA NUEVO SE MANDE EL MÉTODO DE INSERTAR
            INSERTAR(GloUsuario, Me.txtDescripcion.Text, Me.txtImporte.Text)
            bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Se da de alta Enterga Global por: " + CStr(Me.txtImporte.Text), "", SubCiudad)
        End If

        If prmOpcion = "M" Then ''''SE VALIDA QUE CUANDO SEA MODIFICAR SE MANDE EL MÉTODO DE MODIFICAR
            MODIFICAR(prmClaveEntrega, Me.txtDescripcion.Text, Me.txtImporte.Text, 0)
            bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Se Modifica Entrega Global # " + CStr(prmClaveEntrega) + " por $" + CStr(Me.txtImporte.Text) + " de: " + CStr(Me.txtUsuario.Text), "Importe Anterior" + CStr(prmImporteAnterior), SubCiudad)
        End If

        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub bnEntregasGlobalesGuargar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnEntregasGlobalesGuargar.Click
        If Me.txtDescripcion.Text.Length = 0 Then '''''SE VALIDA QUE HAYAN INGRESADO UNA DESCRIPCIÓN AL GASTO
            MsgBox("Capture una Descripción para continuar", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.txtImporte.Text.Length = 0 Then '''''SE VALIDA QUE SE HAYA INGRESADO EL IMPORTE DEL GASTO
            MsgBox("Capture el Importe de la Entrega para continuar", MsgBoxStyle.Information)
            Exit Sub
        End If

        If CDec(Me.txtImporte.Text) <= 0 Then '''''SE VALIDA QUE SE HAYA INGRESADO EL IMPORTE DEL GASTO
            MsgBox("Capture el Importe de la Entrega para continuar", MsgBoxStyle.Information)
            Exit Sub
        End If

        If IsNumeric(Me.txtImporte.Text) = False Then
            MsgBox("Capture un Importe Válido para continuar", MsgBoxStyle.Information)
            Exit Sub
        End If

        If CDec(Me.txtImporte.Text) > CDec(Me.txtEfectivoCobrado.Text) Then
            MsgBox("El Importe no puede ser mayor al Efectivo Disponible", MsgBoxStyle.Information)
            Exit Sub
        End If

        validaEfectivo() ''''VALIDAMOS QUE LOS CAMPOS NO VAYAN NULOS ANTES DE MANDAR ALMACENAR LA ENTREGA GLOBAL

        If prmOpcion = "N" Then ''''SE VALIDA QUE CUANDO SEA NUEVO SE MANDE EL MÉTODO DE INSERTAR
            INSERTAR(GloUsuario, Me.txtDescripcion.Text, Me.txtImporte.Text) ''''SE ALMACENA LA NUEVA ENTREGA
            uspInsertaRelEntregaGlobalDesglose(prmIdEntrega, Me.txtB1000.Text, Me.txtB500.Text, Me.txtB200.Text, Me.txtB100.Text, Me.txtB50.Text, Me.txtB20.Text, _
                                               Me.txtM100.Text, Me.txtM50.Text, Me.txtM20.Text, Me.txtM10.Text, Me.txtM5.Text, Me.txtM2.Text, Me.txtM1.Text, Me.txtM050.Text, _
                                               Me.txtM020.Text, Me.txtM010.Text, prmOpcion) ''''SE ALMACENA EL DESGLOSE DE LA NUEVA ENTREGA
            uspReporteEntregaGlobalNueva(prmIdEntrega) '''''SE MANDA IMPRIMIR EL REPORTE DE LA ENTREGA GENERADA
            bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Se da de alta Enterga Global por: " + CStr(Me.txtImporte.Text), "", SubCiudad)
        End If

        If prmOpcion = "M" Then ''''SE VALIDA QUE CUANDO SEA MODIFICAR SE MANDE EL MÉTODO DE MODIFICAR
            MODIFICAR(prmClaveEntrega, Me.txtDescripcion.Text, Me.txtImporte.Text, 0) ''''SE ALMACENAN LAS MODIFICACIONES REALIZADAS A LA ENTREGA
            uspInsertaRelEntregaGlobalDesglose(prmClaveEntrega, Me.txtB1000.Text, Me.txtB500.Text, Me.txtB200.Text, Me.txtB100.Text, Me.txtB50.Text, Me.txtB20.Text, _
                                               Me.txtM100.Text, Me.txtM50.Text, Me.txtM20.Text, Me.txtM10.Text, Me.txtM5.Text, Me.txtM2.Text, Me.txtM1.Text, Me.txtM050.Text, _
                                               Me.txtM020.Text, Me.txtM010.Text, prmOpcion) ''''SE ALMACENA EL DESGLOSE DE LA ENTREGA MODIFICADA
            uspReporteEntregaGlobalNueva(prmClaveEntrega) '''''SE MANDA IMPRIMIR EL REPORTE DE LA ENTREGA MODIFICADA
            bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Se Modifica Entrega Global # " + CStr(prmClaveEntrega) + " por $" + CStr(Me.txtImporte.Text) + " de: " + CStr(Me.txtUsuario.Text), "Importe Anterior" + CStr(prmImporteAnterior), SubCiudad)
        End If
        Me.Close()
    End Sub

    Private Sub bnEntregasGlobalesCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnEntregasGlobalesCancelar.Click
        Me.Close()
    End Sub

    Private Sub Suma_Detalle()
        Dim b1000 As Integer = 0
        Dim b500 As Integer = 0
        Dim b200 As Integer = 0
        Dim b100 As Integer = 0
        Dim b20 As Integer = 0
        Dim b50 As Integer = 0
        Dim M100 As Integer = 0
        Dim M50 As Integer = 0
        Dim M20 As Integer = 0
        Dim M10 As Integer = 0
        Dim M5 As Integer = 0
        Dim M2 As Integer = 0
        Dim M1 As Integer = 0
        Dim M050 As Double = 0
        Dim M020 As Double = 0
        Dim M010 As Double = 0

        Try
            If IsNumeric(Me.txtB1000.Text) = True Then
                b1000 = Me.txtB1000.Text * 1000
            End If
            If IsNumeric(Me.txtB500.Text) = True Then
                b500 = Me.txtB500.Text * 500
            End If
            If IsNumeric(Me.txtB200.Text) = True Then
                b200 = Me.txtB200.Text * 200
            End If
            If IsNumeric(Me.txtB100.Text) = True Then
                b100 = Me.txtB100.Text * 100
            End If
            If IsNumeric(Me.txtB50.Text) = True Then
                b50 = Me.txtB50.Text * 50
            End If
            If IsNumeric(Me.txtB20.Text) = True Then
                b20 = Me.txtB20.Text * 20
            End If
            If IsNumeric(Me.txtM100.Text) = True Then
                M100 = Me.txtM100.Text * 100
            End If
            If IsNumeric(Me.txtM50.Text) = True Then
                M50 = Me.txtM50.Text * 50
            End If
            If IsNumeric(Me.txtM20.Text) = True Then
                M20 = Me.txtM20.Text * 20
            End If
            If IsNumeric(Me.txtM10.Text) = True Then
                M10 = Me.txtM10.Text * 10
            End If
            If IsNumeric(Me.txtM5.Text) = True Then
                M5 = Me.txtM5.Text * 5
            End If
            If IsNumeric(Me.txtM2.Text) = True Then
                M2 = Me.txtM2.Text * 2
            End If
            If IsNumeric(Me.txtM1.Text) = True Then
                M1 = Me.txtM1.Text * 1
            End If
            If IsNumeric(Me.txtM050.Text) = True Then
                M050 = Me.txtM050.Text * 0.5
            End If
            If IsNumeric(Me.txtM020.Text) = True Then
                M020 = Me.txtM020.Text * 0.2
            End If
            If IsNumeric(Me.txtM010.Text) = True Then
                M010 = Me.txtM010.Text * 0.1
            End If
            Suma = b1000 + b500 + b200 + b100 + b50 + b20 + M100 + M50 + M20 + M10 + M5 + M2 + M1 + M050 + M020 + M010
            Me.txtImporte.Text = Suma
        Catch
            MsgBox("Demasiado Dinero, Rectifica", , "Advertencia")
        End Try
    End Sub

    Private Sub validaEfectivo()
        If IsNumeric(Me.txtB1000.Text) = False Then
            Me.txtB1000.Text = 0
        End If
        If IsNumeric(Me.txtB500.Text) = False Then
            Me.txtB500.Text = 0
        End If
        If IsNumeric(Me.txtB200.Text) = False Then
            Me.txtB200.Text = 0
        End If
        If IsNumeric(Me.txtB100.Text) = False Then
            Me.txtB100.Text = 0
        End If
        If IsNumeric(Me.txtB50.Text) = False Then
            Me.txtB50.Text = 0
        End If
        If IsNumeric(Me.txtB20.Text) = False Then
            Me.txtB20.Text = 0
        End If
        If IsNumeric(Me.txtM100.Text) = False Then
            Me.txtM100.Text = 0
        End If
        If IsNumeric(Me.txtM50.Text) = False Then
            Me.txtM50.Text = 0
        End If
        If IsNumeric(Me.txtM20.Text) = False Then
            Me.txtM20.Text = 0
        End If
        If IsNumeric(Me.txtM10.Text) = False Then
            Me.txtM10.Text = 0
        End If
        If IsNumeric(Me.txtM5.Text) = False Then
            Me.txtM5.Text = 0
        End If
        If IsNumeric(Me.txtM2.Text) = False Then
            Me.txtM2.Text = 0
        End If
        If IsNumeric(Me.txtM1.Text) = False Then
            Me.txtM1.Text = 0
        End If
        If IsNumeric(Me.txtM050.Text) = False Then
            Me.txtM050.Text = 0
        End If
        If IsNumeric(Me.txtM020.Text) = False Then
            Me.txtM020.Text = 0
        End If
        If IsNumeric(Me.txtM010.Text) = False Then
            Me.txtM010.Text = 0
        End If
        If IsNumeric(Me.txtImporte.Text) = False Then
            Me.txtImporte.Text = 0
        End If
    End Sub

    Private Sub txtB1000_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtB1000.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub txtB500_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtB500.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub txtB200_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtB200.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub txtB100_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtB100.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub txtB50_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtB50.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub txtB20_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtB20.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub txtM100_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtM100.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub txtM50_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtM50.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub txtM20_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtM20.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub txtM10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtM10.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub txtM5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtM5.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub txtM2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtM2.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub txtM1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtM1.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub txtM050_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtM050.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub txtM020_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtM020.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub txtM010_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtM010.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub uspReporteEntregaGlobalNueva(ByVal prmIdEntregaGlobal As Long)
        '''''MANDAMOS EL REPORTE (INICIO)
        ControlEfectivoClass.limpiaParametros() ''LIMPIAMOS LA LISTA DE PARÁMETROS

        ControlEfectivoClass.CreateMyParameter("@idEntrega", SqlDbType.BigInt, prmIdEntregaGlobal) ''MANDAMOS LOS PARÁMETROS

        Dim listaTablas As New List(Of String) ''MANDAMOS EL NOMBRE DE LAS TABLAS QUE DEVOLVERÁ EL DATASET DEL REPORTE
        listaTablas.Add("uspReporteEntregaGlobalNueva")

        Dim DataSet As New DataSet ''LLENAMOS EL DATASET QUE LLENARÁ EL REPORTE
        DataSet = ControlEfectivoClass.ConsultaDS("uspReporteEntregaGlobalNueva", listaTablas)

        Dim diccioFormulasReporte As New Dictionary(Of String, String) ''LENAMOS EL DICCIONARIO QUE CONTENDRÁ LAS FÓRMULAS QUE REQUIERE EL REPORTE
        diccioFormulasReporte.Add("Ciudad", LocNomEmpresa)
        diccioFormulasReporte.Add("Empresa", GloNomSucursal)

        ControlEfectivoClass.llamarReporteCentralizado(RutaReportes + "/rptEntregasGlobalesNuevas", DataSet, diccioFormulasReporte) ''MANDAMOS LLAMAR EL REPORTE
        '''''MANDAMOS EL REPORTE (INICIO)
    End Sub
End Class
﻿Imports System.Data.SqlClient
Public Class FrmSelCompania

    Private Sub FrmSelCompania_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, -1)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCompania")
        llenalistboxs()
    End Sub
    Private Sub llenalistboxs()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        loquehay.DataSource = BaseII.ConsultaDT("ProcedureSeleccionCompania")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        seleccion.DataSource = BaseII.ConsultaDT("ProcedureSeleccionCompania")
    End Sub

    Private Sub agregar_Click(sender As System.Object, e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, loquehay.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCompania")
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(sender As System.Object, e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 5)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, seleccion.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCompania")
        llenalistboxs()
    End Sub

    Private Sub agregartodo_Click(sender As System.Object, e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 6)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCompania")
        llenalistboxs()
    End Sub

    Private Sub quitartodo_Click(sender As System.Object, e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 7)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCompania")
        llenalistboxs()
    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button6_Click(sender As System.Object, e As System.EventArgs) Handles Button6.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        If varfrmselcompania = "cortefac" Then
            FrmOpRep.Show()
        ElseIf varfrmselcompania = "relingreso" Or locband_pant = 1011 Then
            If GloBnd_Des_Men = True Then
                FrmFechaIngresosConcepto.Show()
                Me.Close()
            ElseIf LocbndPolizaCiudad = True Then
                FrmFechaIngresosConcepto.Show()
                Me.Close()
            Else
                GloBnd_Des_Men = False
                GloBnd_Des_Cont = False
                LocbndPolizaCiudad = False
                FrmFechaIngresosConcepto.Show()

            End If
        ElseIf varfrmselcompania = "" And varfrmselsucursal = "relingreso" Then
            FrmSelSucursal.Show()
            Me.Close()
        Else
            GloBndDesgloceCompNew = True
        End If

        Me.Close()
    End Sub
End Class
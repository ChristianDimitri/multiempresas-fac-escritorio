﻿Public Class FrmFechasCobroMaterial
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedValue = 0

            End If
            GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub rbPorContrato_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbPorContrato.CheckedChanged
        If Me.rbPorContrato.Checked Then
            Me.pnlContrato.Enabled = True
            Me.pnlFechas.Enabled = False
            Me.pnlStatus.Enabled = False
        End If
    End Sub

    Private Sub rbPorFechas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbPorFechas.CheckedChanged
        If Me.rbPorFechas.Checked Then
            Me.txtContrato.Clear()
            Me.pnlContrato.Enabled = False
            Me.pnlFechas.Enabled = True
            Me.pnlStatus.Enabled = False
        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub FrmFechasCobroMaterial_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Llena_companias()
        Me.dtpFechaFin.MaxDate = Today
        Me.dtpFechaIni.MaxDate = Me.dtpFechaFin.Value
    End Sub

    Private Sub dtpFechaFin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaFin.ValueChanged
        Me.dtpFechaFin.MaxDate = Today
        Me.dtpFechaIni.MaxDate = Me.dtpFechaFin.Value
    End Sub

    Private Sub btnGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerar.Click
        If ComboBoxCompanias.SelectedValue = 0 Then
            MsgBox("Seleccione una Compañía")
            Exit Sub
        End If
        Dim FRM As New FrmImprimirRepGral

        FRM.entraReporte = True
        FRM.contratoCobroMaterial = 0
        FRM.fechaIniCobroMaterial = "01-01-1900"
        FRM.fechaFinCobroMaterial = "01-01-1900"
        FRM.saldadosCobroMaterial = False
        FRM.pendientesCobroMaterial = False

        If Me.dtpFechaFin.Value < Me.dtpFechaIni.Value Then
            MsgBox("La Fecha Final no puede ser menor a la Fecha Inicial", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.rbPorContrato.Checked Then
            If IsNumeric(Me.txtContrato.Text) = False Then
                MsgBox("Ingrese un Contrato válido", MsgBoxStyle.Information)
                Exit Sub
            End If
            FRM.contratoCobroMaterial = CInt(Me.txtContrato.Text)
            FRM.opCobroMaterial = 1
        End If

        If Me.rbPorFechas.Checked Then
            FRM.fechaIniCobroMaterial = Me.dtpFechaIni.Value
            FRM.fechaFinCobroMaterial = Me.dtpFechaFin.Value
            FRM.opCobroMaterial = 2
        End If

        If Me.rbPorStatus.Checked Then
            If Not (Me.cbSaldados.Checked) And Not (Me.cbPendientes.Checked) Then
                MsgBox("Seleccione al menos un Status", MsgBoxStyle.Information)
                Exit Sub
            End If
            FRM.saldadosCobroMaterial = Me.cbSaldados.Checked
            FRM.pendientesCobroMaterial = Me.cbPendientes.Checked
            FRM.opCobroMaterial = 3
        End If

        FRM.Show()
        Me.Close()
    End Sub

    Private Sub rbPorStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbPorStatus.CheckedChanged
        If Me.rbPorStatus.Checked Then
            Me.txtContrato.Clear()
            Me.pnlContrato.Enabled = False
            Me.pnlFechas.Enabled = False
            Me.pnlStatus.Enabled = True
        End If
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
        Catch ex As Exception

        End Try
    End Sub
End Class
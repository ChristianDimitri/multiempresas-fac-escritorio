Imports System.Data.SqlClient

Public Class BrwPolizas

    Private Sub BrwPolizas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Locbndactualizapoiza = True Then
            Locbndactualizapoiza = False
            MUESTRAPoliza(0, 0, DateTime.Today, "", cbCompania.SelectedValue, 0)
        End If

    End Sub



    Private Sub BrwPolizas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clvCompania", SqlDbType.Int, 0)
        cbCompania.DataSource = BaseII.ConsultaDT("MUESTRAtblCompanias")

        'browdfgdfg

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If TextBox1.Text.Length = 0 Then
            MessageBox.Show("Captura una Clave P�liza.")
            Exit Sub
        End If
        If IsNumeric(TextBox1.Text) = False Then
            MessageBox.Show("Captura una Clave P�liza v�lida.")
            Exit Sub
        End If
        MUESTRAPoliza(1, TextBox1.Text, DateTime.Today, "", cbCompania.SelectedValue, 0)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        MUESTRAPoliza(2, 0, DateTimePicker1.Value, "", cbCompania.SelectedValue, 0)


    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        LocopPoliza = "N"

        gloClv_Session = 0
        LocbndPolizaCiudad = True
        GloClvCompania = cbCompania.SelectedValue
        FrmSelCiudadPoliza.Show()
        ' FrmSeleccionaSucursalPoliza.Show()
        ' FrmFechaIngresosConcepto.Show()
        ' FrmSelSucursal.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If DataGridView1.Rows.Count = 0 Then
            MessageBox.Show("Selecciona una P�liza para ser Consultada.")
            Exit Sub
        End If
        LocopPoliza = "C"
        LocGloClv_poliza = DataGridView1.SelectedCells(0).Value
        FrmPoliza.Show()

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If DataGridView1.Rows.Count = 0 Then
            MessageBox.Show("Selecciona una P�liza para ser Modificada.")
            Exit Sub
        End If
        LocopPoliza = "M"
        LocGloClv_poliza = DataGridView1.SelectedCells(0).Value
        FrmPoliza.Show()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If DataGridView1.Rows.Count = 0 Then
            MessageBox.Show("Selecciona una P�liza a Eliminar.")
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_POLIZA", SqlDbType.Int, DataGridView1.SelectedCells(0).Value)
        BaseII.Inserta("ELIMINAPoliza")

        MUESTRAPoliza(0, 0, DateTime.Today, "", cbCompania.SelectedValue, 0)

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        LocGloClv_poliza = DataGridView1.SelectedCells(0).Value
        If IsNumeric(LocGloClv_poliza) = True Then
            gLOoPrEPpOLIZA = 0
            FrmImprimePoliza.Show()
            gLOoPrEPpOLIZA = 0
        End If
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click

        If IsNumeric(LocGloClv_poliza) = True Then
            gLOoPrEPpOLIZA = 1
            FrmImprimePoliza.Show()
            gLOoPrEPpOLIZA = 0
        End If
    End Sub

    Private Sub MUESTRAPoliza(ByVal Op As Integer, ByVal Clv_Llave_Poliza As Integer, ByVal Fecha As DateTime, ByVal Concepto As String, ByVal ClvCompania As Integer, ByVal Sucursal As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Llave_Poliza", SqlDbType.Int, Clv_Llave_Poliza)
        BaseII.CreateMyParameter("@Fecha", SqlDbType.DateTime, Fecha)
        BaseII.CreateMyParameter("@Concepto", SqlDbType.VarChar, Concepto, 250)
        BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, ClvCompania)
        BaseII.CreateMyParameter("@Sucursal", SqlDbType.Int, Sucursal)
        DataGridView1.DataSource = BaseII.ConsultaDT("MUESTRAPoliza")
    End Sub

    Private Sub cbCompania_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCompania.SelectedIndexChanged
        If cbCompania.Text.Length = 0 Then
            Exit Sub
        End If

        GloClvCompania = cbCompania.SelectedValue

        MUESTRAPoliza(0, 0, DateTime.Today, "", cbCompania.SelectedValue, 0)

        MUESTRASucursalesCompania(GloClvCompania)
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        Try
            Clv_calleLabel2.Text = DataGridView1.SelectedCells(0).Value
            CMBNombreTextBox.Text = DataGridView1.SelectedCells(1).Value
            CMBTextBox5.Text = DataGridView1.SelectedCells(3).Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub MUESTRASucursalesCompania(ByVal CLVCOMPANIA As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, CLVCOMPANIA)
        cbSucursal.DataSource = BaseII.ConsultaDT("MUESTRASucursalesPorCompania")
    End Sub

    Private Sub bnBuscarSucursal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnBuscarSucursal.Click
        If cbCompania.Text.Length = 0 Then Exit Sub
        If cbSucursal.Text.Length = 0 Then Exit Sub
        MUESTRAPoliza(4, 0, DateTime.Today, "", cbCompania.SelectedValue, cbSucursal.SelectedValue)
    End Sub

    Private Sub cbSucursal_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbSucursal.SelectedIndexChanged

    End Sub
End Class
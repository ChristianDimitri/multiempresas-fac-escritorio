Imports System.Data.SqlClient
Imports Com.Netpay.Pinpad
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.IO
Imports System.Windows.Forms


Public Class FrmPago
    Public BndLocal As Boolean = True
    Dim Bndnoentres As Boolean = False
    Dim monto As Double
    'Dim ipp As Ipp320Manager

    Dim orderid As String

    'Dim userSandbox As String = ""
    'Dim passwordSandbox As String = ""
    'Dim TipoIntegracion As String = ""
    'Dim Puerto As String = ""
    'Dim StoreID As String = ""
    'Dim terminalId As String = ""
    'Dim timeOut As String = ""

    Dim FILENAME As String = "./Log_Ipp320.txt"
    Dim ostrm As FileStream
    Dim writer As StreamWriter
    Dim oldOut As TextWriter


    Dim TerminoBackGroundWorker As Boolean = False

    Private Sub Llena_Combo()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand()
        Try
            CMD = New SqlCommand()
            CON.Open()
            With CMD
                .CommandText = "Muestra_Tipos_Tarjeta"
                .CommandTimeout = 0
                .Connection = CON
                .CommandType = CommandType.StoredProcedure

                Dim PRM As New SqlParameter("@OP", SqlDbType.Int)
                PRM.Value = 0
                PRM.Direction = ParameterDirection.Input
                .Parameters.Add(PRM)

                Dim READER As SqlDataReader = .ExecuteReader()

                While (READER.Read)
                    Me.CmbTipCuenta.Items.Add(READER.GetValue(1))
                End While
            End With
            CON.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub EfectivoTextBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles EfectivoTextBox.Click
        Me.EfectivoTextBox.SelectionStart = 0
        Me.EfectivoTextBox.SelectionLength = Len(Me.EfectivoTextBox.Text)
    End Sub

    Private Sub EfectivoTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles EfectivoTextBox.GotFocus
        Me.EfectivoTextBox.SelectionStart = 0
        Me.EfectivoTextBox.SelectionLength = Len(Me.EfectivoTextBox.Text)
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EfectivoTextBox.TextChanged
        Suma(0)
    End Sub

    Private Sub Label4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label4.Click

    End Sub

    Private Sub Suma(ByVal TipoPago As Integer)
        If BndLocal = True Then
            Dim Efectivo As Double = 0
            Dim cheque As Double = 0
            Dim Tarjeta As Double = 0
            Dim Nota As Double = 0
            Dim Cambio As Double = 0
            Dim Total As Double = 0
            Dim SubTotal As Double = 0
            Dim saldo As Double = 0
            Dim Abono As Double = 0

            '0 --Efectivo
            '1 --Cheque
            '2 --Tarjeta
            If IsNumeric(Me.CMBTotalaPagarLabel.Text) = False Then Total = 0 Else Total = Me.CMBTotalaPagarLabel.Text
            If IsNumeric(Me.EfectivoTextBox.Text) = False Then Efectivo = 0 Else Efectivo = Me.EfectivoTextBox.Text
            If IsNumeric(Me.ChequeTextBox.Text) = False Then cheque = 0 Else cheque = Me.ChequeTextBox.Text
            If IsNumeric(Me.TextBox4.Text) = False Then Nota = 0 Else Nota = Me.TextBox4.Text
            If IsNumeric(Me.TarjetaTextBox.Text) = False Then Tarjeta = 0 Else Tarjeta = Me.TarjetaTextBox.Text
            If IsNumeric(Me.CMBSaldoLabel.Text) = False Then saldo = 0 Else saldo = Me.CMBSaldoLabel.Text
            If IsNumeric(Me.CMBAbonoLabel.Text) = False Then Abono = 0 Else Abono = Me.CMBAbonoLabel.Text
            '
            Select Case TipoPago
                Case 0 'Efectivo
                    SubTotal = Total - (cheque + Tarjeta + Nota)
                Case 1 'chaque
                    SubTotal = Total - (Efectivo + Tarjeta + Nota)
                Case 2 'tarjeta
                    SubTotal = Total - (Efectivo + cheque + Nota)
                Case 3 'Nota
                    SubTotal = Total - (Efectivo + cheque + Tarjeta)
            End Select


            If SubTotal < 0 Then
                saldo = 0
                Abono = Total
            Else
                saldo = SubTotal
                Abono = Total - SubTotal
            End If

            Select Case TipoPago
                Case 0 'Efectivo
                    If saldo > 0 Then
                        SubTotal = SubTotal - Efectivo
                        If SubTotal < 0 Then
                            Cambio = (SubTotal * -1)
                        Else
                            Cambio = 0
                        End If
                        If SubTotal < 0 Then
                            saldo = 0
                            Abono = Total
                        Else
                            saldo = SubTotal
                            Abono = Total - SubTotal
                        End If

                    Else
                        Me.EfectivoTextBox.Text = 0
                        Cambio = 0
                    End If
                Case 1 'cheque
                    If saldo > 0 Then
                        SubTotal = SubTotal - cheque
                        If SubTotal < 0 Then
                            'MsgBox("La Cantidad Capturada no es Valida", MsgBoxStyle.Information)
                            Me.ChequeTextBox.Text = 0
                            Cambio = 0
                        Else
                            If SubTotal < 0 Then
                                saldo = 0
                                Abono = Total
                            Else
                                saldo = SubTotal
                                Abono = Total - SubTotal
                            End If
                            Cambio = 0
                        End If
                    Else
                        BndLocal = False
                        Me.ChequeTextBox.Text = 0
                        cheque = 0
                        BndLocal = True

                        SubTotal = Total - (cheque + Tarjeta + Nota)
                        If SubTotal < 0 Then
                            saldo = 0
                            Abono = Total
                        Else
                            saldo = SubTotal
                            Abono = Total - SubTotal
                        End If
                        If saldo > 0 Then
                            SubTotal = SubTotal - Efectivo
                            If SubTotal < 0 Then
                                Cambio = (SubTotal * -1)
                            Else
                                Cambio = 0
                            End If
                            If SubTotal < 0 Then
                                saldo = 0
                                Abono = Total
                            Else
                                saldo = SubTotal
                                Abono = Total - SubTotal
                            End If

                        Else
                            Me.EfectivoTextBox.Text = 0
                            Cambio = 0
                        End If
                        MsgBox(SubTotal)
                        Me.CMBCambioLabel.Text = Cambio
                        Me.CMBAbonoLabel.Text = Abono
                        Me.CMBSaldoLabel.Text = saldo

                        Exit Sub
                        BndLocal = True
                        'Cambio = 0
                    End If
                Case 2 'tarjeta
                    If saldo > 0 Then
                        SubTotal = SubTotal - Tarjeta
                        If SubTotal < 0 Then
                            'MsgBox("La Cantidad Capturada no es Valida", MsgBoxStyle.Information)
                            Me.TarjetaTextBox.Text = 0
                            Cambio = 0
                        Else
                            If SubTotal < 0 Then
                                saldo = 0
                                Abono = Total
                            Else
                                saldo = SubTotal
                                Abono = Total - SubTotal
                            End If
                            Cambio = 0
                        End If
                    Else
                        BndLocal = False
                        Me.TarjetaTextBox.Text = 0
                        Tarjeta = 0
                        BndLocal = True
                        SubTotal = Total - (cheque + Tarjeta + Nota)
                        If SubTotal < 0 Then
                            saldo = 0
                            Abono = Total
                        Else
                            saldo = SubTotal
                            Abono = Total - SubTotal
                        End If
                        If saldo > 0 Then
                            SubTotal = SubTotal - Efectivo
                            If SubTotal < 0 Then
                                Cambio = (SubTotal * -1)
                            Else
                                Cambio = 0
                            End If
                            If SubTotal < 0 Then
                                saldo = 0
                                Abono = Total
                            Else
                                saldo = SubTotal
                                Abono = Total - SubTotal
                            End If

                        Else
                            Me.EfectivoTextBox.Text = 0
                            Cambio = 0
                        End If
                        Me.CMBCambioLabel.Text = Cambio
                        Me.CMBAbonoLabel.Text = Abono
                        Me.CMBSaldoLabel.Text = saldo

                        Exit Sub
                        BndLocal = True
                        'Cambio = 0
                    End If
                Case 3 'Nota de Credito
                    If saldo > 0 Then
                        If Nota > SubTotal And Nota > 0 Then
                            BndLocal = False
                            Me.TextBox4.Text = SubTotal
                            Nota = SubTotal
                            BndLocal = True
                        End If

                        SubTotal = SubTotal - Nota
                        If SubTotal < 0 Then
                            'MsgBox("La Cantidad Capturada no es Valida", MsgBoxStyle.Information)
                            Me.TextBox4.Text = 0
                            Cambio = 0
                        Else
                            If SubTotal < 0 Then
                                saldo = 0
                                Abono = Total
                            Else
                                saldo = SubTotal
                                Abono = Total - SubTotal
                            End If
                            Cambio = 0
                        End If
                    Else
                        BndLocal = False
                        Me.TextBox4.Text = 0
                        Nota = 0
                        BndLocal = True
                        SubTotal = Total - (cheque + Tarjeta + Nota)
                        If SubTotal < 0 Then
                            saldo = 0
                            Abono = Total
                        Else
                            saldo = SubTotal
                            Abono = Total - SubTotal
                        End If
                        If saldo > 0 Then
                            SubTotal = SubTotal - Efectivo
                            If SubTotal < 0 Then
                                Cambio = (SubTotal * -1)
                            Else
                                Cambio = 0
                            End If
                            If SubTotal < 0 Then
                                saldo = 0
                                Abono = Total
                            Else
                                saldo = SubTotal
                                Abono = Total - SubTotal
                            End If

                        Else
                            Me.EfectivoTextBox.Text = 0
                            Cambio = 0
                        End If
                        Me.CMBCambioLabel.Text = Cambio
                        Me.CMBAbonoLabel.Text = Abono
                        Me.CMBSaldoLabel.Text = saldo
                        Exit Sub
                        BndLocal = True
                        'Cambio = 0
                    End If
            End Select

            Me.CMBCambioLabel.Text = Format(Cambio, "##,##0.00")
            Me.CMBAbonoLabel.Text = Format(Abono, "##,##0.00")
            Me.CMBSaldoLabel.Text = Format(saldo, "##,##0.00")
            GloPagorecibido = (Cambio + Abono)
        End If
    End Sub

    Private Sub FrmPago_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        ticketTarjeta = ""
        bTerminal.Visible = gloterminal
        Button2.Visible = gloterminal

        ' log file
        'oldOut = Console.Out
        'Try
        '    ostrm = New FileStream(FILENAME, FileMode.Append, FileAccess.Write)
        '    writer = New StreamWriter(ostrm)
        'Catch ex As Exception
        '    Console.WriteLine("Cannot open Redirect.txt for writing")
        '    Console.WriteLine(ex.Message)
        '    Return
        'End Try

        'Console.SetOut(writer)



        'Fin log

        '----Terminal
        'TerminoBackGroundWorker = False
        'BackgroundWorker2.WorkerSupportsCancellation = True
        '-----Fin Terminal
        'Try

        '    Dim dt As DataTable
        '    'Dim userSandbox As String = ""
        '    'Dim passwordSandbox As String = ""
        '    'Dim TipoIntegracion As String = ""
        '    'Dim Puerto As String = ""
        '    'Dim StoreID As String = ""
        '    'Dim terminalId As String = ""
        '    'Dim timeOut As String = "2000"

        '    BaseII.limpiaParametros()
        '    BaseII.CreateMyParameter("@Computadora", SqlDbType.NVarChar, My.Computer.Name)
        '    dt = BaseII.ConsultaDT("GetIntegracionByNombreComputadora")
        '    Dim dr As DataRow
        '    For Each dr In dt.Rows
        '        userSandbox = dr("Usuario")
        '        passwordSandbox = dr("Contrase�a")
        '        TipoIntegracion = dr("TipoIntegracion")
        '        Puerto = dr("Puerto")
        '        StoreID = dr("SandboxId")
        '        terminalId = dr("TerminalId")
        '    Next

        '    'userSandbox = "netLtwXvX2Q5U"
        '    'passwordSandbox = "3iOUQzsiIMJJVOex"
        '    'Puerto = "COM3"
        '    'StoreID = "280"
        '    'terminalId = "821957070"
        '    timeOut = "30000"

        '    'ipp = New Ipp320Manager(Ipp320Manager.NetpayHostType.SANDBOX, userSandbox, passwordSandbox, StoreID, Puerto, False)
        '    ipp = New Ipp320Manager(Puerto, "https://200.57.87.243/acquirertst", "https://200.57.87.243/acquirertst", timeOut, False)
        '    ipp.useTLS1_2 = False
        '    'ipp.TimeOut = "2000"

        'Catch ex As Exception
        '    MsgBox("No se pudo incializar la terminal")
        'End Try

        '----Terminal
        'BackgroundWorker2.RunWorkerAsync()
        'Timer1.Enabled = True
        '---Fin terminal

        'GLOIMPTOTAL = 210.89
        GloPagorecibido = 0
        GloTipoTarjeta = 0
        GLOPAGOTARJETAAUTORIZADO = False
        Me.CMBTotalaPagarLabel.Text = Format(GLOIMPTOTAL, "##,##0.00")
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet1.MUESTRABANCOS1' Puede moverla o quitarla seg�n sea necesario.
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MUESTRABANCOS1TableAdapter.Connection = CON
        Me.MUESTRABANCOS1TableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRABANCOS1)

        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet1.MUESTRABANCOS' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRABANCOSTableAdapter.Connection = CON
        Me.MUESTRABANCOSTableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRABANCOS)
        CON.Close()
        Llena_Combo()
        'If SubCiudad <> "AG" Then
        '    Me.Panel5.Visible = False
        'End If
        If IdSistema = "LO" Then
            Me.Panel3.Enabled = False
            'NOTAS DE CREDITO DESEPARECEN PARA LOGITEL
            Me.Panel5.Enabled = False
            Me.Panel5.Visible = False
        End If
        Me.ComboBox1.Text = ""
        Me.ComboBox2.Text = ""
        Suma(0)
    End Sub

    Private Sub TarjetaTextBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TarjetaTextBox.Click
        Me.TarjetaTextBox.SelectionStart = 0
        Me.TarjetaTextBox.SelectionLength = Len(Me.TarjetaTextBox.Text)
    End Sub

    Private Sub TarjetaTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TarjetaTextBox.GotFocus
        Me.TarjetaTextBox.SelectionStart = 0
        Me.TarjetaTextBox.SelectionLength = Len(Me.TarjetaTextBox.Text)
    End Sub

    Private Sub TarjetaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TarjetaTextBox.TextChanged
        Suma(2)
    End Sub

    Private Sub ChequeTextBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChequeTextBox.Click
        Me.ChequeTextBox.SelectionStart = 0
        Me.ChequeTextBox.SelectionLength = Len(Me.ChequeTextBox.Text)
    End Sub

    Private Sub ChequeTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChequeTextBox.GotFocus
        Me.ChequeTextBox.SelectionStart = 0
        Me.ChequeTextBox.SelectionLength = Len(Me.ChequeTextBox.Text)
    End Sub

    Private Sub ChequeTextBox_TabIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChequeTextBox.TabIndexChanged

    End Sub

    Private Sub ChequeTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChequeTextBox.TextChanged
        Suma(1)
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Si ya pago con tarjeta mediante la terminal, aunque presione el boton de cancelar, se va a guardar el pago porque ya se proceso la transaccion con la terminal
        If (GLOPAGOTARJETAAUTORIZADO = True And CmbTipCuenta.SelectedIndex >= 0) Then
            Button8_Click(sender, e)
        End If
        If GLOPAGOTARJETAAUTORIZADO = True And CmbTipCuenta.SelectedIndex < 0 Then
            MsgBox("Captura el tipo de cuenta")
            'CmbTipCuenta.Focus()
            Return
        End If
        GLOSIPAGO = 2
        Me.Close()
    End Sub

    Private Sub PagoTarjeta()

        Try

            If (Not BackgroundWorker1.IsBusy) Then
                BackgroundWorker1.RunWorkerAsync()
            End If
        Catch ex As Exception
            MsgBox("Error" + vbCrLf + vbCrLf)
        End Try
    End Sub

    'Boton Aceptar
    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        If IsNumeric(Me.ChequeTextBox.Text) = True Then
            If Me.ChequeTextBox.Text > 0 Then
                If Len(Trim(Me.ComboBox1.Text)) = 0 Then
                    MsgBox("Seleccione el Banco")
                    Exit Sub
                End If
                If Len(Trim(Me.TextBox1.Text)) = 0 Then
                    MsgBox("Capture el Numero del Cheque ")
                    Exit Sub
                End If
            End If
        End If
        If IsNumeric(Me.TarjetaTextBox.Text) = True Then
            If Me.TarjetaTextBox.Text > 0 Then
                If Len(Trim(Me.ComboBox2.Text)) = 0 Then
                    MsgBox("Seleccione el Banco")
                    Exit Sub
                End If
                If Len(Trim(Me.TextBox3.Text)) = 0 Then
                    MsgBox("Capture el Numero de la Tarjeta  ")
                    Exit Sub
                End If
                If Len(Trim(Me.TextBox2.Text)) = 0 Then
                    MsgBox("Capture el Numero de Autorizaci�n  ")
                    Exit Sub
                End If
                If Len(Trim(Me.CmbTipCuenta.Text)) = 0 Then
                    MsgBox("Seleccione el Tipo de Cuenta")
                    Exit Sub
                End If
            End If
        End If



        If IsNumeric(Me.CMBSaldoLabel.Text) = False Then Me.CMBSaldoLabel.Text = 0
        If Me.CMBSaldoLabel.Text = 0 Then
            GLOCLV_NOTA = 0
            GLONOTA = 0
            GLOCHEQUE = 0
            GLOCLV_BANCOCHEQUE = 0
            NUMEROCHEQUE = ""
            GLOTARJETA = 0
            GLOCLV_BANCOTARJETA = 0
            NUMEROTARJETA = ""
            TARJETAAUTORIZACION = ""
            GLOCAMBIO = 0

            If IsNumeric(Me.TarjetaTextBox.Text) = False Then GLOTARJETA = 0 Else GLOTARJETA = Me.TarjetaTextBox.Text
            If IsNumeric(Me.ChequeTextBox.Text) = False Then GLOCHEQUE = 0 Else GLOCHEQUE = Me.ChequeTextBox.Text
            If IsNumeric(Me.TextBox5.Text) = False Then GLOCLV_NOTA = 0 Else GLOCLV_NOTA = Me.TextBox5.Text
            If IsNumeric(Me.TextBox4.Text) = False Then GLONOTA = 0 Else GLONOTA = Me.TextBox4.Text
            If GLONOTA = 0 Then GLOCLV_NOTA = 0
            If GLOCHEQUE > 0 Then
                GLOCHEQUE = Me.ChequeTextBox.Text
                GLOCLV_BANCOCHEQUE = Me.ComboBox1.SelectedValue
                NUMEROCHEQUE = Me.TextBox1.Text
            End If

            If GLOTARJETA > 0 Then
                GLOTARJETA = Me.TarjetaTextBox.Text
                GLOCLV_BANCOTARJETA = Me.ComboBox2.SelectedValue
                NUMEROTARJETA = Me.TextBox3.Text
                TARJETAAUTORIZACION = Me.TextBox2.Text
                GloTipoTarjeta = Me.CmbTipCuenta.SelectedIndex + 1
            End If
            If IsNumeric(GLOTARJETA) = False Then GLOTARJETA = 0
            If IsNumeric(GLOCHEQUE) = False Then GLOCHEQUE = 0
            If IsNumeric(Me.CMBTotalaPagarLabel.Text) = False Then GLOEFECTIVO = 0 Else GLOEFECTIVO = Me.CMBTotalaPagarLabel.Text
            GLOEFECTIVO = GLOEFECTIVO - (GLOTARJETA + GLOCHEQUE + GLONOTA)
            GLOSIPAGO = 1

            If IsNumeric(EfectivoTextBox.Text) = True Then
                GLOTOTALEFECTIVO = EfectivoTextBox.Text
            End If

            If IsNumeric(CMBCambioLabel.Text) = True Then
                GLOCAMBIO = CMBCambioLabel.Text
            End If




            'If locBndBon1 = True Then
            '    'Guarda el Supervisor de la Bonificacion
            '    'GloClv_Factura
            '    'LocSupBon
            '    Dim CON As New SqlConnection(MiConexion)
            '    CON.Open()
            '    MsgBox(GloClv_Factura.ToString, MsgBoxStyle.Information)
            '    If GloClv_Factura > 0 Then
            '        Me.Inserta_Bonificacion_SupervisorTableAdapter.Connection = CON
            '        Me.Inserta_Bonificacion_SupervisorTableAdapter.Fill(Me.Procedimientos_arnoldo.Inserta_Bonificacion_Supervisor, GloClv_Factura, LocSupBon)
            '        CON.Close()
            '    End If
            'End If
            Me.Close()
        Else
            MsgBox("No se ha saldado la factura", MsgBoxStyle.Information)
        End If


    End Sub



    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(TextBox1, Asc(LCase(e.KeyChar)), "N")))
    End Sub



    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        e.KeyChar = Chr((ValidaKey(TextBox3, Asc(LCase(e.KeyChar)), "N")))
    End Sub


    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox5.TextChanged
        If IsNumeric(Me.TextBox5.Text) = True Then
            'If Me.CMBSaldoLabel.Text = 0 Then
            '    Me.TextBox5.Text = 0
            'End If
            Dim cone As New SqlClient.SqlConnection(MiConexion)
            Dim comando As SqlClient.SqlCommand
            comando = New SqlClient.SqlCommand
            cone.Open()
            If Me.TextBox5.Text > 0 Then
                With comando
                    .Connection = cone
                    .CommandText = "DameMonto_NotadeCredito "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm As New SqlParameter("@Clv_Nota", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)
                    Dim prm6 As New SqlParameter("@Monto", SqlDbType.Decimal)
                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Input
                    prm6.Direction = ParameterDirection.Output
                    prm.Value = Me.TextBox5.Text
                    prm1.Value = GloContrato
                    prm6.Value = 0
                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm6)
                    Dim i As Integer = comando.ExecuteNonQuery()
                    monto = prm6.Value
                    If monto = 0 Then
                        Me.REDLabel7.Visible = False
                        'Me.REDLabel7.Text = "El Cliente " + CStr(GloContrato) + " no Tiene Generada una Nota de Cr�dito con el Folio: " + CStr(Me.TextBox5.Text)
                    Else
                        Me.REDLabel7.Visible = False
                    End If
                    Me.TextBox4.Text = Format(monto, "##,##0.00")
                End With
            End If
            cone.Close()
        Else
            Me.TextBox4.Text = ""
            Me.REDLabel7.Visible = False
        End If
    End Sub


    Private Sub TextBox4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox4.TextChanged
        Suma(3)
    End Sub

    Private Sub bTerminal_Click(sender As Object, e As EventArgs) Handles bTerminal.Click
        PagoTarjeta()
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Try


            orderid = Date.Now.Day.ToString + Date.Now.Month.ToString() + Date.Now.Year.ToString() + gloClv_Session.ToString()

            ipp.ProcessTransaction("Auth", StoreID, userSandbox, passwordSandbox, terminalId, "", CMBTotalaPagarLabel.Text.Trim(), "", orderid, "", "P", "", "", "", "", "", "", "0")

        Catch ex As Exception
            MsgBox("Error" + vbCrLf + vbCrLf + ex.Message)
        End Try
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted

        If ipp.PtResponseCode = "00" Then
            ipp.Display(ipp.PtResponseMsg)
            GLOPAGOTARJETAAUTORIZADO = True
            TextBox2.Text = ipp.PtAuthCode
            'cbBanco
            ComboBox2.Text = ipp.PtBankName
            TextBox3.Text = Strings.Right(ipp.PtCardNumber, 4)
            TarjetaTextBox.Text = CMBTotalaPagarLabel.Text
            If ipp.PtCardType = "D" Then
                CmbTipCuenta.SelectedIndex = 0
            End If
            If ipp.PtCardType = "C" Then
                CmbTipCuenta.SelectedIndex = 1
            End If
            Timer1.Enabled = True
            Panel1.Enabled = False
            ticketTarjeta = "Aut. " + ipp.PtAuthCode + vbCrLf + "Tarjeta" + vbCrLf + "**** " + Strings.Right(ipp.PtCardNumber, 4) + vbCrLf + "Order Id" + vbCrLf + orderid

        ElseIf ipp.PtResponseCode = "02" Then

            MsgBox("No se pudo hacer el pago" + vbCrLf + vbCrLf + ipp.PtResponseMsg)
            ipp.ProcessTransaction("Reverse", StoreID, userSandbox, passwordSandbox, terminalId, "", CMBTotalaPagarLabel.Text.Trim(), "Reverso", orderid, "", "P", "", "", "", "", "", "", "0")
        ElseIf ipp.PtResponseCode = "NE" Then
            MsgBox("No se pudo hacer el pago" + vbCrLf + vbCrLf + ipp.PtResponseMsg)
            Timer2.Enabled = True

        Else
            MsgBox("No se pudo hacer el pago.  " + ipp.PtResponseMsg)
            ipp.Display(ipp.PtResponseMsg)
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            ipp.ProcessManagement("LoadInitKey", StoreID, terminalId, userSandbox, passwordSandbox)
        Catch ex As Exception
            MsgBox("No se pudieron inicializar las llaves")
        End Try
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Timer1.Enabled = False

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.VarChar, orderid)
        BaseII.CreateMyParameter("@Autorizacion", SqlDbType.VarChar, ipp.PtAuthCode)
        BaseII.CreateMyParameter("@Tarjeta", SqlDbType.VarChar, Strings.Right(ipp.PtCardNumber, 4))
        BaseII.Inserta("GuardaPagoTerminal")

        MsgBox("Pagado")
        Panel1.Enabled = True
        Button8_Click(sender, e)
    End Sub

    Private Sub CmbTipCuenta_Validated(sender As Object, e As EventArgs) Handles CmbTipCuenta.Validated
        'If (CmbTipCuenta.SelectedIndex < 0) Then
        '    CmbTipCuenta.Focus()
        'End If
    End Sub

    Private Sub CHECADevolucionAparatosCliente(ByVal CLV_SESSION As Integer, ByVal CONTRATO As Integer, ByVal ACTIVO As Boolean)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, CONTRATO)
        BaseII.CreateMyParameter("@ACTIVO", SqlDbType.Bit, ACTIVO)
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("CHECADevolucionAparatosCliente")
        eMsj = ""
        eMsj = BaseII.dicoPar("@MSJ").ToString
    End Sub

    Private Function GotInternet() As Boolean
        Dim req As System.Net.HttpWebRequest
        Dim res As System.Net.HttpWebResponse
        GotInternet = False
        Try
            req = CType(System.Net.HttpWebRequest.Create("http://www.google.com"), System.Net.HttpWebRequest)
            res = CType(req.GetResponse(), System.Net.HttpWebResponse)
            req.Abort()
            If res.StatusCode = System.Net.HttpStatusCode.OK Then
                GotInternet = True
            End If
        Catch weberrt As System.Net.WebException
            GotInternet = False
        Catch except As Exception
            GotInternet = False
        End Try
    End Function

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        If GotInternet() Then
            Timer2.Enabled = False
            System.Threading.Thread.Sleep(15000)
            ipp.ProcessTransaction("FindTransaction", StoreID, userSandbox, passwordSandbox, terminalId, Nothing, CMBTotalaPagarLabel.Text.Trim(), Nothing, orderid, Nothing, "P", Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
            If ipp.PtResponseCode = "00" Then
                ipp.ProcessTransaction("Reverse", StoreID, userSandbox, passwordSandbox, terminalId, "", CMBTotalaPagarLabel.Text.Trim(), "Reverso", orderid, "", "P", "", "", "", "", "", "", "0")
                MsgBox(ipp.PtResponseCode + " " + ipp.PtResponseMsg)
                If ipp.PtResponseCode = "00" Then
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@OrderId", SqlDbType.VarChar, orderid)
                    BaseII.CreateMyParameter("@Importe", SqlDbType.VarChar, CMBTotalaPagarLabel.Text.Trim())
                    BaseII.CreateMyParameter("@Tarjeta", SqlDbType.VarChar, Strings.Right(ipp.PtCardNumber, 4))
                    BaseII.Inserta("GuardarReverso")
                End If
            ElseIf ipp.PtResponseCode = "05" Then
                MsgBox(ipp.PtResponseCode + " " + ipp.PtResponseMsg)
            End If
        End If

    End Sub
End Class
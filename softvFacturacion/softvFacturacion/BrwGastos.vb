﻿Public Class BrwGastos

    Private Sub BrwGastos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)

        If GloTipoUsuario = 40 Then '''''EL COMBO DE LAS CAJERAS ESTARÁ HABILITADO PARA CONTROL TOTAL
            Me.CMBlblBuscaCajera.Visible = True
            Me.cmbBuscaCajera.Visible = True
            Me.btnAceptar.Enabled = True
        Else '''''EL COMBO DE LAS CAJERAS ESTARÁ DESHABILITADO PARA LAS CAJERAS
            Me.CMBlblBuscaCajera.Visible = False
            Me.cmbBuscaCajera.Visible = False
            Me.btnAceptar.Enabled = True
        End If

        '''''LLENAMOS EL COMBO DE LOS ESTATUS (INICIO)
        ControlEfectivoClass.limpiaParametros()
        Me.cmbBuscaEstatus.DisplayMember = "descripcion"
        Me.cmbBuscaEstatus.ValueMember = "status"
        Me.cmbBuscaEstatus.DataSource = ControlEfectivoClass.ConsultaDT("uspLlenaComboStatus")
        '''''LLENAMOS EL COMBO DE LOS ESTATUS (FIN)

        '''''LLENAMOS EL COMBO DE LOS TIPOS DE GASTOS (INICIO)
        ControlEfectivoClass.limpiaParametros()
        Me.cmbBuscaTipoGasto.DisplayMember = "descripcion"
        Me.cmbBuscaTipoGasto.ValueMember = "valor"
        Me.cmbBuscaTipoGasto.DataSource = ControlEfectivoClass.ConsultaDT("uspLlenaComboTipoGasto")
        '''''LLENAMOS EL COMBO DE LOS TIPOS DE GASTOS (FIN)

        If GloTipoUsuario = 40 Then
            '''''LLENAMOS EL COMBO DE LAS CAJERAS (INICIO)
            ControlEfectivoClass.limpiaParametros()
            Me.cmbBuscaCajera.DisplayMember = "nombre"
            Me.cmbBuscaCajera.ValueMember = "clvUsuario"
            Me.cmbBuscaCajera.DataSource = ControlEfectivoClass.ConsultaDT("uspLlenaComboCajera")
            '''''LLENAMOS EL COMBO DE LAS CAJERAS (FIN)
        End If

        '''''LLENAMOS EL GRID CON TODA LA INFORMACIÓN (INICIO)
        SubBusqueda()
        '''''LLENAMOS EL GRID CON TODA LA INFORMACIÓN (FIN)

        '''''LE DAMOS FORMATO AL CAMPO DE IMPORTE PARA QUE NO MUESTRE MUCHOS CEROS (INICIO)
        Me.dgvGastos.Columns(5).DefaultCellStyle.Format = "c"
        '''''LE DAMOS FORMATO AL CAMPO DE IMPORTE PARA QUE NO MUESTRE MUCHOS CEROS (FIN)

        '''''LLENAMOS LAS ETIQUETAS DEL PANEL CON LA INFORMACIÓN DEL REGISTRO SELECCIONADO (INICIO)
        If Me.dgvGastos.RowCount > 0 Then
            Try
                Me.CMBlblIdTipoPago.Text = Me.dgvGastos.SelectedCells(1).Value
                Me.CMBlblFecha.Text = Me.dgvGastos.SelectedCells(2).Value
                Me.CMBlblCajera.Text = Me.dgvGastos.SelectedCells(3).Value
                Me.CMBlblImporte.Text = CDec(Me.dgvGastos.SelectedCells(5).Value).ToString("#.00")
                Me.CMBlblStatus.Text = Me.dgvGastos.SelectedCells(6).Value
            Catch ex As Exception

            End Try
        End If
        '''''LLENAMOS LAS ETIQUETAS DEL PANEL CON LA INFORMACIÓN DEL REGISTRO SELECCIONADO (FIN)
    End Sub

    Private Sub Busqueda(ByVal prmIdGasto As Long, ByVal prmIdTipoGasto As Long, ByVal prmStatus As String, ByVal prmCajera As String, ByVal prmDescripcion As String, ByVal prmLoginUsuario As String)
        '''''LLENAMOS EL GRID CON LA INFORMACIÓN CORRESPONDIENTE (INICIO)
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@idGasto", SqlDbType.BigInt, prmIdGasto)
        ControlEfectivoClass.CreateMyParameter("@idTipoGasto", SqlDbType.BigInt, prmIdTipoGasto)
        ControlEfectivoClass.CreateMyParameter("@status", SqlDbType.VarChar, prmStatus, 1)
        ControlEfectivoClass.CreateMyParameter("@cajera", SqlDbType.VarChar, prmCajera, 5)
        ControlEfectivoClass.CreateMyParameter("@descripcion", SqlDbType.VarChar, prmDescripcion, 250)
        ControlEfectivoClass.CreateMyParameter("@loginUsuario", SqlDbType.VarChar, prmLoginUsuario, 5)

        Me.dgvGastos.DataSource = ControlEfectivoClass.ConsultaDT("uspConsultaGastos")
        '''''LLENAMOS EL GRID CON LA INFORMACIÓN CORRESPONDIENTE (FIN)
    End Sub

    Private Sub Cancelar(ByVal prmIdGasto As Long, ByVal prmIdTipoGasto As Long, ByVal prmDescripcion As String, ByVal prmImporte As Decimal, ByVal prmOp As Integer)
        '''''EJECUTAMOS EL PROCEDIMIENTO QUE EFECTUA LA CANCELACIÓN DE LOS GASTOS (INICIO)
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@idGasto", SqlDbType.BigInt, prmIdGasto)
        ControlEfectivoClass.CreateMyParameter("@idTipoGasto", SqlDbType.BigInt, prmIdTipoGasto)
        ControlEfectivoClass.CreateMyParameter("@descripcion", SqlDbType.VarChar, prmDescripcion, 250)
        ControlEfectivoClass.CreateMyParameter("@importe", SqlDbType.Money, prmImporte)
        ControlEfectivoClass.CreateMyParameter("@op", SqlDbType.Int, prmOp)

        ControlEfectivoClass.Inserta("uspModificaGastos")
        MsgBox("Registro Cancelado Exitosamente", MsgBoxStyle.Information)
        '''''EJECUTAMOS EL PROCEDIMIENTO QUE EFECTUA LA CANCELACIÓN DE LOS GASTOS (FIN)
    End Sub

    Private Sub SubBusqueda()
        If Me.txtBuscaDescripcion.Text.Length = 0 Then
            Me.txtBuscaDescripcion.Text = ""
        End If

        If GloTipoUsuario = 40 Then
            Busqueda(0, Me.cmbBuscaTipoGasto.SelectedValue, Me.cmbBuscaEstatus.SelectedValue, Me.cmbBuscaCajera.SelectedValue, Me.txtBuscaDescripcion.Text, GloUsuario)
        Else
            Busqueda(0, Me.cmbBuscaTipoGasto.SelectedValue, Me.cmbBuscaEstatus.SelectedValue, "TODOS", Me.txtBuscaDescripcion.Text, GloUsuario)
        End If
    End Sub

    Private Sub cmbBuscaEstatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBuscaEstatus.SelectedIndexChanged
        If Me.cmbBuscaTipoGasto.Items.Count > 0 Then
            If GloTipoUsuario = 40 Then
                If Me.cmbBuscaCajera.Items.Count > 0 Then
                    SubBusqueda()
                End If
            Else
                SubBusqueda()
            End If
        End If
    End Sub

    Private Sub cmbBuscaTipoGasto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBuscaTipoGasto.SelectedIndexChanged
        If Me.cmbBuscaTipoGasto.Items.Count > 0 Then
            If GloTipoUsuario = 40 Then
                If Me.cmbBuscaCajera.Items.Count > 0 Then
                    SubBusqueda()
                End If
            Else
                SubBusqueda()
            End If
        End If
    End Sub

    Private Sub btnBuscaDescripcion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscaDescripcion.Click
        SubBusqueda()
        Me.txtBuscaDescripcion.Clear()
    End Sub

    Private Sub cmbBuscaCajera_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBuscaCajera.SelectedIndexChanged
        Busqueda(0, Me.cmbBuscaTipoGasto.SelectedValue, Me.cmbBuscaEstatus.SelectedValue, Me.cmbBuscaCajera.SelectedValue, Me.txtBuscaDescripcion.Text, GloUsuario)
    End Sub

    Private Sub dgvGastos_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvGastos.CurrentCellChanged
        If Me.dgvGastos.RowCount > 0 Then
            Try
                Me.CMBlblIdTipoPago.Text = Me.dgvGastos.SelectedCells(1).Value
                Me.CMBlblFecha.Text = Me.dgvGastos.SelectedCells(2).Value
                Me.CMBlblCajera.Text = Me.dgvGastos.SelectedCells(3).Value
                Me.CMBlblImporte.Text = CDec(Me.dgvGastos.SelectedCells(5).Value).ToString("#.00")
                Me.CMBlblStatus.Text = Me.dgvGastos.SelectedCells(6).Value
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim FRM As New FrmGastos

        FRM.prmClaveGasto = 0
        FRM.prmOpcion = "N"
        FRM.ShowDialog()

        SubBusqueda() '''''ACTUALIZAMOS EL GRID
    End Sub

    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        If Me.dgvGastos.RowCount = 0 Then '''''VALIDAMOS SI NO HAY REGISTRO EN EL GRID
            MsgBox("No hay Registros en la Lista", MsgBoxStyle.Information)
            Exit Sub
        End If

        If IsNumeric(Me.dgvGastos.SelectedCells(0).Value) = False Then '''''VALIDAMOS QUE TENGA SELECCIONADO AL MENOS UN REGISTRO
            MsgBox("Seleccione un Registro de la Lista", MsgBoxStyle.Information)
            Exit Sub
        End If

        Dim FRM As New FrmGastos
        FRM.prmClaveGasto = Me.dgvGastos.SelectedCells(0).Value
        FRM.prmOpcion = "C"
        FRM.ShowDialog()

        SubBusqueda() '''''ACTUALIZAMOS EL GRID
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If Me.dgvGastos.RowCount = 0 Then '''''VALIDAMOS SI NO HAY REGISTRO EN EL GRID
            MsgBox("No hay Registros en la Lista", MsgBoxStyle.Information)
            Exit Sub
        End If

        If IsNumeric(Me.dgvGastos.SelectedCells(0).Value) = False Then '''''VALIDAMOS QUE TENGA SELECCIONADO AL MENOS UN REGISTRO
            MsgBox("Seleccione un Registro de la Lista", MsgBoxStyle.Information)
            Exit Sub
        End If


        If Me.dgvGastos.SelectedCells(6).Value = "C" Then ''''''SE VALIDA QUE EL REGISTRO A MODIFICAR NO ESTÉ CANCELADO ACTUALMENTE
            MsgBox("No puede Modificar Gastos Cancelados", MsgBoxStyle.Information)
            Exit Sub
        End If

        If GloTipoUsuario <> 40 Or GloTipoUsuario <> 1 Then
            If CDate(Me.dgvGastos.SelectedCells(2).Value) <> Today Then '''''SE VALIDA EL REGISTRO A MODIFICAR SE DE LA FECHA DEL DÍA ACTUAL
                MsgBox("No puede Modifcar Gastos de Fechas Anteriores", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If

        If GloTipoUsuario = 40 Then
            If CDate(Me.dgvGastos.SelectedCells(2).Value) <> Today Then '''''SE VALIDA EL REGISTRO A MODIFICAR SE DE LA FECHA DEL DÍA ACTUAL
                Dim res2 = MsgBox("Para esta operación requiere realizar nuevamente el Desglose de Moneda y Arqueo del día:  " + CStr(Me.dgvGastos.SelectedCells(2).Value) + "¿Desea Continuar?", MsgBoxStyle.YesNo)
                If res2 = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
        End If

        Dim FRM As New FrmGastos
        FRM.prmClaveGasto = Me.dgvGastos.SelectedCells(0).Value
        FRM.prmOpcion = "M"
        FRM.prmImporteAnterior = CDec(Me.dgvGastos.SelectedCells(5).Value).ToString("#.00")
        FRM.ShowDialog()

        SubBusqueda() '''''ACTUALIZAMOS EL GRID
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        If Me.dgvGastos.SelectedCells(6).Value = "C" Then ''''''SE VALIDA QUE EL REGISTRO A CANCELAR NO ESTÉ CANCELADO ACTUALMENTE
            MsgBox("El Registro Seleccionado ya ha sido Cancelado", MsgBoxStyle.Information)
            Exit Sub
        End If

        If GloTipoUsuario <> 40 Then

            If CDate(Me.dgvGastos.SelectedCells(2).Value) <> Today Then '''''SE VALIDA EL REGISTRO A CANCELAR SE DE LA FECHA DEL DÍA ACTUAL
                MsgBox("No puede Cancelar Gastos de Fechas Anteriores", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If

        Dim res2 = MsgBox("Para esta operación requiere realizar nuevamente el Desglose de Moneda y Arqueo del día:  " + CStr(Me.dgvGastos.SelectedCells(2).Value) + "¿Desea Continuar?", MsgBoxStyle.YesNo)
        If res2 = MsgBoxResult.Yes Then
            Dim res = MsgBox("¿Realmente Deseas Cancelar el Gasto Selecionado?", MsgBoxStyle.YesNo)
            If res = MsgBoxResult.Yes Then
                Cancelar(Me.dgvGastos.SelectedCells(0).Value, 0, "", 0, 1)
                bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Se Cancela Gasto # " + CStr(Me.dgvGastos.SelectedCells(0).Value) + " por: $" + CDec(Me.dgvGastos.SelectedCells(5).Value).ToString("#.00") + " de la cajera: " + CStr(Me.dgvGastos.SelectedCells(3).Value), "", SubCiudad)
            Else
                Exit Sub
            End If
        Else
            Exit Sub
        End If
        SubBusqueda() '''''ACTUALIZAMOS EL GRID
    End Sub
End Class
﻿Public Class FrmSeleccionaSucursalPoliza

    Private Sub DameClv_Session_Servicios()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameClv_Session_Servicios")
        gloClv_Session = BaseII.dicoPar("@Clv_Session").ToString()
    End Sub

    Private Sub CONTABLA_POLIZASucursalTmp(ByVal Clv_Session As Integer, ByVal Op As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, Op)
        If Op = 0 Then
            dgvIzq.DataSource = BaseII.ConsultaDT("CONTABLA_POLIZASucursalTmp")
        ElseIf Op = 1 Then
            dgvDer.DataSource = BaseII.ConsultaDT("CONTABLA_POLIZASucursalTmp")
        End If
    End Sub

    Private Sub NUETABLA_POLIZASucursalTmp(ByVal Clv_Session As Integer, ByVal Op As Integer, ByVal Clv_Sucursal As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Sucursal", SqlDbType.Int, Clv_Sucursal)
        BaseII.Inserta("NUETABLA_POLIZASucursalTmp")
    End Sub

    Private Sub BORTABLA_POLIZASucursalTmp(ByVal Clv_Session As Integer, ByVal Op As Integer, ByVal Clv_Sucursal As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Sucursal", SqlDbType.Int, Clv_Sucursal)
        BaseII.Inserta("BORTABLA_POLIZASucursalTmp")
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If dgvIzq.RowCount = 0 Then Exit Sub
        NUETABLA_POLIZASucursalTmp(gloClv_Session, 0, dgvIzq.SelectedCells(0).Value)
        CONTABLA_POLIZASucursalTmp(gloClv_Session, 0)
        CONTABLA_POLIZASucursalTmp(gloClv_Session, 1)
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        If dgvIzq.RowCount = 0 Then Exit Sub
        NUETABLA_POLIZASucursalTmp(gloClv_Session, 1, 0)
        CONTABLA_POLIZASucursalTmp(gloClv_Session, 0)
        CONTABLA_POLIZASucursalTmp(gloClv_Session, 1)
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        If dgvDer.RowCount = 0 Then Exit Sub
        BORTABLA_POLIZASucursalTmp(gloClv_Session, 0, dgvDer.SelectedCells(0).Value)
        CONTABLA_POLIZASucursalTmp(gloClv_Session, 0)
        CONTABLA_POLIZASucursalTmp(gloClv_Session, 1)
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        If dgvDer.RowCount = 0 Then Exit Sub
        BORTABLA_POLIZASucursalTmp(gloClv_Session, 1, 0)
        CONTABLA_POLIZASucursalTmp(gloClv_Session, 0)
        CONTABLA_POLIZASucursalTmp(gloClv_Session, 1)
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona al menos una sucursal.")
            Exit Sub
        End If
        FrmFechaIngresosConcepto.Show()
        Me.Close()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmSeleccionaSucursalPoliza_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me)
        DameClv_Session_Servicios()
        CONTABLA_POLIZASucursalTmp(gloClv_Session, 0)
        CONTABLA_POLIZASucursalTmp(gloClv_Session, 1)
    End Sub
End Class
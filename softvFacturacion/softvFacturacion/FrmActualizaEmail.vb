﻿Imports System.Data.SqlClient

Public Class FrmActualizaEmail

    Private Sub FrmActualizaEmail_FormClosed(sender As Object, e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        TextBox1.Text = ""
        TextBox2.Text = ""
    End Sub

    Private Sub FrmActualizaEmail_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        ConEmail()
    End Sub

    Private Sub ConEmail()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConEmail", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = GloContrato
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Email", SqlDbType.VarChar, 100)
        parametro1.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Telefono", SqlDbType.VarChar, 100)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            Label4.Text = parametro1.Value
            Label5.Text = parametro2.Value
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click

        If TextBox1.TextLength > 0 Or TextBox2.TextLength > 0 Then
            If TextBox2.TextLength > 0 And TextBox2.TextLength < 10 Then
                MessageBox.Show("El teléfono debe contener 10 digitos.")
                Exit Sub
            End If
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, GloContrato)
            BaseII.CreateMyParameter("@Email", SqlDbType.VarChar, TextBox1.Text, 100)
            BaseII.CreateMyParameter("@Telefono", SqlDbType.VarChar, TextBox2.Text, 100)
            BaseII.Inserta("ModActualizaEmailCliente")

            MessageBox.Show("Se guardó con éxito.")
            TextBox1.Text = ""
            TextBox2.Text = ""

            Me.Close()
        Else
            MessageBox.Show("Capture el Email y/o Teléfono, o de click en Conservar Datos Actuales")
        End If

    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        TextBox1.Text = ""
        TextBox2.Text = ""
        Me.Close()
    End Sub

    Private Sub Button2_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles Button2.KeyPress

    End Sub

    Private Sub TextBox2_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        e.KeyChar = ChrW(ValidaKey(TextBox2, Asc(e.KeyChar), "N"))
    End Sub

    Private Sub TextBox2_TextChanged(sender As System.Object, e As System.EventArgs) Handles TextBox2.TextChanged

    End Sub
End Class
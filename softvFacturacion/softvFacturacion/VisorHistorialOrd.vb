Imports System.Data.SqlClient
Public Class VisorHistorialOrd
    Dim autom As Boolean
    Dim bndbuscastatus As Boolean = False

    Private Sub VisorHistorialOrd_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        'Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        'Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        'GloClv_TipSer = Me.ComboBox4.SelectedValue
        Busca(30)
        bndbuscastatus = True
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub consultar()
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            If gloClave > 0 And Me.Clv_calleLabel2.Text > 0 Then
                OPCION = "C"
                GloBnd = False
                gloClave = Me.Clv_calleLabel2.Text
                'GloClv_TipSer = Me.Label9.Text
                LiTipo = 4
                FrmImprimir.Show()
            Else
                MsgBox("Seleccione la fila que desea Consultar")
            End If
        Else
            MsgBox("No tiene una Orden seleccionada", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        consultar()
    End Sub

    Private Sub modificar()
        If gloClave > 0 Then
            OPCION = "M"
            GloBnd = False
            gloClave = Me.Clv_calleLabel2.Text
            'GloClv_TipSer = Me.ComboBox4.SelectedValue
            'FrmOrdSer.Show()
        End If
    End Sub


    Private Sub Busca(ByVal op As Integer)
        Dim sTATUS As String = "P"
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            'If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
            If Me.RadioButton1.Checked = True Then
                sTATUS = "P"
            ElseIf Me.RadioButton2.Checked = True Then
                sTATUS = "E"
            ElseIf Me.RadioButton3.Checked = True Then
                sTATUS = "V"
            End If
            BaseII.limpiaParametros()
            If op = 30 Then 'contrato

                If IsNumeric(GloContrato) = True Then
                    Me.BUSCAORDSERTableAdapter.Connection = CON
                    'Me.BUSCAORDSERTableAdapter.Fill(Me.LydiaDataSet2.BUSCAORDSER, 0, 0, GloContrato, sTATUS, "", "", 300, False)
                    BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
                    BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, 0)
                    BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, GloContrato)
                    BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, sTATUS)
                    BaseII.CreateMyParameter("@CALLE", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@NUMERO", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@OP", SqlDbType.Int, 300)
                    BaseII.CreateMyParameter("@procauto", SqlDbType.Bit, False)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            ElseIf op = 499 Then 'contrato
                Me.BUSCAORDSERTableAdapter.Connection = CON
                'Me.BUSCAORDSERTableAdapter.Fill(Me.LydiaDataSet2.BUSCAORDSER, 0, 0, GloContrato, sTATUS, "", "", 4990, False)
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, 0)
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, GloContrato)
                BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, sTATUS)
                BaseII.CreateMyParameter("@CALLE", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@NUMERO", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@OP", SqlDbType.Int, 4990)
                BaseII.CreateMyParameter("@procauto", SqlDbType.Bit, False)
                'ElseIf op = 31 Then
                '    If Len(Trim(Me.TextBox2.Text)) > 0 Then
                '        Me.BUSCAORDSERTableAdapter.Connection = CON
                '        Me.BUSCAORDSERTableAdapter.Fill(Me.LydiaDataSet2.BUSCAORDSER, 0, 0, GloContrato, Me.TextBox2.Text, "", "", 310, False)
                '    Else
                '        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                '    End If
                'ElseIf op = 32 Then 'Calle y numero
                '    Me.BUSCAORDSERTableAdapter.Connection = CON
                '    Me.BUSCAORDSERTableAdapter.Fill(Me.LydiaDataSet2.BUSCAORDSER, 0, 0, GloContrato, "", Me.BCALLE.Text, Me.BNUMERO.Text, 320, False)
                'ElseIf op = 33 Then 'clv_Orden
                '    If IsNumeric(Me.TextBox3.Text) = True Then
                '        Me.BUSCAORDSERTableAdapter.Connection = CON
                '        Me.BUSCAORDSERTableAdapter.Fill(Me.LydiaDataSet2.BUSCAORDSER, 0, Me.TextBox3.Text, GloContratoVer, "", "", "", 330, False)
                '    Else
                '        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                '    End If
            Else
                Me.BUSCAORDSERTableAdapter.Connection = CON
                'Me.BUSCAORDSERTableAdapter.Fill(Me.LydiaDataSet2.BUSCAORDSER, 0, 0, 0, "", "", "", 340, False)
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, 0)
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, 0)
                BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@CALLE", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@NUMERO", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@OP", SqlDbType.Int, 340)
                BaseII.CreateMyParameter("@procauto", SqlDbType.Bit, False)
            End If
            DataGridView1.DataSource = BaseII.ConsultaDT("BUSCAORDSER")
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub cambioseleccion()
        Try
            Clv_calleLabel2.Text = DataGridView1.SelectedCells(0).Value
            ContratoLabel1.Text = DataGridView1.SelectedCells(2).Value
            CMBNombreTextBox.Text = DataGridView1.SelectedCells(3).Value
            CALLELabel1.Text = DataGridView1.SelectedCells(4).Value
            NUMEROLabel1.Text = DataGridView1.SelectedCells(5).Value
        Catch ex As Exception

        End Try

    End Sub

    Private Sub BRWORDSER_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            'GloClv_TipSer = Me.ComboBox4.SelectedValue
            Busca(30)
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        gloClave = Me.Clv_calleLabel2.Text
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        End If
    End Sub


    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If bndbuscastatus = True Then
            Busca(499)
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If bndbuscastatus = True Then
            Busca(499)
        End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        If bndbuscastatus = True Then
            Busca(499)
        End If
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        cambioseleccion()
    End Sub
End Class
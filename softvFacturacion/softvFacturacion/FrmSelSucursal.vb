﻿Imports System.Data.SqlClient
Public Class FrmSelSucursal

    Private Sub FrmSelCompania_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)

        DameSession()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, -1)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(eClv_Session))
        BaseII.CreateMyParameter("@idSucursal", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionSucursal")
        llenalistboxs()
    End Sub
    Private Sub llenalistboxs()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(eClv_Session))
        BaseII.CreateMyParameter("@idSucursal", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        loquehay.DataSource = BaseII.ConsultaDT("ProcedureSeleccionSucursal")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(eClv_Session))
        BaseII.CreateMyParameter("@idSucursal", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        seleccion.DataSource = BaseII.ConsultaDT("ProcedureSeleccionSucursal")
    End Sub

    Private Sub agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(eClv_Session))
        BaseII.CreateMyParameter("@idSucursal", SqlDbType.Int, loquehay.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionSucursal")
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 5)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(eClv_Session))
        BaseII.CreateMyParameter("@idSucursal", SqlDbType.Int, seleccion.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionSucursal")
        llenalistboxs()
    End Sub

    Private Sub agregartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 6)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(eClv_Session))
        BaseII.CreateMyParameter("@idSucursal", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionSucursal")
        llenalistboxs()
    End Sub

    Private Sub quitartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 7)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(eClv_Session))
        BaseII.CreateMyParameter("@idSucursal", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionSucursal")
        llenalistboxs()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
       
        If varfrmselsucursal = "relingreso" Then
            GloBnd_Des_Men = False
            GloBnd_Des_Cont = False
            LocbndPolizaCiudad = False
            FrmFechaIngresosConcepto.Show()
        End If
        GloBndDesgloceNew = True

        Me.Close()
    End Sub

    Private Sub DameSession()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Dame_clv_session_Reportes", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            conexion.Dispose()

            eClv_Session = CLng(parametro.Value)

        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

End Class
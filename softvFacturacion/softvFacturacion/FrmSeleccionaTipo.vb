Public Class FrmSeleccionaTipo

    Private Sub FrmSeleccionaTipo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)

        Me.ComboBox1.Items.Add("Historial de Pagos")
        If IdSistema <> "LO" Then
            Me.ComboBox1.Items.Add("Historial de Ordenes de Servicio")
        End If
        'Me.ComboBox1.Items.Add("Historial de Pagos")
        Me.ComboBox1.Items.Add("Historial de Quejas")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click

        If IsNumeric(GloContrato) = True And GloContrato > 0 Then
            GloOpFacturas = 3
            eBotonGuardar = False
            If Me.ComboBox1.Text = "Historial de Pagos" Then
                BrwFacturas_Cancelar.Show()
            ElseIf Me.ComboBox1.Text = "Historial de Ordenes de Servicio" Then
                VisorHistorialOrd.Show()
            ElseIf Me.ComboBox1.Text = "Historial de Quejas" Then
                VisorHistorialQuejas.Show()
            End If
        Else
            MsgBox("Seleccione un Cliente por favor", MsgBoxStyle.Information)
        End If
        Me.Close()

       
    End Sub
End Class
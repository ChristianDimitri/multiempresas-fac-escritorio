﻿Imports System.Collections.Generic
Imports System.Xml
Imports System.IO

Public Class FrmFiltroGastos
    Dim Dicociclo As New Dictionary(Of Integer, DataRow)
    Dim LDciclo As New DataTable
    Dim LIciclo As New DataTable


    Private Sub llenaComboCajeras()
        '''''LLENAMOS EL COMBO DE LAS CAJERAS (INICIO)
        ControlEfectivoClass.limpiaParametros()
        Me.cmbCajera.DisplayMember = "nombre"
        Me.cmbCajera.ValueMember = "clvUsuario"
        Me.cmbCajera.DataSource = ControlEfectivoClass.ConsultaDT("uspLlenaComboCajera")
        '''''LLENAMOS EL COMBO DE LAS CAJERAS (FIN)
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        If CDate(Me.dtpFechaInicial.Value.ToShortDateString) > CDate(Me.dtpFechaFinal.Value.ToShortDateString) Then ''''VALIDAMOS QUE LA FECHA INICIAL NO SEA MAYOR A LA FINAL
            MsgBox("La Fecha Inicial no puede ser Mayor a la Fecha Final", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.cmbCajera.Text.Length = 0 Then ''''VALIDAMOS QUE HAYA SELECCIONADO AL MENOS UN REGISTRO EN LOS CAJEROS
            MsgBox("Seleccione una opción en el Cajero(a)", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.lbxAgregados.Items.Count = 0 Then
            MsgBox("Seleccione al menos un Tipo de Gasto", MsgBoxStyle.Information)
            Exit Sub
        End If

        uspReporteTiposGastos(GeneraXml(), Me.dtpFechaInicial.Value, Me.dtpFechaFinal.Value)
        Me.Close()
    End Sub

    Private Sub uspReporteTiposGastos(ByVal prmXml As String, ByVal prmFechaIni As Date, ByVal prmFechaFin As Date)
        '''''MANDAMOS EL REPORTE (INICIO)
        ControlEfectivoClass.limpiaParametros() ''LIMPIAMOS LA LISTA DE PARÁMETROS

        ControlEfectivoClass.CreateMyParameter("@xml", SqlDbType.Xml, prmXml) ''MANDAMOS LOS PARÁMETROS

        Dim listaTablas As New List(Of String) ''MANDAMOS EL NOMBRE DE LAS TABLAS QUE DEVOLVERÁ EL DATASET DEL REPORTE
        listaTablas.Add("uspReporteTiposGastos")

        Dim DataSet As New DataSet ''LLENAMOS EL DATASET QUE LLENARÁ EL REPORTE
        DataSet = ControlEfectivoClass.ConsultaDS("uspReporteTiposGastos", listaTablas)

        Dim diccioFormulasReporte As New Dictionary(Of String, String) ''LENAMOS EL DICCIONARIO QUE CONTENDRÁ LAS FÓRMULAS QUE REQUIERE EL REPORTE
        diccioFormulasReporte.Add("Ciudad", LocNomEmpresa)
        diccioFormulasReporte.Add("Empresa", GloNomSucursal)
        diccioFormulasReporte.Add("fechaFin", prmFechaFin)
        diccioFormulasReporte.Add("fechaIni", prmFechaIni)

        ControlEfectivoClass.llamarReporteCentralizado(RutaReportes + "/rptReporteTiposGastos", DataSet, diccioFormulasReporte) ''MANDAMOS LLAMAR EL REPORTE
        '''''MANDAMOS EL REPORTE (INICIO)
    End Sub

    Private Sub FrmFiltroGastos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        llenaComboCajeras() ''''LLENAMOS EL COMBO DE LOS CAJEROS
        llenaListBoxPorAgregar() '''''LLENAMOS LA LISTA
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub llenarLB()
        Dim LDcicloX As New DataTable
        Dim LIcicloX As New DataTable

        LDcicloX = LDciclo.Clone()
        LIcicloX = LIciclo.Clone()

        LIcicloX.PrimaryKey = {LIcicloX.Columns("idGasto")}

        For i As Integer = 0 To LIciclo.Rows.Count - 1
            LIcicloX.ImportRow(LIciclo.Rows(i))
        Next

        For i As Integer = 0 To LDciclo.Rows.Count - 1
            LDcicloX.ImportRow(LDciclo.Rows(i))
        Next

        OrdenameEsta(LDciclo, LDcicloX)
        OrdenameEsta(LIciclo, LIcicloX)

        LlenameEsta(lbxPorAgregar, LIcicloX)
        LlenameEsta(lbxAgregados, LDcicloX)
    End Sub

    Private Sub LlenameEsta(ByRef lb As ListBox, ByRef dt As DataTable)
        lb.DataSource = Nothing
        lb.Items.Clear()
        lb.DisplayMember = "descripcion"
        lb.ValueMember = "idGasto"
        lb.DataSource = dt
    End Sub


    Private Sub OrdenameEsta(ByRef dto As DataTable, ByRef dtn As DataTable)
        Dim lint As New List(Of Integer)

        For Each dr As DataRow In dtn.Rows
            lint.Add(CInt(dr("idGasto")))
        Next
        lint.Sort()
        dtn.Rows.Clear()

        For Each i As Integer In lint
            dtn.ImportRow(dto.Rows.Find(i))
        Next
        dto.Rows.Clear()

        For Each i As Integer In lint
            dto.ImportRow(dtn.Rows.Find(i))
        Next
    End Sub

    Private Sub llenaListBoxPorAgregar()
        LIciclo = (ControlEfectivoClass.ConsultaDT("uspLlenaComboTipoGastoFiltro"))
        LIciclo.PrimaryKey = {LIciclo.Columns("idGasto")}
        For Each dr As DataRow In LIciclo.Rows()

            Dicociclo.Add(dr("idGasto"), dr)
        Next
        LDciclo = LIciclo.Clone()
        llenarLB()
    End Sub

    Private Sub btnAgregarUno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarUno.Click
        If lbxPorAgregar.SelectedItems.Count > 0 Then
            LDciclo.ImportRow(LIciclo.Rows(CInt(lbxPorAgregar.SelectedIndex.ToString())))
            LIciclo.Rows.RemoveAt(CInt(lbxPorAgregar.SelectedIndex))
            llenarLB()
        End If
    End Sub

    Private Sub btnAgregarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarTodos.Click
        If lbxPorAgregar.Items.Count > 0 Then
            For Each dr As DataRow In LIciclo.Rows
                LDciclo.ImportRow(dr)
            Next
            LIciclo.Rows.Clear()
            llenarLB()
        End If
    End Sub

    Private Sub btnQuitarUno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuitarUno.Click
        If lbxAgregados.SelectedItems.Count > 0 Then
            LIciclo.ImportRow(LDciclo.Rows(CInt(lbxAgregados.SelectedIndex.ToString())))
            LDciclo.Rows.RemoveAt(CInt(lbxAgregados.SelectedIndex))
            llenarLB()
        End If
    End Sub

    Private Sub btnQuitarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuitarTodos.Click
        If lbxAgregados.Items.Count > 0 Then
            For Each dr As DataRow In LDciclo.Rows
                LIciclo.ImportRow(dr)
            Next
            LDciclo.Rows.Clear()
            llenarLB()
        End If
    End Sub

    Public Function GeneraXml() As String
        Dim sw As StringWriter = New StringWriter()
        Dim w As XmlTextWriter = New XmlTextWriter(sw)
        w.Formatting = Formatting.Indented
        w.WriteStartElement("root")
        w.WriteStartElement("TiposGastos")
        w.WriteAttributeString("clvUsuario", Me.cmbCajera.SelectedValue)
        w.WriteAttributeString("FechaI", CDate(Me.dtpFechaInicial.Value.ToShortDateString()))
        w.WriteAttributeString("FechaF", CDate(Me.dtpFechaFinal.Value.ToShortDateString()))

        For Each dr As DataRow In LDciclo.Rows
            w.WriteStartElement("Gastos")
            w.WriteAttributeString("idGasto", dr("idGasto"))
            w.WriteEndElement()
        Next
        w.WriteEndElement()
        w.WriteEndElement()
        w.Close()
        GeneraXml = sw.ToString()
    End Function
End Class
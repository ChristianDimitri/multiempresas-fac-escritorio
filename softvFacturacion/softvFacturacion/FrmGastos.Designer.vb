﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGastos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBlblClaveGasto = New System.Windows.Forms.Label()
        Me.CMBlblFecha = New System.Windows.Forms.Label()
        Me.CMBlblTipoGasto = New System.Windows.Forms.Label()
        Me.CMBlblDescripcion = New System.Windows.Forms.Label()
        Me.CMBlblImporte = New System.Windows.Forms.Label()
        Me.gbIngresaGastos = New System.Windows.Forms.GroupBox()
        Me.txtEfectivoCobrado = New System.Windows.Forms.TextBox()
        Me.lblCobradoEfectivo = New System.Windows.Forms.Label()
        Me.txtImporte = New System.Windows.Forms.TextBox()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.cmbTipoGasto = New System.Windows.Forms.ComboBox()
        Me.txtUsuario = New System.Windows.Forms.TextBox()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.txtClaveGasto = New System.Windows.Forms.TextBox()
        Me.CMBlblUsuario = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.gbIngresaGastos.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBlblClaveGasto
        '
        Me.CMBlblClaveGasto.AutoSize = True
        Me.CMBlblClaveGasto.Location = New System.Drawing.Point(56, 23)
        Me.CMBlblClaveGasto.Name = "CMBlblClaveGasto"
        Me.CMBlblClaveGasto.Size = New System.Drawing.Size(101, 16)
        Me.CMBlblClaveGasto.TabIndex = 0
        Me.CMBlblClaveGasto.Text = "Clave Gasto :"
        '
        'CMBlblFecha
        '
        Me.CMBlblFecha.AutoSize = True
        Me.CMBlblFecha.Location = New System.Drawing.Point(98, 55)
        Me.CMBlblFecha.Name = "CMBlblFecha"
        Me.CMBlblFecha.Size = New System.Drawing.Size(59, 16)
        Me.CMBlblFecha.TabIndex = 1
        Me.CMBlblFecha.Text = "Fecha :"
        '
        'CMBlblTipoGasto
        '
        Me.CMBlblTipoGasto.AutoSize = True
        Me.CMBlblTipoGasto.Location = New System.Drawing.Point(42, 153)
        Me.CMBlblTipoGasto.Name = "CMBlblTipoGasto"
        Me.CMBlblTipoGasto.Size = New System.Drawing.Size(115, 16)
        Me.CMBlblTipoGasto.TabIndex = 2
        Me.CMBlblTipoGasto.Text = "Tipo de Gasto :"
        '
        'CMBlblDescripcion
        '
        Me.CMBlblDescripcion.AutoSize = True
        Me.CMBlblDescripcion.Location = New System.Drawing.Point(58, 187)
        Me.CMBlblDescripcion.Name = "CMBlblDescripcion"
        Me.CMBlblDescripcion.Size = New System.Drawing.Size(99, 16)
        Me.CMBlblDescripcion.TabIndex = 3
        Me.CMBlblDescripcion.Text = "Descripción :"
        '
        'CMBlblImporte
        '
        Me.CMBlblImporte.AutoSize = True
        Me.CMBlblImporte.Location = New System.Drawing.Point(89, 273)
        Me.CMBlblImporte.Name = "CMBlblImporte"
        Me.CMBlblImporte.Size = New System.Drawing.Size(68, 16)
        Me.CMBlblImporte.TabIndex = 4
        Me.CMBlblImporte.Text = "Importe :"
        '
        'gbIngresaGastos
        '
        Me.gbIngresaGastos.Controls.Add(Me.txtEfectivoCobrado)
        Me.gbIngresaGastos.Controls.Add(Me.lblCobradoEfectivo)
        Me.gbIngresaGastos.Controls.Add(Me.txtImporte)
        Me.gbIngresaGastos.Controls.Add(Me.txtDescripcion)
        Me.gbIngresaGastos.Controls.Add(Me.cmbTipoGasto)
        Me.gbIngresaGastos.Controls.Add(Me.txtUsuario)
        Me.gbIngresaGastos.Controls.Add(Me.dtpFecha)
        Me.gbIngresaGastos.Controls.Add(Me.txtClaveGasto)
        Me.gbIngresaGastos.Controls.Add(Me.CMBlblUsuario)
        Me.gbIngresaGastos.Controls.Add(Me.CMBlblClaveGasto)
        Me.gbIngresaGastos.Controls.Add(Me.CMBlblImporte)
        Me.gbIngresaGastos.Controls.Add(Me.CMBlblFecha)
        Me.gbIngresaGastos.Controls.Add(Me.CMBlblDescripcion)
        Me.gbIngresaGastos.Controls.Add(Me.CMBlblTipoGasto)
        Me.gbIngresaGastos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbIngresaGastos.Location = New System.Drawing.Point(12, 12)
        Me.gbIngresaGastos.Name = "gbIngresaGastos"
        Me.gbIngresaGastos.Size = New System.Drawing.Size(447, 301)
        Me.gbIngresaGastos.TabIndex = 5
        Me.gbIngresaGastos.TabStop = False
        '
        'txtEfectivoCobrado
        '
        Me.txtEfectivoCobrado.Location = New System.Drawing.Point(163, 119)
        Me.txtEfectivoCobrado.Name = "txtEfectivoCobrado"
        Me.txtEfectivoCobrado.ReadOnly = True
        Me.txtEfectivoCobrado.Size = New System.Drawing.Size(140, 22)
        Me.txtEfectivoCobrado.TabIndex = 8
        Me.txtEfectivoCobrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblCobradoEfectivo
        '
        Me.lblCobradoEfectivo.AutoSize = True
        Me.lblCobradoEfectivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCobradoEfectivo.Location = New System.Drawing.Point(6, 122)
        Me.lblCobradoEfectivo.Name = "lblCobradoEfectivo"
        Me.lblCobradoEfectivo.Size = New System.Drawing.Size(151, 16)
        Me.lblCobradoEfectivo.TabIndex = 9
        Me.lblCobradoEfectivo.Text = "Efectivo Disponible :"
        '
        'txtImporte
        '
        Me.txtImporte.Location = New System.Drawing.Point(163, 270)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(140, 22)
        Me.txtImporte.TabIndex = 5
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Location = New System.Drawing.Point(163, 184)
        Me.txtDescripcion.Multiline = True
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(270, 78)
        Me.txtDescripcion.TabIndex = 4
        '
        'cmbTipoGasto
        '
        Me.cmbTipoGasto.FormattingEnabled = True
        Me.cmbTipoGasto.Location = New System.Drawing.Point(163, 150)
        Me.cmbTipoGasto.Name = "cmbTipoGasto"
        Me.cmbTipoGasto.Size = New System.Drawing.Size(270, 24)
        Me.cmbTipoGasto.TabIndex = 3
        '
        'txtUsuario
        '
        Me.txtUsuario.Location = New System.Drawing.Point(163, 86)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.ReadOnly = True
        Me.txtUsuario.Size = New System.Drawing.Size(140, 22)
        Me.txtUsuario.TabIndex = 2
        Me.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'dtpFecha
        '
        Me.dtpFecha.Checked = False
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(163, 52)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(106, 22)
        Me.dtpFecha.TabIndex = 1
        '
        'txtClaveGasto
        '
        Me.txtClaveGasto.Location = New System.Drawing.Point(163, 21)
        Me.txtClaveGasto.Name = "txtClaveGasto"
        Me.txtClaveGasto.ReadOnly = True
        Me.txtClaveGasto.Size = New System.Drawing.Size(140, 22)
        Me.txtClaveGasto.TabIndex = 0
        Me.txtClaveGasto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CMBlblUsuario
        '
        Me.CMBlblUsuario.AutoSize = True
        Me.CMBlblUsuario.Location = New System.Drawing.Point(87, 89)
        Me.CMBlblUsuario.Name = "CMBlblUsuario"
        Me.CMBlblUsuario.Size = New System.Drawing.Size(70, 16)
        Me.CMBlblUsuario.TabIndex = 5
        Me.CMBlblUsuario.Text = "Usuario :"
        '
        'btnAceptar
        '
        Me.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(71, 322)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(112, 32)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "&Guardar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(278, 322)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(112, 32)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'FrmGastos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(471, 361)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.gbIngresaGastos)
        Me.Name = "FrmGastos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ingresar Gastos"
        Me.gbIngresaGastos.ResumeLayout(False)
        Me.gbIngresaGastos.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CMBlblClaveGasto As System.Windows.Forms.Label
    Friend WithEvents CMBlblFecha As System.Windows.Forms.Label
    Friend WithEvents CMBlblTipoGasto As System.Windows.Forms.Label
    Friend WithEvents CMBlblDescripcion As System.Windows.Forms.Label
    Friend WithEvents CMBlblImporte As System.Windows.Forms.Label
    Friend WithEvents gbIngresaGastos As System.Windows.Forms.GroupBox
    Friend WithEvents txtImporte As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents cmbTipoGasto As System.Windows.Forms.ComboBox
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents dtpFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtClaveGasto As System.Windows.Forms.TextBox
    Friend WithEvents CMBlblUsuario As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents txtEfectivoCobrado As System.Windows.Forms.TextBox
    Friend WithEvents lblCobradoEfectivo As System.Windows.Forms.Label
End Class

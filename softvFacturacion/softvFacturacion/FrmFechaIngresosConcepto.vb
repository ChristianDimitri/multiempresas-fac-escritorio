Imports System.Data.SqlClient

Public Class FrmFechaIngresosConcepto

    Private Sub FrmFechaIngresosConcepto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Me.DateTimePicker2.Value = Today
        Me.DateTimePicker1.Value = Today
        Llena_companias()
        Me.DateTimePicker2.MaxDate = Today
    End Sub
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_Corte_Facturas")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedValue = 999

            End If
            GloIdCompania = 999
            
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eFechaInicial = Me.DateTimePicker1.Value
        eFechaFinal = Me.DateTimePicker2.Value
        NombreCompania = ComboBoxCompanias.Text
        'Cambios 2000
        If LocBndrelingporconceptosCiudad = True Then
            LocBndrelingporconceptosCiudad = False
            LocbndDesPagosCiudad = True
            FrmImprimirRepGral.Show()
        ElseIf LocBndrelingporconceptosSucursal = True Then
            LocBndrelingporconceptosSucursal = False
            LocbndDesPagosSucursal = True
            FrmImprimirRepGral.Show()
        ElseIf GloBnd_Des_Men = True Then
            FrmImprimirIngresosConcepto.Show()
        ElseIf LocbndPolizaCiudad = True Then
            GeneraPoliza()
            Locbndactualizapoiza = True
        Else
            GloBndDesgloceNew = True
            If locband_pant = 1011 Then FrmImprimirIngresosConcepto.Show()
            'FrmImprimirIngresosConcepto.Show()
        End If
        Me.Close()
    End Sub

    Private Sub GeneraPoliza()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, DateTimePicker1.Value)
        BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, DateTimePicker2.Value)
        BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, "C", 1)
        BaseII.CreateMyParameter("@SUCURSAL", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@CAJA", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@CAJERA", SqlDbType.VarChar, GloUsuario, 11)
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, gloClv_Session)
        BaseII.CreateMyParameter("@CLV_FACTURA", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@CLV_USUARIO", SqlDbType.VarChar, GloUsuario, 11)
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, GloClvCompania)
        BaseII.CreateMyParameter("@CLV_LLAVE_POLIZANEW", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@NUMERROR", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@MSJERROR", ParameterDirection.Output, SqlDbType.VarChar, 250)
        BaseII.ProcedimientoOutPut("GeneraPoliza")
        LocGloClv_poliza = CInt(BaseII.dicoPar("@CLV_LLAVE_POLIZANEW").ToString())

        LocopPoliza = "N"
        bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Se gener� p�liza", "Con clave de p�liza: " + CStr(LocGloClv_poliza), LocClv_Ciudad)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_llave_Poliza", SqlDbType.Int, LocGloClv_poliza)
        BaseII.CreateMyParameter("@opc", SqlDbType.VarChar, LocopPoliza, 5)
        BaseII.CreateMyParameter("@clave", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("Dame_Clave_poliza")
        GloPoliza2 = CInt(BaseII.dicoPar("@clave").ToString())

        FrmPoliza.Show()
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
        Catch ex As Exception

        End Try
    End Sub
End Class
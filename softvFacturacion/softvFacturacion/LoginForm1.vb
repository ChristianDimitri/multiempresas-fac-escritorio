Imports System
Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports Microsoft.VisualBasic
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Text

Public Class LoginForm1
    Dim objDraw As System.Drawing.Drawing2D.GraphicsPath = _
  New System.Drawing.Drawing2D.GraphicsPath
    Dim objDraw2 As System.Drawing.Drawing2D.GraphicsPath = _
    New System.Drawing.Drawing2D.GraphicsPath


    Private Sub OBTEN_IP()
        Dim host As String
        host = Dns.GetHostName
        GloHostName = host
        'Dim hostNameOrAddress As String
        Dim returnValue As IPAddress()

        returnValue = Dns.GetHostAddresses(host)
        '  MsgBox(returnValue.GetValue(0).ToString)
        GloIpMaquina = returnValue.GetValue(0).ToString
    End Sub

    ' TODO: inserte el código para realizar autenticación personalizada usando el nombre de usuario y la contraseña proporcionada 
    ' (Consulte http://go.microsoft.com/fwlink/?LinkId=35339).  
    ' El objeto principal personalizado se puede adjuntar al objeto principal del subproceso actual como se indica a continuación: 
    '     My.User.CurrentPrincipal = CustomPrincipal
    ' donde CustomPrincipal es la implementación de IPrincipal utilizada para realizar la autenticación. 
    ' Posteriormente, My.User devolverá la información de identidad encapsulada en el objeto CustomPrincipal
    ' como el nombre de usuario, nombre para mostrar, etc.
    Private Sub acceso()
        Try
            Dim bnd As Integer = 0
            Dim Bnd_1 As Integer = 0
            Dim eBnd As Integer = 0
            OBTEN_IP()
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim comando As New SqlClient.SqlCommand

            With Comando
                .Connection = CON
                .CommandText = "ConGeneralRelBD"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                '' Create a SqlParameter for each parameter in the stored procedure.
                Dim prm As New SqlParameter("@Clv_Id", SqlDbType.Int)
                Dim prm1 As New SqlParameter("@NombreBD", SqlDbType.VarChar, 250)
                Dim prm2 As New SqlParameter("@Clave_Txt", SqlDbType.VarChar, 50)
                Dim prm3 As New SqlParameter("@Ciudad", SqlDbType.VarChar, 250)

                prm.Direction = ParameterDirection.Output
                prm1.Direction = ParameterDirection.Output
                prm2.Direction = ParameterDirection.Output
                prm3.Direction = ParameterDirection.Output

                prm.Value = 0
                prm1.Value = " "
                prm2.Value = " "
                prm3.Value = " "
                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)
                Dim i As Integer = Comando.ExecuteNonQuery()
                locclv_id = prm.Value
                LocBdd = prm1.Value
                LocClv_Ciudad = prm2.Value
                LocNomciudad = prm3.Value
            End With
            
            Me.DAMECAJA_SUCURSALTableAdapter.Connection = CON
            Me.DAMECAJA_SUCURSALTableAdapter.Fill(Me.NewsoftvDataSet.DAMECAJA_SUCURSAL, GloHostName, GloIpMaquina, GloCaja, GloSucursal, GlonOMCaja, Bnd_1, GloImprimeTickets)
            Me.DAME_INF_SUCURSALTableAdapter.Connection = CON
            Me.DAME_INF_SUCURSALTableAdapter.Fill(Me.NewsoftvDataSet2.DAME_INF_SUCURSAL, GloHostName, GloIpMaquina, facnormal, facticket, GloSucursal, impresorafiscal)
            Me.Dame_clv_EmpresaTableAdapter.Connection = CON
            Me.Dame_clv_EmpresaTableAdapter.Fill(Me.Procedimientos_arnoldo.Dame_clv_Empresa, Locclv_empresa)
            Me.Selecciona_impresoraticketsTableAdapter.Connection = CON
            Me.Selecciona_impresoraticketsTableAdapter.Fill(Me.Procedimientos_arnoldo.Selecciona_impresoratickets, GloSucursal, LocImpresoraTickets)
            CON.Close()
            If Bnd_1 = 1 Then
                GloUsuario = Me.UsernameTextBox.Text
                Dim CON2 As New SqlConnection(MiConexion)
                CON2.Open()
                Me.VerAccesoTableAdapter.Connection = CON2
                Me.VerAccesoTableAdapter.Fill(Me.NewsoftvDataSet.VerAcceso, Me.UsernameTextBox.Text, Me.PasswordTextBox.Text, bnd)
                CON2.Close()
                If bnd = 1 Then

                    VALIDAAccesoUsuario(UsernameTextBox.Text, False, True)
                    If eMsj.Length > 0 Then
                        MessageBox.Show(eMsj)
                        Exit Sub
                    End If

                    Dim CON3 As New SqlConnection(MiConexion)
                    CON3.Open()
                    Me.VerAccesoAdminTableAdapter.Connection = CON3
                    Me.VerAccesoAdminTableAdapter.Fill(Me.DataSetEdgar.VerAccesoAdmin, Me.UsernameTextBox.Text, Me.PasswordTextBox.Text, eBnd)
                    CON3.Close()
                    If eBnd = 1 Then
                        eAccesoAdmin = True
                        eAccesoAdminBonifica = True
                        LocSupBon = Me.UsernameTextBox.Text
                    Else
                        eAccesoAdmin = False
                        eAccesoAdminBonifica = False
                    End If
                    eLoginCajera = RTrim(LTrim(Me.UsernameTextBox.Text))
                    ePassCajera = Me.PasswordTextBox.Text
                    LocLoginUsuario = Me.UsernameTextBox.Text
                    GloClv_Factura = 0
                    My.Forms.FrmCargarReporte.Show()
                    My.Forms.FrmCargarReporte.Close()
                    GloNomSucursal = Me.CiudadTextBox.Text
                    'MsgBox(Me.CiudadTextBox.Text, MsgBoxStyle.Information)
                    My.Forms.FrmMenu.Show()
                    Me.Close()
                Else
                    MsgBox(" Acceso Denegado ", MsgBoxStyle.Information)
                    Me.UsernameTextBox.Clear()
                    Me.PasswordTextBox.Clear()
                    'EnD
                End If
            Else
                MsgBox("La Máquina no tiene Acceso al Sistema de Faturación")
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub OK_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        acceso()


    End Sub

    Private Sub PasswordTextBox_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PasswordTextBox.KeyPress
        If Asc(e.KeyChar.ToString) = 13 Then
            acceso()
        End If
    End Sub

    Private Sub Cancel_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        End
        Me.Close()
    End Sub
    Private Sub Verifica_Ruta_reportes()
        Dim cad(4) As String
        cad = RutaReportes.Substring(2).Split("\")
        Dim ruta_new As New StringBuilder
        Dim CON1 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()

        If String.Equals(GloIp, cad(0)) = False Then
            ruta_new.Append("\\")
            ruta_new.Append(GloIp)
            If IdSistema <> "SA" Then
                ruta_new.Append("\exes\NewSoftvFacturacion\Reportes")
            ElseIf IdSistema = "SA" Then
                ruta_new.Append("\exes\FacSoftv\Reportes")
            End If
            Try
                CON1.Open()
                cmd = New SqlCommand()
                With cmd
                    .CommandText = "Modifica_Ruta_reportes"
                    .Connection = CON1
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    '@ruta varchar(max),@clave int

                    Dim prm As New SqlParameter("@ruta", SqlDbType.VarChar)
                    prm.Value = ruta_new.ToString()
                    prm.Direction = ParameterDirection.Input
                    .Parameters.Add(prm)

                    Dim prm1 As New SqlParameter("@clave", SqlDbType.Int)
                    prm1.Value = 2
                    prm1.Direction = ParameterDirection.Input
                    .Parameters.Add(prm1)

                    Dim i As Integer = .ExecuteNonQuery()
                End With
                Me.DameRutaTableAdapter.Connection = CON1
                Me.DameRutaTableAdapter.Fill(Me.NewsoftvDataSet2.DameRuta, 2, RutaReportes)
                CON1.Close()
            Catch ex As Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
    Private Sub LoginForm1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameDatosGenerales_2TableAdapter.Connection =con
            Me.DameDatosGenerales_2TableAdapter.Fill(Me.DataSetEdgar.DameDatosGenerales_2)
            con.close    
            GloEmpresa = Me.NombreTextBox.Text
            GloDireccionEmpresa = Me.DireccionTextBox.Text
            GloColonia_CpEmpresa = Me.ColoniaTextBox.Text
            GloCiudadEmpresa = Me.CiudadTextBox1.Text
            GloRfcEmpresa = Me.RfcTextBox.Text
            GloTelefonoEmpresa = Me.TELefonosTextBox.Text

            Dim polyPoints() As Point = {New Point(320, 125), New Point(700, 125), New Point(700, 50), New Point(320, 50)}
            Dim polyPoints2() As Point = {New Point(320, 185), New Point(700, 185), New Point(700, 140), New Point(320, 140)}
            Dim polyPoints3() As Point = {New Point(559, 209), New Point(500 + 194, 209), New Point(500 + 194, 235), New Point(559, 235)} 'Boton Cancelar
            Dim polyPoints4() As Point = {New Point(355, 209), New Point(489, 209), New Point(489, 235), New Point(355, 235)} ' Boton Aceptar
            Dim polyPoints5() As Point = {New Point(70, 235), New Point(300, 235), New Point(300, 50), New Point(70, 50)}
            'objDraw.AddEllipse(100, 50, 200, 200)
            objDraw.AddPolygon(polyPoints5)
            objDraw.AddPolygon(polyPoints)
            objDraw.AddPolygon(polyPoints2)
            objDraw.AddPolygon(polyPoints3)
            objDraw.AddPolygon(polyPoints4)
            Me.Region = New Region(objDraw)
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()

            Me.DameIdTableAdapter.Connection = CON2
            Me.DameIdTableAdapter.Fill(Me.NewsoftvDataSet2.DameId, IdSistema)

            Me.DameRutaTableAdapter.Connection =con2
            Me.DameRutaTableAdapter.Fill(Me.NewsoftvDataSet2.DameRuta, 2, RutaReportes)
            'TODO: esta línea de código carga datos en la tabla 'DataSetEdgar.DameDatosGenerales_2' Puede moverla o quitarla según sea necesario.
            'Verifica Ruta Reportes

            Verifica_Ruta_reportes()

            Dim Comando2 As New SqlClient.SqlCommand
            Dim reader As SqlDataReader
            With Comando2
                .Connection = CON2
                .CommandText = "DimeTipSer_CualEsPrincipal"
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = Comando2.ExecuteReader()
                Using reader
                    While reader.Read
                        ' Process SprocResults datareader here.
                        Glo_tipSer = reader.GetValue(0)
                    End While
                End Using
            End With
            'TODO: esta línea de código carga datos en la tabla 'DataSetEdgar.DameDatosGenerales' Puede moverla o quitarla según sea necesario.
            Me.DameDatosGeneralesTableAdapter.Connection =con2
            Me.DameDatosGeneralesTableAdapter.Fill(Me.DataSetEdgar.DameDatosGenerales)
            'TODO: esta línea de código carga datos en la tabla 'Procedimientos_arnoldo.Muestra_Empresa' Puede moverla o quitarla según sea necesario.
            Me.Muestra_EmpresaTableAdapter.Connection =con2
            Me.Muestra_EmpresaTableAdapter.Fill(Me.Procedimientos_arnoldo.Muestra_Empresa)
            'TODO: esta línea de código carga datos en la tabla 'Procedimientos_arnoldo.MUESTRAIMAGEN' Puede moverla o quitarla según sea necesario.
            Me.MUESTRAIMAGENTableAdapter.connection=con2
            Me.MUESTRAIMAGENTableAdapter.Fill(Me.Procedimientos_arnoldo.MUESTRAIMAGEN)
            Dim COMANDO As New SqlClient.SqlCommand
            With COMANDO
                .CommandText = "Dame_SubCiudad"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = CON2
                Dim prm As New SqlParameter("@subciudad", SqlDbType.VarChar, 50)
                prm.Direction = ParameterDirection.Output
                prm.Value = ""
                .Parameters.Add(prm)
                Dim i As Integer = COMANDO.ExecuteNonQuery()
                SubCiudad = prm.Value
            End With
            con2.close
        Catch ex As System.Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MsgBox("Es neta heee!!!")
    End Sub

    Private Sub VALIDAAccesoUsuario(ByVal CLV_USUARIO As String, ByVal CATV As Boolean, ByVal Facturacion As Boolean)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_USUARIO", SqlDbType.VarChar, CLV_USUARIO, 5)
        BaseII.CreateMyParameter("@CATV", SqlDbType.Bit, CATV)
        BaseII.CreateMyParameter("@Facturacion", SqlDbType.Bit, Facturacion)
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("VALIDAAccesoUsuario")
        eMsj = String.Empty
        eMsj = BaseII.dicoPar("@MSJ").ToString()
    End Sub
 
  

    
  
End Class

Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text

Public Class CAPTURAFACTURAGLOBAL
    Dim locIdFac As Long
    Dim locerror As Integer
    Private customersByCityReport As ReportDocument
    Dim Mes As Integer
    Dim StrMes As String
    Dim a�o As Integer
    Dim fecha As Date
    Private eCont As Integer = 1
    Private eRes As Integer = 0

    Private Sub CAPTURAFACTURAGLOBAL_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bnd = 3 Then
            If glotipoFacGlo = 0 Then
                Me.DateTimePicker2.Visible = False
                Me.Label1.Visible = False
            ElseIf glotipoFacGlo = 1 Then
                Me.DateTimePicker2.Visible = True
            End If
            bnd = 0
        End If
    End Sub


    Private Sub CAPTURAFACTURAGLOBAL_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        If Gloop = "N" Then
            Dim CON1 As New SqlConnection(MiConexion)
            CON1.Open()
            'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.DameFechadelServidorHora' Puede moverla o quitarla seg�n sea necesario.
            Me.DameFechadelServidorHoraTableAdapter.Connection = CON1
            Me.DameFechadelServidorHoraTableAdapter.Fill(Me.NewsoftvDataSet2.DameFechadelServidorHora)
            'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.mueastrasuc' Puede moverla o quitarla seg�n sea necesario.
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, bec_tipo)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            ComboBox1.DataSource = BaseII.ConsultaDT("mueastrasuc_compania")
            ComboBox1.DisplayMember = "NOMBRE"
            ComboBox1.ValueMember = "CLV_SUCURSAL"
            Me.MueastrasucTableAdapter.Connection = CON1
            Me.MueastrasucTableAdapter.Fill(Me.NewsoftvDataSet2.mueastrasuc, bec_tipo)
            CON1.Close()
            ValidaFechas()
            Me.ComboBox1.Text = ""
            'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.MUESTRASUCURSALES' Puede moverla o quitarla seg�n sea necesario.
            If eAccesoAdmin = True Then
                Me.DateTimePicker1.Enabled = True
                Me.DateTimePicker2.Enabled = True
            Else
                Me.DateTimePicker2.Enabled = False
                Me.DateTimePicker1.Enabled = False
            End If

            If bnd = 1 Then
                FrmTipoNota.Show()
            End If
        ElseIf Gloop = "C" Then
            Me.Panel1.Enabled = False
            Me.GroupBox1.Enabled = False
            Me.ComboBox1.Text = Locclv_sucursalglo
            Me.SerieTextBox.Text = LocSerieglo
            Me.NuevafacturaTextBox.Text = LocFacturaGlo
            Me.DateTimePicker2.Text = LocFechaGloFinal
            Me.DateTimePicker1.Text = LocFechaGlo

            'MODIFICADO 29 DIC 2009-------------------------------------------------------------
            'Me.TextBox4.Text = LocImporteGlo
            TextBoxImporte.Text = LocImporteGlo

            Me.TextBox5.Text = LocCajeroGlo

        End If
        If Gloop = "N" Or Gloop = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub
    Private Function Usp_DameDetallaFacturaGlobal(ByVal prmclvfactura As Long) As DataSet
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdFactura", SqlDbType.BigInt, prmclvfactura)
        Dim listaTablas As New List(Of String)
        listaTablas.Add("Usp_DameDetallaFacturaGlobal")
        Usp_DameDetallaFacturaGlobal = BaseII.ConsultaDS("Usp_DameDetallaFacturaGlobal", listaTablas)

    End Function

    Private Sub ConfigureCrystalReportefacturaGlobal(ByVal Letra2 As String, ByVal Importe As String, ByVal SubTotal As String, ByVal Iva As String, ByVal Ieps As String, ByVal Serie2 As String, ByVal Fecha2 As String, ByVal Cajera2 As String, ByVal Factura2 As String, ByVal prmIdFactura As Long)
        Try
            Dim ds As New DataSet
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim cliente2 As String = "P�BLICO EN GENERAL"
            Dim concepto2 As String = "Ingreso por Pago de Servicios"
            Dim reportPath As String = Nothing

            Select Case Locclv_empresa
                Case "AG"
                    reportPath = RutaReportes + "\ReporteFacturaGlobalGiga.rpt"
                    
                Case "TO"
                    reportPath = RutaReportes + "\ReporteFacturaGlobal.rpt"
                Case "SA"
                    reportPath = RutaReportes + "\ReporteFacturaGlobalTvRey.rpt"
                Case "VA"
                    reportPath = RutaReportes + "\ReporteFacturaGlobalTvRey.rpt"
            End Select
            customersByCityReport.Load(reportPath)

            ds.Clear()
            ds.Tables.Clear()
            ds = Usp_DameDetallaFacturaGlobal(prmIdFactura)

            SetDBReport(ds, customersByCityReport)

            If Locclv_empresa = "SA" Or Locclv_empresa = "TO" Or Locclv_empresa = "AG" Or Locclv_empresa = "VA" Then

                customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & Format(CDec(SubTotal.ToString()), "##,##0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("Factura").Text = "'" & Factura2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Direccion").Text = "'" & GloDireccionEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("RFC").Text = "'" & GloRfcEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudadEmpresa & "'"

                customersByCityReport.DataDefinition.FormulaFields("SubTotal").Text = "'" & Format(CDec(SubTotal), "##,##0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & Format(CDec(Iva.ToString()), "##,##0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ieps").Text = "'" & Format(CDec(Ieps.ToString()), "##,##0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & Format(CDec(Importe.ToString()), "##,##0.00") & "'"


                'subtotal2 = CDec(importe2) / 1.15
                'txtsubtotal = subtotal2
                'txtsubtotal = subtotal2.ToString("##0.00")
                'iva2 = CDec(importe2) / 1.15 * 0.15
                'myString = iva2.ToString("##0.00")

                'ElseIf Locclv_empresa = "AG" Then
                '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
                '    subtotal2 = CDec(importe2) / 1.15
                '    txtsubtotal = subtotal2
                '    txtsubtotal = subtotal2.ToString("##0.00")
                '    iva2 = CDec(importe2) / 1.15 * 0.15
                '    myString = iva2.ToString("##0.00")
            End If

            'MODIFICADO 29 DIC 09---------------------------------------------------------------

            'customersByCityReport.DataDefinition.FormulaFields("Subtotal").Text = "'" & txtsubtotal & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & myString & "'"
            'customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & txtsubtotal & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Factura").Text = "'" & Factura2 & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & importe2 & "'"




            'Select Case Locclv_empresa
            '    Case "AG"
            '        'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
            '        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            '    Case "TO"
            '        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            '    Case "SA"
            '        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            '    Case "VA"
            '        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            'End Select

            'eCont = 1
            'eRes = 0
            'Do
            '    If (MsgBox("Se va a imprimir una Factura Global. �Est� lista la impresora?", MsgBoxStyle.YesNo)) = 6 Then
            '        customersByCityReport.PrintToPrinter(1, True, 1, 1)
            '        eRes = MsgBox("La Impresi�n de la Factura " + CStr(eCont) + "/4, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
            '        '6=Yes;7=No
            '        If eRes = 6 Then eCont = eCont + 1
            '    Else
            '        Exit Sub
            '    End If
            'Loop While eCont <= 1
            'eCont = 1
            'eRes = 0
            'Do
            '    If (MsgBox("Se va a imprimir una copia de la Factura Global anterior. �Est� lista la impresora?", MsgBoxStyle.YesNo)) = 6 Then
            '        customersByCityReport.PrintToPrinter(1, True, 1, 1)
            '        eRes = MsgBox("La Impresi�n de la Factura " + CStr(eCont + 1) + "/4, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
            '        '6=Yes;7=No
            '        If eRes = 6 Then eCont = eCont + 1
            '    Else
            '        Exit Sub
            '    End If
            'Loop While eCont <= 1
            'eCont = 1
            'eRes = 0
            'Do
            '    If (MsgBox("Se va a imprimir una copia de la Factura Global anterior. �Est� lista la impresora?", MsgBoxStyle.YesNo)) = 6 Then
            '        customersByCityReport.PrintToPrinter(1, True, 1, 1)
            '        eRes = MsgBox("La Impresi�n de la Factura " + CStr(eCont + 2) + "/4, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
            '        '6=Yes;7=No
            '        If eRes = 6 Then eCont = eCont + 1
            '    Else
            '        Exit Sub
            '    End If
            'Loop While eCont <= 1
            'eCont = 1
            'eRes = 0
            'Do
            '    If (MsgBox("Se va a imprimir una copia de la Factura Global anterior. �Est� lista la impresora?", MsgBoxStyle.YesNo)) = 6 Then
            '        customersByCityReport.PrintToPrinter(1, True, 1, 1)
            '        eRes = MsgBox("La Impresi�n de la Factura " + CStr(eCont + 3) + "/4, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
            '        '6=Yes;7=No
            '        If eRes = 6 Then eCont = eCont + 1
            '    Else
            '        Exit Sub
            '    End If
            'Loop While eCont <= 1
            ''customersByCityReport.PrintToPrinter(1, True, 1, 1)
            'customersByCityReport = Nothing

            FrmImprimir.CrystalReportViewer1.ReportSource = customersByCityReport
            LiTipo = 0
            FrmImprimir.Show()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    'Private Sub oldConfigureCrystalReportefacturaGlobal(ByVal Letra2 As String, ByVal Importe As String, ByVal SubTotal As String, ByVal Iva As String, ByVal Ieps As String, ByVal Serie2 As String, ByVal Fecha2 As String, ByVal Cajera2 As String, ByVal Factura2 As String)
    '    customersByCityReport = New ReportDocument
    '    Dim connectionInfo As New ConnectionInfo

    '    connectionInfo.ServerName = GloServerName
    '    connectionInfo.DatabaseName = GloDatabaseName
    '    connectionInfo.UserID = GloUserID
    '    connectionInfo.Password = GloPassword




    '    Dim cliente2 As String = "P�blico en General"
    '    Dim concepto2 As String = "Ingreso por Pago de Servicios"

    '    Dim reportPath As String = Nothing
    '    If Locclv_empresa <> "TO" Then
    '        reportPath = RutaReportes + "\ReporteFacturaGlobalticket.rpt"
    '    Else
    '        reportPath = RutaReportes + "\ReporteFacturaGlobal.rpt"
    '    End If

    '    customersByCityReport.Load(reportPath)

    '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"

    '    customersByCityReport.DataDefinition.FormulaFields("Subtotal").Text = "'" & SubTotal.ToString("##.00") & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & Iva.ToString("##.00") & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & Importe.ToString("##.00") & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Factura").Text = "'" & Factura2 & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & Importe.ToString("##.00") & "'"

    '    'subtotal2 = CDec(importe2) / 1.15
    '    'txtsubtotal = subtotal2
    '    'txtsubtotal = subtotal2.ToString("##0.00")

    '    'iva2 = CDec(importe2) / 1.15 * 0.15
    '    'myString = iva2.ToString("##0.00")

    '    'customersByCityReport.DataDefinition.FormulaFields("Subtotal").Text = "'" & txtsubtotal & "'"
    '    'customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & myString & "'"
    '    'customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & txtsubtotal & "'"
    '    'customersByCityReport.DataDefinition.FormulaFields("Factura").Text = "'" & Factura2 & "'"
    '    'customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & importe2 & "'"




    '    If Locclv_empresa = "SA" Then
    '        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
    '    ElseIf Locclv_empresa <> "TO" Then
    '        customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
    '    Else
    '        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
    '    End If

    '    customersByCityReport.PrintToPrinter(1, True, 1, 1)
    '    customersByCityReport = Nothing


    'End Sub



    Private Sub Guardar()
        Try
            Dim glofac As Integer = 0
            Dim comando As New SqlClient.SqlCommand
            Dim Comando2 As New SqlClient.SqlCommand
            Dim CON3 As New SqlConnection(MiConexion)
            If glotipoFacGlo = 1 Then
                CON3.Open()
                Me.NUEVAFACTGLOBALTableAdapter.Connection = CON3
                'MODIFICADO 29 DIC 09---------------------------------------------------------------
                'Me.NUEVAFACTGLOBALTableAdapter.Fill(Me.DataSetLydia.NUEVAFACTGLOBAL, glofac, bec_tipo, Me.SerieTextBox.Text, Me.NuevafacturaTextBox.Text, Me.DateTimePicker2.Text, Me.TextBox5.Text, CDec(Me.TextBox4.Text), 0, CStr(Me.ComboBox1.SelectedValue), CDec(Me.Label6.Tag), bec_consecutivo)
                'Me.NUEVAFACTGLOBALTableAdapter.Fill(Me.DataSetLydia.NUEVAFACTGLOBAL, glofac, bec_tipo, Me.SerieTextBox.Text, Me.NuevafacturaTextBox.Text, Me.DateTimePicker2.Text, Me.TextBox5.Text, CDec(TextBoxImporte.Text), 0, CStr(Me.ComboBox1.SelectedValue), CDec(Me.Label6.Tag), bec_consecutivo)
                '(@IdFactura bigint output,@Tipo varchar(1),@Serie varchar(5),@Factura varchar(50),@Fecha datetime,@Cajera varchar(11),
                '@Importe decimal(18, 2),@Cancelada tinyint,@Sucursal int,@ImporteSinIva decimal(18, 2), @clv_factura bigint output)
                Dim conexion2 As New SqlConnection(MiConexion)
                conexion2.Open()
                Dim com As New SqlCommand("NUEVAFACTGLOBAL", conexion2)
                com.CommandType = CommandType.StoredProcedure
                Dim parametro1 As New SqlParameter("@IdFactura", SqlDbType.Int)
                parametro1.Direction = ParameterDirection.Output
                parametro1.Value = glofac
                com.Parameters.Add(parametro1)
                Dim prm2 As New SqlParameter("@Tipo", SqlDbType.VarChar)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = bec_tipo
                com.Parameters.Add(prm2)
                Dim prm3 As New SqlParameter("@Serie", SqlDbType.VarChar)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = Me.SerieTextBox.Text
                com.Parameters.Add(prm3)
                Dim prm4 As New SqlParameter("@Factura", SqlDbType.VarChar)
                prm4.Direction = ParameterDirection.Input
                prm4.Value = Me.NuevafacturaTextBox.Text
                com.Parameters.Add(prm4)
                Dim prm5 As New SqlParameter("@Fecha", SqlDbType.DateTime)
                prm5.Direction = ParameterDirection.Input
                prm5.Value = Me.DateTimePicker2.Text
                com.Parameters.Add(prm5)
                Dim prm6 As New SqlParameter("@Cajera", SqlDbType.VarChar)
                prm6.Direction = ParameterDirection.Input
                prm6.Value = Me.TextBox5.Text
                com.Parameters.Add(prm6)
                Dim prm7 As New SqlParameter("@Importe", SqlDbType.Decimal)
                prm7.Direction = ParameterDirection.Input
                prm7.Value = CDec(TextBoxImporte.Text)
                com.Parameters.Add(prm7)
                Dim prm8 As New SqlParameter("@Cancelada", SqlDbType.TinyInt)
                prm8.Direction = ParameterDirection.Input
                prm8.Value = 0
                com.Parameters.Add(prm8)
                Dim prm9 As New SqlParameter("@Sucursal", SqlDbType.Int)
                prm9.Direction = ParameterDirection.Input
                prm9.Value = CStr(Me.ComboBox1.SelectedValue)
                com.Parameters.Add(prm9)
                Dim prm10 As New SqlParameter("@ImporteSinIva", SqlDbType.Decimal)
                prm10.Direction = ParameterDirection.Input
                prm10.Value = CDec(Me.Label6.Tag)
                com.Parameters.Add(prm10)
                Dim prm11 As New SqlParameter("@clv_factura", SqlDbType.BigInt)
                prm11.Direction = ParameterDirection.Output
                prm11.Value = bec_consecutivo
                com.Parameters.Add(prm11)
                Dim prm12 As New SqlParameter("@idcompania", SqlDbType.Int)
                prm12.Direction = ParameterDirection.Input
                prm12.Value = GloIdCompania
                com.Parameters.Add(prm12)
                com.ExecuteNonQuery()
                glofac = parametro1.Value
                bec_consecutivo = prm11.Value
                conexion2.Close()
                NueDetFacturaGlobalImpuestos(glofac, TextBoxIVA.Text, TextBoxIEPS.Text, TextBoxSubTotal.Text, TextBoxImporte.Text)

                'FacturaFiscalCFD---------------------------------------------------------------------
                'Generaci�n de FF
                facturaFiscalCFD = False
                facturaFiscalCFD = ChecaSiEsFacturaFiscal("G", 0)
                If facturaFiscalCFD = True Then
                    GeneraFacturaCFD("G", glofac, SerieTextBox.Text, NuevafacturaTextBox.Text, 0, "")
                End If
                '-------------------------------------------------------------------------------------


                With Comando2
                    .CommandText = "GUARDARel_FacturaGlo_Rango"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = CON3
                    Dim prm As New SqlParameter("@Clv_factura", SqlDbType.BigInt)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = glofac
                    .Parameters.Add(prm)

                    Dim prm22 As New SqlParameter("@Fecha_Ini", SqlDbType.DateTime)
                    prm22.Direction = ParameterDirection.Input
                    prm22.Value = Me.DateTimePicker1.Text
                    .Parameters.Add(prm22)

                    Dim prm33 As New SqlParameter("@Fecha_Fin", SqlDbType.DateTime)
                    prm33.Direction = ParameterDirection.Input
                    prm33.Value = Me.DateTimePicker2.Text
                    .Parameters.Add(prm33)

                    'Dim prm44 As New SqlParameter("@idcompania", SqlDbType.Int)
                    'prm44.Direction = ParameterDirection.Input
                    'prm44.Value = GloIdCompania
                    '.Parameters.Add(prm44)

                    Dim i As Integer = Comando2.ExecuteNonQuery
                End With
                CON3.Close()
            Else
                CON3.Open()
                Me.NUEVAFACTGLOBALTableAdapter.Connection = CON3
                'MODIFICADO 29 DIC 09---------------------------------------------------------------
                'Me.NUEVAFACTGLOBALTableAdapter.Fill(Me.DataSetLydia.NUEVAFACTGLOBAL, glofac, bec_tipo, Me.SerieTextBox.Text, Me.NuevafacturaTextBox.Text, Me.DateTimePicker1.Text, Me.TextBox5.Text, CDec(Me.TextBox4.Text), 0, CStr(Me.ComboBox1.SelectedValue), CDec(Me.Label6.Tag), bec_consecutivo)
                ''Me.NUEVAFACTGLOBALTableAdapter.Fill(Me.DataSetLydia.NUEVAFACTGLOBAL, glofac, bec_tipo, Me.SerieTextBox.Text, Me.NuevafacturaTextBox.Text, Me.DateTimePicker1.Text, Me.TextBox5.Text, CDec(TextBoxImporte.Text), 0, CStr(Me.ComboBox1.SelectedValue), CDec(Me.Label6.Tag), bec_consecutivo)
                Dim com As New SqlCommand("NUEVAFACTGLOBAL", CON3)
                com.CommandType = CommandType.StoredProcedure
                Dim parametro1 As New SqlParameter("@IdFactura", SqlDbType.Int)
                parametro1.Direction = ParameterDirection.Output
                parametro1.Value = 0
                com.Parameters.Add(parametro1)
                Dim prm2 As New SqlParameter("@Tipo", SqlDbType.VarChar)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = bec_tipo
                com.Parameters.Add(prm2)
                Dim prm3 As New SqlParameter("@Serie", SqlDbType.VarChar)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = Me.SerieTextBox.Text
                com.Parameters.Add(prm3)
                Dim prm4 As New SqlParameter("@Factura", SqlDbType.VarChar)
                prm4.Direction = ParameterDirection.Input
                prm4.Value = Me.NuevafacturaTextBox.Text
                com.Parameters.Add(prm4)
                Dim prm5 As New SqlParameter("@Fecha", SqlDbType.DateTime)
                prm5.Direction = ParameterDirection.Input
                prm5.Value = Me.DateTimePicker2.Text
                com.Parameters.Add(prm5)
                Dim prm6 As New SqlParameter("@Cajera", SqlDbType.VarChar)
                prm6.Direction = ParameterDirection.Input
                prm6.Value = Me.TextBox5.Text
                com.Parameters.Add(prm6)
                Dim prm7 As New SqlParameter("@Importe", SqlDbType.Decimal)
                prm7.Direction = ParameterDirection.Input
                prm7.Value = CDec(TextBoxImporte.Text)
                com.Parameters.Add(prm7)
                Dim prm8 As New SqlParameter("@Cancelada", SqlDbType.TinyInt)
                prm8.Direction = ParameterDirection.Input
                prm8.Value = 0
                com.Parameters.Add(prm8)
                Dim prm9 As New SqlParameter("@Sucursal", SqlDbType.Int)
                prm9.Direction = ParameterDirection.Input
                prm9.Value = CStr(Me.ComboBox1.SelectedValue)
                com.Parameters.Add(prm9)
                Dim prm10 As New SqlParameter("@ImporteSinIva", SqlDbType.Decimal)
                prm10.Direction = ParameterDirection.Input
                If IsNumeric(Me.Label6.Tag) = False Then Me.Label6.Tag = 0
                If Me.Label6.Tag = Nothing Then Me.Label6.Tag = 0
                prm10.Value = CDec(Me.Label6.Tag)
                com.Parameters.Add(prm10)
                Dim prm11 As New SqlParameter("@clv_factura", SqlDbType.BigInt)
                prm11.Direction = ParameterDirection.Output
                prm11.Value = 0
                com.Parameters.Add(prm11)
                Dim prm12 As New SqlParameter("@idcompania", SqlDbType.Int)
                prm12.Direction = ParameterDirection.Input
                prm12.Value = GloIdCompania
                com.Parameters.Add(prm12)
                com.ExecuteNonQuery()
                glofac = CLng(parametro1.Value)
                bec_consecutivo = CLng(prm11.Value)
                CON3.Close()
                NueDetFacturaGlobalImpuestos(glofac, TextBoxIVA.Text, TextBoxIEPS.Text, TextBoxSubTotal.Text, TextBoxImporte.Text)

                'FacturaFiscalCFD---------------------------------------------------------------------
                'Generaci�n de FF
                facturaFiscalCFD = False
                facturaFiscalCFD = ChecaSiEsFacturaFiscal("G", 0)
                If facturaFiscalCFD = True Then
                    GeneraFacturaCFD("G", glofac, SerieTextBox.Text, NuevafacturaTextBox.Text, 0, "")
                End If
                '--------------------------------------------------------------------------------------

            End If
            CON3.Open()
            With comando
                .CommandText = "Guarda_Desgloce_con_iva"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = CON3
                Dim prm As New SqlParameter("@Clv_factura", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = glofac
                .Parameters.Add(prm)
                Dim i As Integer = comando.ExecuteNonQuery
            End With
            CON3.Close()

            Guarda_Rel_FacturaGlobal_Facturas(Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, glofac, glotipoFacGlo)
            locIdFac = glofac
            'MODIFICADO 29 DIC 09---------------------------------------------------------------
            'bitsist(GloUsuario, 0, GloSistema, "Nueva Factura Global", "Sucursal En Donde Se Gener�: " + CStr(GloSucursal) + ", Nombre de la Caja: " + CStr(GlonOMCaja), "Se Gener� la Factura Global del cajero: " + Me.TextBox5.Text, "Del Dia:" + Me.DateTimePicker1.Text + " Por la Cantidad de: " + TEXTbox4.Text, LocNomciudad)
            bitsist(GloUsuario, 0, GloSistema, "Nueva Factura Global", "Sucursal En Donde Se Gener�: " + CStr(GloSucursal) + ", Nombre de la Caja: " + CStr(GlonOMCaja), "Se Gener� la Factura Global del cajero: " + Me.TextBox5.Text, "Del Dia:" + Me.DateTimePicker1.Text + " Por la Cantidad de: " + TextBoxImporte.Text, LocNomciudad)


        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CantidadaLetra()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameCantidadALetraTableAdapter.Connection = CON
            'MODIFICADO 29 DIC 09---------------------------------------------------------------
            'Me.DameCantidadALetraTableAdapter.Fill(Me.NewsoftvDataSet2.DameCantidadALetra, CDec(Me.TextBox4.Text), 0, bec_letra)
            Me.DameCantidadALetraTableAdapter.Fill(Me.NewsoftvDataSet2.DameCantidadALetra, CDec(TextBoxImporte.Text), 0, bec_letra)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ValidaFechas()
        Mes = CInt(Month(Me.DateTimePicker1.Text))
        a�o = CInt(Year(Me.DateTimePicker1.Text))
        fecha = DateSerial(a�o, Mes + 1, 0)
        StrMes = CStr(Mes)
        StrMes = StrMes.PadLeft(2, "0")
        'Me.DateTimePicker1.MinDate = CDate("01/" + StrMes + "/" + CStr(a�o))
        'Me.DateTimePicker1.MaxDate = CDate(fecha)
        'Me.DateTimePicker2.MinDate = CDate("01/" + StrMes + "/" + CStr(a�o))
        'Me.DateTimePicker2.MaxDate = CDate(fecha)
        Me.Label1.Visible = True
    End Sub


    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Dim conlidia2 As New SqlClient.SqlConnection(MiConexion)
        Dim num As Integer
        If Gloop = "N" Then
            If (glotipoFacGlo = 1 And (CDate(Me.DateTimePicker1.Text) <= CDate(Me.DateTimePicker2.Text))) Or glotipoFacGlo = 0 Then

                'MODIFICADO 29 DIC 09---------------------------------------------------------------
                'If Me.TextBox4.Text <> "" Then
                'If CInt(Me.TextBox4.Text) <> 0 Then

                If IsNumeric(TextBoxImporte.Text) = False Then
                    MsgBox("Importe no v�lido.", MsgBoxStyle.Exclamation)
                    Exit Sub
                Else
                    If CDec(TextBoxImporte.Text) = 0 Then
                        MsgBox("Importe mayor a cero.", MsgBoxStyle.Exclamation)
                        Exit Sub
                    End If
                End If

                If locerror = 0 Then
                    bec_bandera = 1
                    conlidia2.Open()
                    Dim comando2 As New SqlClient.SqlCommand
                    With comando2
                        .Connection = conlidia2
                        .CommandText = "Dime_FacGlo_Ex "
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@sucursal", SqlDbType.Int)
                        Dim prm1 As New SqlParameter("@fecha", SqlDbType.DateTime)
                        Dim prm2 As New SqlParameter("@Fecha_2", SqlDbType.DateTime)
                        Dim prm3 As New SqlParameter("@Opc", SqlDbType.Int)
                        Dim prm4 As New SqlParameter("@Cont", SqlDbType.Int)
                        Dim prm5 As New SqlParameter("@idcompania", SqlDbType.Int)

                        prm.Direction = ParameterDirection.Input
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Output
                        prm5.Direction = ParameterDirection.Input
                        prm.Value = Me.ComboBox1.SelectedValue
                        prm1.Value = Me.DateTimePicker1.Text
                        prm2.Value = Me.DateTimePicker2.Text
                        If bec_tipo = "C" Then
                            prm3.Value = glotipoFacGlo + 1
                        ElseIf bec_tipo = "V" Then
                            prm3.Value = 4
                        End If
                        prm4.Value = 0
                        prm5.Value = GloIdCompania
                        .Parameters.Add(prm)
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm5)
                        Dim i As Integer = comando2.ExecuteNonQuery()
                        num = prm4.Value

                    End With
                    conlidia2.Close()
                    If num = 0 Then
                        Guardar()
                        bec_serie = Me.SerieTextBox.Text
                        bec_factura = Me.NuevafacturaTextBox.Text
                        bec_fecha = Me.DateTimePicker1.Text
                        'bec_importe = Me.TextBox4.Text
                        'MODIFICADO 29 DIC 09
                        CantidadaLetra()
                    ElseIf num > 0 And glotipoFacGlo = 0 Then
                        MsgBox("La factura Global del dia " + Me.DateTimePicker1.Text + " ya ha sido Generada ", MsgBoxStyle.Information)
                        Exit Sub
                    ElseIf num > 0 And glotipoFacGlo = 1 Then
                        MsgBox("Ya Existe una Factura Global en el Rango del : " + Me.DateTimePicker1.Text + " Al D�a " + Me.DateTimePicker2.Text, MsgBoxStyle.Information)
                        Exit Sub
                    ElseIf num = -3 Then
                        MsgBox("La Factura Global se Genera Cada Mes Autom�ticamente, por lo que no es Posible Generarla Manual", MsgBoxStyle.Information)
                    ElseIf num = -4 Then
                        MsgBox("La Factura Global se Genera Diario Autom�ticamente, por lo que no es Posible Generarla Manual", MsgBoxStyle.Information)
                    End If

                    If LocImpresoraTickets = "" Then
                        MsgBox("No Se Ha Asigando Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
                    ElseIf LocImpresoraTickets <> "" Then

                        'FacturaFiscalCFD---------------------------------------------------------------------
                        'Impresi�n de Factura Global
                        'facturaFiscalCFD = False
                        'facturaFiscalCFD = ChecaSiEsFacturaFiscal("G", 0)
                        'If facturaFiscalCFD = False Then
                        Me.ConfigureCrystalReportefacturaGlobal(bec_letra, TextBoxImporte.Text, TextBoxSubTotal.Text, TextBoxIVA.Text, TextBoxIEPS.Text, bec_serie, bec_fecha, GloUsuario, bec_factura, locIdFac)
                        'End If
                        '--------------------------------------------------------------------------------------

                    End If
                    'GloReporte = 5
                    'My.Forms.FrmImprimirRepGral.Show()
                    Me.Close()
                    'End If
                Else
                    MsgBox("La Sucursal no tiene Asignada una Serie Para sus Facturas Globales", MsgBoxStyle.Information)
                End If
                'Else
                '    MsgBox("El Importe debe ser Mayor que Cero", MsgBoxStyle.Information)
                'End If
                'Else
                '    MsgBox("Seleccione una Sucursal", MsgBoxStyle.Information)
                'End If
            ElseIf glotipoFacGlo = 1 And (CDate(Me.DateTimePicker1.Text) > CDate(Me.DateTimePicker2.Text)) Then
                MsgBox("La Fecha de Inicio debe ser Mayor a la Fecha Final", MsgBoxStyle.Information)
            End If
        ElseIf Gloop = "C" Then
            Me.Close()
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub ComboBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.LostFocus
        If Me.ComboBox1.SelectedIndex = -1 Then
            Me.SerieTextBox.Clear()
            Me.NuevafacturaTextBox.Clear()
            'MODIFICADO 29 DIC 09
            'Me.TextBox4.Clear()
            TextBoxImporte.Clear()
            Me.TextBox5.Clear()
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)

        DameImporteFacturaGlobal(Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, 0, glotipoFacGlo)
        'MODIFICADO 29 DIC 09---------------------------------------------------------------
        'Me.DameImporteSucursalTableAdapter.Connection = CON
        'Me.DameImporteSucursalTableAdapter.Fill(Me.DataSetLydia.DameImporteSucursal, Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, glotipoFacGlo)
        CON.Open()
        Me.Llena_Factura_Global_nuevoTableAdapter.Connection = CON
        Me.Llena_Factura_Global_nuevoTableAdapter.Fill(Me.Procedimientos_arnoldo.Llena_Factura_Global_nuevo, CInt(Me.ComboBox1.SelectedValue), locerror)
        BaseII.limpiaParametros()
        'BaseII.CreateMyParameter()
        CON.Close()
        Me.TextBox5.Text = GloUsuario
    End Sub



    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Dim CON13 As New SqlConnection(MiConexion)
        If Gloop = "N" Then
            DameImporteFacturaGlobal(Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, 0, glotipoFacGlo)
            'MODIFICADO 29 DIC 09---------------------------------------------------------------
            'Me.DameImporteSucursalTableAdapter.Connection = CON13
            'Me.DameImporteSucursalTableAdapter.Fill(Me.DataSetLydia.DameImporteSucursal, Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, glotipoFacGlo)
            CON13.Open()
            Me.Llena_Factura_Global_nuevoTableAdapter.Connection = CON13
            Me.Llena_Factura_Global_nuevoTableAdapter.Fill(Me.Procedimientos_arnoldo.Llena_Factura_Global_nuevo, CInt(Me.ComboBox1.SelectedValue), locerror)
            CON13.Close()
            Me.TextBox5.Text = GloUsuario
            'ElseIf glotipoFacGlo = 1 Then
            'ValidaFechas()
        End If
    End Sub


    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Dim conLidia As New SqlClient.SqlConnection(MiConexion)
        If glotipoFacGlo = 1 Then
            DameImporteFacturaGlobal(Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, 0, glotipoFacGlo)
            'MODIFICADO 29 DIC 09---------------------------------------------------------------
            'Me.DameImporteSucursalTableAdapter.Connection = conLidia
            'Me.DameImporteSucursalTableAdapter.Fill(Me.DataSetLydia.DameImporteSucursal, Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, glotipoFacGlo)
            conLidia.Open()
            Me.Llena_Factura_Global_nuevoTableAdapter.Connection = conLidia
            Me.Llena_Factura_Global_nuevoTableAdapter.Fill(Me.Procedimientos_arnoldo.Llena_Factura_Global_nuevo, CInt(Me.ComboBox1.SelectedValue), locerror)
            conLidia.Close()
        End If
    End Sub

    Private Sub DameImporteFacturaGlobal(ByVal Fecha As DateTime, ByVal FechaFin As DateTime, ByVal SelSucursal As Integer, ByVal Tipo As Char, ByVal IDFactura As Long, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC DameImporteFacturaGlobal ")
        strSQL.Append("'" & CStr(Fecha) & "', ")
        strSQL.Append("'" & CStr(FechaFin) & "', ")
        strSQL.Append(CStr(SelSucursal) & ", ")
        strSQL.Append("'" & Tipo & "', ")
        strSQL.Append(CStr(IDFactura) & ", ")
        strSQL.Append(CStr(Op) & ", ")
        strSQL.Append(CStr(GloIdCompania))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable

        Try

            dataAdapter.Fill(dataTable)
            TextBoxImporte.Text = Format(CDec(dataTable.Rows(0)(0).ToString()), "#####0.00")
            TextBoxSubTotal.Text = Format(CDec(dataTable.Rows(0)(1).ToString()), "#####0.00")
            TextBoxIVA.Text = Format(CDec(dataTable.Rows(0)(2).ToString()), "#####0.00")
            TextBoxIEPS.Text = Format(CDec(dataTable.Rows(0)(3).ToString()), "#####0.00")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub Guarda_Rel_FacturaGlobal_Facturas(ByVal Fecha As DateTime, ByVal FechaFin As DateTime, ByVal SelSucursal As Integer, ByVal Tipo As Char, ByVal IDFactura As Long, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Guarda_Rel_FacturaGlobal_Facturas", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        '@Fecha DateTime,	@FechaFin datetime,	@Selsucursal int,@Tipo as varchar(1),	@IDFACTURA BIGINT,	@opc int

        Dim parametro As New SqlParameter("@Fecha", SqlDbType.DateTime)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Fecha
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@FechaFin", SqlDbType.DateTime)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = FechaFin
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Selsucursal", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = SelSucursal
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Tipo", SqlDbType.VarChar, 50)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Tipo
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@IDFACTURA", SqlDbType.BigInt)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = IDFactura
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@opc", SqlDbType.Int)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = Op
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@idcompania", SqlDbType.Int)
        parametro7.Direction = ParameterDirection.Input
        parametro7.Value = GloIdCompania
        comando.Parameters.Add(parametro7)
        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub


    Private Sub NueDetFacturaGlobalImpuestos(ByVal IDFactura As Long, ByVal Iva As Decimal, ByVal Ieps As Decimal, ByVal SubTotal As Decimal, ByVal Total As Decimal)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueDetFacturaGlobalImpuestos", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@IDFactura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = IDFactura
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Iva", SqlDbType.Money)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Iva
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Ieps", SqlDbType.Money)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Ieps
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@SubTotal", SqlDbType.Money)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = SubTotal
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Total", SqlDbType.Money)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Total
        comando.Parameters.Add(parametro5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

End Class
Imports System.Data.SqlClient

Public Class FrmListadoEntregasParciales

    Private Sub FrmListadoEntregasParciales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MUESTRAUSUARIOS_Con_Entregas_ParcialesTableAdapter.Connection = CON
        Me.MUESTRAUSUARIOS_Con_Entregas_ParcialesTableAdapter.Fill(Me.Procedimientos_arnoldo.MUESTRAUSUARIOS_Con_Entregas_Parciales, 0)
        CON.Close()
        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0)
        'BaseII.CreateMyParameter("@usuario", SqlDbType.VarChar, GloCaja)
        Me.ListBox2.Text = ""
        If ListBox1.Items.Count = 0 Then
            MsgBox("No hay Cajeros(as) con Entregas Parciales")
            Me.Close()
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim x As Integer = 0
        Dim y As Integer
        x = Me.ListBox2.Items.Count()
        Repetido = False


        If x > 0 Then

            For y = 0 To (x - 1)
                Me.ListBox2.SelectedIndex = y
                If (Me.ListBox1.Text = Me.ListBox2.Text) Then
                    Repetido = True
                End If
            Next

            If Repetido = True Then
                MsgBox("El(La) Cajero(a) ya est� en la Lista", , "Atenci�n")
            Else
                Me.ListBox2.Items.Add(Me.ListBox1.Text)
                Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
            End If

        Else
            Me.ListBox2.Items.Add(Me.ListBox1.Text)
            Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        End If


        If Me.ListBox2.Items.Count > 0 Then
            Button3.Enabled = True
            Button4.Enabled = True
        Else
            Button3.Enabled = False
            Button4.Enabled = False
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim x As Integer
        Dim y As Integer


        ListBox2.Items.Clear()
        ListBox3.Items.Clear()
        x = Me.ListBox1.Items.Count()


        Me.ListBox1.SelectedIndex = 0
        For y = 1 To x
            Me.ListBox1.SelectedIndex = (y - 1)
            Me.ListBox2.Items.Add(Me.ListBox1.Text)
            Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        Next


        If Me.ListBox2.Items.Count > 0 Then
            Button3.Enabled = True
            Button4.Enabled = True
        Else
            Button3.Enabled = False
            Button4.Enabled = False
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim x As Integer
        Dim y As Integer
        x = Me.ListBox2.Items.Count()
        For y = 0 To (x - 1)
            Me.ListBox2.Items.RemoveAt(0)
            Me.ListBox3.Items.RemoveAt(0)
        Next

        If Me.ListBox2.Items.Count > 0 Then
            Button3.Enabled = True
            Button4.Enabled = True
        Else
            Button3.Enabled = False
            Button4.Enabled = False
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If (Me.ListBox2.SelectedIndex <> -1) Then
            Me.ListBox3.SelectedIndex = Me.ListBox2.SelectedIndex
            Me.ListBox2.Items.RemoveAt(Me.ListBox2.SelectedIndex)
            Me.ListBox3.Items.RemoveAt(Me.ListBox3.SelectedIndex)
        Else
            MsgBox("Selecciona primero a un(a) Cajero(a) a Eliminar", , "Atenci�n")
        End If

        If Me.ListBox2.Items.Count > 0 Then
            Button3.Enabled = True
            Button4.Enabled = True
        Else
            Button3.Enabled = False
            Button4.Enabled = False
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim x As Integer = 0
        Dim y As Integer

        If Me.ListBox2.Items.Count = 0 Then
            MsgBox("Selecciona al menos un(a) Cajero(a)", , "Atenci�n")
        Else
            x = Me.ListBox3.Items.Count()
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            For y = 0 To (x - 1)
                Me.ListBox3.SelectedIndex = y
                Me.LLena_Tabla_UsuariosTableAdapter.Connection = CON
                Me.LLena_Tabla_UsuariosTableAdapter.Fill(Me.Procedimientos_arnoldo.LLena_Tabla_Usuarios, LocClv_session, Me.ListBox3.Text)
            Next
            CON.Close()
            GloReporte = 8
            My.Forms.FrmImprimirRepGral.Show()
            Me.Close()
        End If
    End Sub


End Class